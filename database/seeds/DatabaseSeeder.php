<?php

use Illuminate\Database\Seeder;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        User::create([
        	'user_type_id' => 5,
        	'user_username' => 'krisnasw',
        	'user_password' => bcrypt('admin'),
        	'user_first_name' => 'Krisna',
        	'user_last_name' => 'Satria',
        	'user_email' => 'krisnasw@live.com',
        	'user_phone' => '082229246468',
        	'user_img' => 'images/user/user1.png',
        	'user_active_status' => 'true',
        	'employee_id' => 3
        ]);
    }
}
