@extends('template.index')

@section('title')
Dealer Information System - Edit Paket Barang
@stop

@section('style')

    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/css/buttons.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css') }}">

@stop

@section('content')
<div class="main-body">
	<div class="page-wrapper">

        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Edit Paket Barang</h4>
                            <span>Edit Paket Barang - {!! $item->item_package_name !!}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Master Data - Paket Barang</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

		<div class="page-body">
			<div class="row">
				<div class="col-sm-12">

					<!-- Tooltip Validation card start -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Edit Paket Barang - {!! $item->item_package_name !!}</h5>
                            <div class="card-header-right">
                            	<i class="icofont icofont-spinner-alt-5"></i>
                            </div>
                        </div>
                        <div class="card-block">
                        	{!! Form::model($item, ['id' => 'second', 'method' => 'PATCH', 'novalidate' => 'novalidate', 'action' => ['PaketBarangController@update', base64_encode($item->item_package_id)]]) !!}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Number Part</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="number_part" placeholder="Masukkan No. Part" value="{!! $item->item_package_code !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nama Barang</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="f_nama" placeholder="Masukkan Nama Barang" value="{!! $item->item_package_name !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Merk</label>
                                    <div class="col-sm-10">
                                        <select class="col-sm-12 js-example-basic-single select2-hidden-accessible" name="merk">
                                            @foreach($brand as $key)
                                            <option value="{!! $key->brand_id !!}">{!! $key->brand_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Kode Part</label>
                                    <div class="col-sm-10">
                                        <select class="col-sm-12 js-example-basic-single select2-hidden-accessible" name="kodePart">
                                            @foreach($BrandDetail as $key)
                                            <option value="{!! $key->brand_detail_id !!}" {!! ($key->brand_detail_id == $item->brand_detail_id) ? 'selected' : '' !!}>{!! $key->brand_detail_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Motor</label>
                                    <div class="col-sm-10">
                                        <select class="js-example-basic-multiple col-sm-12" multiple="multiple" name="motor[]">
                                            @foreach($character as $key)
                                            <option value="{!! $key->character_id !!}" {!! (in_array($key->character_id, explode(',', $item->character_id))) ? 'selected' : '' !!}>{!! $key->character_seri !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Satuan</label>
                                    <div class="col-sm-10">
                                        <select class="col-sm-12 js-example-basic-single select2-hidden-accessible" name="unit">
                                            @foreach($unit as $key)
                                            <option value="{!! $key->unit_id !!}" {!! ($key->unit_id == $item->unit_id) ? 'selected' : '' !!}>{!! $key->unit_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">H.E.T</label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" name="het" placeholder="Masukkan H.E.T" value="{!! $item->item_package_het !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Harga Pokok</label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" name="harga_pokok" placeholder="Masukkan Harga Pokok" value="{!! $item->item_package_price_primary !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Harga Beli</label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" name="harga_beli" placeholder="Masukkan Harga Beli" value="{!! $item->item_package_price_buy !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Stok Barang</label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" name="stok" placeholder="Masukkan Stok Barang" value="{!! $item->item_package_stock !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Minimum Stok</label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" name="min_stok" placeholder="Masukkan Minimum Stok" value="{!! $item->item_package_stock_min !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Maksimum Stok</label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" name="max_stok" placeholder="Masukkan Maksimum Stok" value="{!! $item->item_package_stock_max !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Barcode</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="barcode" placeholder="Masukkan Barcode" value="{!! $item->item_package_barcode !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Berat</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="berat" placeholder="Masukkan Berat" value="{!! $item->item_package_weight !!}">
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-block">
                            <a href="javascript::void(0)" id="addRow" class="btn btn-primary m-b-20">+ Add Formula</a>
                            <a href="javascript::void(0)" id="prosesFormulas" class="btn btn-info m-b-20"><i class="icofont icofont-refresh"></i> Proses Formula</a>

                            <div class="card" id="form-hidden">
                                <div class="card-block">
                                {!! Form::open(['method' => 'POST', 'action' => 'ApiController@addFormulas', 'id' => 'form-detail']) !!}
                                    <h4 class="sub-title">Form Formula</h4>
                                        <input type="hidden" name="id" value="{!! $item->item_package_id !!}">
                                        <input type="hidden" name="motor" value="{!! $item->character_id !!}">
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Barang</label>
                                            <div class="col-sm-10">
                                                <select class="col-sm-12" id="select-barang" name="f_barang">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Quantity</label>
                                            <div class="col-sm-10">
                                                <input type="number" class="form-control" name="qty" placeholder="Masukkan Quantity">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <label class="col-sm-2"></label>
                                            <div class="col-sm-10">
                                                <button type="reset" class="btn btn-danger m-b-0" id="reset">Reset</button>
                                                <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                            </div>
                                        </div>
                                {!! Form::close() !!}
                                </div>
                            </div>

                            <div class="card" id="form-hidden2">
                                <div class="card-block">
                                {!! Form::open(['method' => 'POST', 'action' => 'ApiController@prosesFormulas', 'id' => 'form-detail2']) !!}
                                    <h4 class="sub-title">Form Formula</h4>
                                        <input type="hidden" name="id" value="{!! $item->item_package_id !!}">
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Jumlah Paket Barang</label>
                                            <div class="col-sm-10">
                                                <input type="number" class="form-control" name="jumlahPaket" placeholder="Masukkan Jumlah Paket Barang">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <label class="col-sm-2"></label>
                                            <div class="col-sm-10">
                                                <button type="reset" class="btn btn-danger m-b-0" id="reset2">Reset</button>
                                                <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                            </div>
                                        </div>
                                {!! Form::close() !!}
                                </div>
                            </div>

                            <div class="dt-responsive table-responsive">
                                <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Number Part</th>
                                            <th>Nama Barang</th>
                                            <th>Qty</th>
                                            <th>Config</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($formulas as $val => $keys)
                                        <tr>
                                            <td>{!! ++$val !!}</td>
                                            <td>{!! (empty($keys->packages->item_package_name)) ? 'Kosong' : $keys->packages->item_package_name !!}</td>
                                            <td>{!! (empty($keys->items->item_name)) ? 'Kosong' : $keys->items->item_name !!}</td>
                                            <td>{!! $keys->item_package_formula_qty !!}</td>
                                            <td>
                                                <button type="button" class="btn btn-danger btn-outline-info" data-toggle="modal" data-target="#delete-target{{{ $keys->item_package_formula_id }}}"><i class="icofont icofont-delete-alt"></i></button>
                                            </td>
                                        </tr>

                                        <div class="modal fade" id="delete-target{{{ $keys->item_package_formula_id }}}" tabindex="-1" role="dialog">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header bg-danger">
                                                        <h4 class="modal-title">Hapus Data</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Apakah Anda Yakin Akan Menghapus <b>{!! $keys->packages->item_package_name !!}</b> ?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        {!! Form::open(['method' => 'POST', 'action' => 'ApiController@deleteFormulas']) !!}
                                                            <input type="hidden" name="id" value="{!! base64_encode($keys->item_package_formula_id) !!}">
                                                            {!! Form::submit("Ya", array('class' => 'btn btn-primary waves-effect waves-light')) !!}
                                                        {!! Form::close() !!}
                                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Number Part</th>
                                            <th>Nama Barang</th>
                                            <th>Qty</th>
                                            <th>Config</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                            <div class="dt-responsive table-responsive">
                                <table id="basic-btns" class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Tanggal</th>
                                            <th>Jumlah Proses</th>
                                            <th>Pemroses</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($history as $val => $keys)
                                        <tr>
                                            <td>{!! ++$val !!}</td>
                                            <td>{!! $keys->item_package_history_date !!}</td>
                                            <td>{!! $keys->item_package_history_qty !!}</td>
                                            <td>{!! $keys->users->user_first_name !!}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Tanggal</th>
                                            <th>Jumlah Proses</th>
                                            <th>Pemroses</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                        </div>

                    </div>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('script')
<!-- Select 2 js -->
<script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
<!-- Multiselect js -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/js/jszip.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/js/pdfmake.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/js/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/jszip.min.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/vfs_fonts.js') }}"></script>
<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>

<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js') }}"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function formatRepo(data) {
        var markup = "<option value='"+data.id+"'>"+data.text+"</option>";

        return markup;
    }

    function formatRepoSelection(data) {
        return data.text;
    }

    $(document).ready(function () {
        $("#basic-btns").DataTable();
        $("#form-hidden").hide();
        $("#form-hidden2").hide();
        $("#addRow").on('click', function () {
            $("#form-hidden").show();
        });

        $("#prosesFormulas").on('click', function () {
            $("#form-hidden2").show();
        });

        $("#reset").on('click', function () {
            $("#form-hidden").hide();
        });

        $("#reset2").on('click', function () {
            $("#form-hidden2").hide();
        });

        $("#select-barang").select2({
            ajax: {
                url: "{{ url('home/ajax/items') }}",
                dataType: 'JSON',
                delay: 100,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function(data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function(markup) {
                return markup;
            }, // let our custom formatter work
            placeholder: 'Pilih Barang',
            minimumInputLength: 1,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });
    });
</script>
@stop