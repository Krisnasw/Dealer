@extends('template.index')

@section('title')
Dealer Information System - Buat Paket Barang Baru
@stop

@section('style')

    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

@stop

@section('content')
<div class="main-body">
	<div class="page-wrapper">

        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Buat Paket Barang</h4>
                            <span>Buat Paket Barang Baru</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Master Data - Paket Barang</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

		<div class="page-body">
			<div class="row">
				<div class="col-sm-12">

					<!-- Tooltip Validation card start -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Buat Paket Barang Baru</h5>
                            <div class="card-header-right">
                            	<i class="icofont icofont-spinner-alt-5"></i>
                            </div>
                        </div>
                        <div class="card-block">
                        	{!! Form::open(['id' => 'second', 'method' => 'POST', 'novalidate' => 'novalidate', 'action' => 'PaketBarangController@store']) !!}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Number Part</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="number_part" placeholder="Masukkan No. Part">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nama Barang</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="f_nama" placeholder="Masukkan Nama Barang">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Merk</label>
                                    <div class="col-sm-10">
                                        <select class="col-sm-12 js-example-basic-single select2-hidden-accessible" name="merk">
                                            @foreach($brand as $key)
                                            <option value="{!! $key->brand_id !!}">{!! $key->brand_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Kode Part</label>
                                    <div class="col-sm-10">
                                        <select class="col-sm-12 js-example-basic-single select2-hidden-accessible" name="kodePart">
                                            @foreach($BrandDetail as $key)
                                            <option value="{!! $key->brand_detail_id !!}">{!! $key->brand_detail_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Motor</label>
                                    <div class="col-sm-10">
                                        <select class="js-example-basic-multiple col-sm-12" multiple="multiple" name="motor[]">
                                            @foreach($character as $key)
                                            <option value="{!! $key->character_id !!}">{!! $key->character_seri !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Satuan</label>
                                    <div class="col-sm-10">
                                        <select class="col-sm-12 js-example-basic-single select2-hidden-accessible" name="unit">
                                            @foreach($unit as $key)
                                            <option value="{!! $key->unit_id !!}">{!! $key->unit_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">H.E.T</label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" name="het" placeholder="Masukkan H.E.T">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Harga Pokok</label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" name="harga_pokok" placeholder="Masukkan Harga Pokok">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Harga Beli</label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" name="harga_beli" placeholder="Masukkan Harga Beli">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Stok Barang</label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" name="stok" placeholder="Masukkan Stok Barang">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Minimum Stok</label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" name="min_stok" placeholder="Masukkan Minimum Stok">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Maksimum Stok</label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" name="max_stok" placeholder="Masukkan Maksimum Stok">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Barcode</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="barcode" placeholder="Masukkan Barcode">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Berat</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="berat" placeholder="Masukkan Berat">
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('script')
<!-- Select 2 js -->
<script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
<!-- Multiselect js -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>
@stop