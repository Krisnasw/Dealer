@extends('template.index')

@section('title')
Dealer Information System - Edit Packaging
@stop

@section('style')

    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

@stop

@section('content')
<div class="main-body">
	<div class="page-wrapper">

        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Edit Packaging</h4>
                            <span>Edit Packaging - {!! $pack->packaging_sj !!}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Gudang - Packaging</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

		<div class="page-body">
			<div class="row">
				<div class="col-sm-12">

					<!-- Tooltip Validation card start -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Edit Packaging - {!! $pack->packaging_sj !!}</h5>
                            <div class="card-header-right">
                            	<i class="icofont icofont-spinner-alt-5"></i>
                            </div>
                        </div>
                        <div class="card-block">
                        	{!! Form::model($pack, ['id' => 'second', 'method' => 'PATCH', 'novalidate' => 'novalidate', 'action' => ['PackagingController@update', base64_encode($pack->packaging_id)]]) !!}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Faktur Penjualan</label>
                                    <div class="col-sm-10">
                                        <select class="js-example-basic-multiple col-sm-12" name="faktur" multiple="multiple">
                                            @foreach($nota as $row)
                                            <option value="{!! $row->nota_id !!}" {!! ($pack->nota_id == $row->nota_id) ? 'selected' : '' !!}>{!! $row->nota_code !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Tanggal Packaging</label>
                                    <div class="col-sm-10">
                                        <input type="date" class="form-control" name="tgl" placeholder="Masukkan Tanggal" value="{!! $pack->packaging_date !!}">
                                    </div>
                                </div>

                                <div id="loadview"></div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-success m-b-0">Submit</button>
                                        <button type="button" class="btn btn-primary m-b-0" onclick="preview();">Preview</button>
                                        <button type="button" class="btn btn-info m-b-0">Print</button>
                                        <button type="button" class="btn btn-danger m-b-0" onclick="history.go(-1);">Kembali</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('script')
<!-- Select 2 js -->
<script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
<!-- Multiselect js -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js') }}"></script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function print_barcode(data) {
        $.post('{{ url('home/ajax/generate-barcode') }}', {
            data : data
        }, function (data) {
            if (data.status == 'success') {

            } else {
                swal('Oops!', data.message, 'error');
            }
        });
    }

    function preview() {
        $("#loadview").load('{{ url('home/ajax/cardboard') }}/'+{!! $pack->packaging_id !!});
    }

    function add_kardus(id) {
        $.post('{{ url('home/ajax/add-kardus') }}', {
            id : {!! $pack->packaging_id !!}
        }, function (data) {
            if (data.status == 'success') {
                preview();
            } else {
                swal('Oops!', data.message, 'error');
            }
        });
    }

    function submitClicked() {
        swal('Clicked');
    }
</script>
@stop