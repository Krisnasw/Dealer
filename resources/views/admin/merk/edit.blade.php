@extends('template.index')

@section('title')
Dealer Information System - Edit Merk
@stop

@section('style')
@stop

@section('content')
<div class="main-body">
	<div class="page-wrapper">

        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Edit Merk</h4>
                            <span>Edit Merk - {!! $merk->brand_name !!}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Setup Data - Merk</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

		<div class="page-body">
			<div class="row">
				<div class="col-sm-12">

					<!-- Tooltip Validation card start -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Edit Merk - {!! $merk->brand_name !!}</h5>
                            <div class="card-header-right">
                            	<i class="icofont icofont-spinner-alt-5"></i>
                            </div>
                        </div>
                        <div class="card-block">
                        	{!! Form::model($merk, ['id' => 'second', 'method' => 'POST', 'novalidate' => 'novalidate', 'action' => ['BrandController@update', base64_encode($merk->brand_id)]]) !!}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Kode Merk</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="kode" placeholder="Masukkan Kode Merk" value="{!! $merk->brand_code !!}">
                                    </div>
                                    <label class="col-sm-2 col-form-label">Nama Merk</label>
                                    <div class="col-sm-4">
                                    	<input type="text" class="form-control" name="nama" placeholder="Masukkan Nama Merk" value="{!! $merk->brand_name !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Limit Cash</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="limitCash" placeholder="Masukkan Limit Cash" value="{!! $merk->brand_limit_cash !!}">
                                    </div>
                                    <label class="col-sm-2 col-form-label">Limit Kredit</label>
                                    <div class="col-sm-4">
                                    	<input type="number" class="form-control" name="limitKredit" placeholder="Masukkan Limit Kredit" value="{!! $merk->brand_limit_kredit !!}">
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-block">
                            <a href="javascript::void(0)" id="addRow" class="btn btn-primary m-b-20">+ Add Detail</a>

                            <div class="card" id="form-hidden">
                                <div class="card-block">
                                {!! Form::open(['method' => 'POST', 'action' => 'ApiController@addBrandDetail', 'id' => 'form-detail']) !!}
                                    <h4 class="sub-title">Form Detail</h4>
                                        <input type="hidden" name="id_brand" value="{!! $merk->brand_id !!}">
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Kode Part</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="kode" placeholder="Masukkan Kode Part">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Target</label>
                                            <div class="col-sm-10">
                                                <input type="number" class="form-control" name="target" placeholder="Masukkan Target" min="0">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <label class="col-sm-2"></label>
                                            <div class="col-sm-10">
                                                <button type="reset" class="btn btn-danger m-b-0" id="resets">Reset</button>
                                                <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                            </div>
                                        </div>
                                {!! Form::close() !!}
                                </div>
                            </div>

                            <div class="dt-responsive table-responsive">
                                <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kode Part</th>
                                            <th>Target ( % )</th>
                                            <th>Config</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($brandDetail as $val => $keys)
                                        <tr>
                                            <td>{!! ++$val !!}</td>
                                            <td>{!! $keys->brand_detail_name !!}</td>
                                            <td>{!! $keys->brand_detail_target !!}%</td>
                                            <td>
                                                <button type="button" class="btn btn-info btn-outline-info" data-toggle="modal" data-target="#update-target{{{ $keys->brand_detail_id }}}"><i class="icofont icofont-edit-alt"></i></button>
                                                <button type="button" class="btn btn-danger btn-outline-info" data-toggle="modal" data-target="#delete-target{{{ $keys->brand_detail_id }}}"><i class="icofont icofont-delete-alt"></i></button>
                                            </td>
                                        </tr>

                                        <div class="modal fade md-effect-1" id="update-target{!! $keys->brand_detail_id !!}" tabindex="-1" role="dialog">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header bg-info">
                                                        <h5 class="modal-title">Update Detail Merk</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                {!! Form::open(['method' => 'POST', 'action' => 'ApiController@updateBrandDetail']) !!}
                                                    <div class="modal-body">
                                                        <!-- Basic Form Inputs card start -->
                                                        <div class="card">
                                                            <div class="card-block">
                                                                <h4 class="sub-title">Form Update Detail Merk</h4>
                                                                    <input type="hidden" name="det_id" value="{!! $keys->brand_detail_id !!}">
                                                                    <input type="hidden" name="id_brand" value="{!! $merk->brand_id !!}">
                                                                    <div class="form-group row">
                                                                        <label class="col-sm-2 col-form-label">Kode Part</label>
                                                                        <div class="col-sm-10">
                                                                            <input type="text" class="form-control" name="kode" placeholder="Masukkan Kode Part" value="{!! $keys->brand_detail_name !!}">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group row">
                                                                        <label class="col-sm-2 col-form-label">Target</label>
                                                                        <div class="col-sm-10">
                                                                            <input type="number" class="form-control" name="target" placeholder="Masukkan Target" min="0" value="{!! $keys->brand_detail_target !!}">
                                                                        </div>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                        <!-- Basic Form Inputs card end -->
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger waves-effect " data-dismiss="modal">Close</button>
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                                    </div>
                                                {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="modal fade" id="delete-target{{{ $keys->brand_detail_id }}}" tabindex="-1" role="dialog">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header bg-danger">
                                                        <h4 class="modal-title">Hapus Data</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Apakah Anda Yakin Akan Menghapus <b>{!! $keys->brand_detail_name !!}</b> ?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        {!! Form::open(['method' => 'POST', 'action' => 'ApiController@deleteBrandDetail']) !!}
                                                            <input type="hidden" name="id" value="{!! base64_encode($keys->brand_detail_id) !!}">
                                                            {!! Form::submit("Ya", array('class' => 'btn btn-primary waves-effect waves-light')) !!}
                                                        {!! Form::close() !!}
                                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Kode Part</th>
                                            <th>Target ( % )</th>
                                            <th>Config</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('script')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function () {
        $("#form-hidden").hide();
        $("#addRow").on('click', function () {
            $("#form-hidden").show();
        });
        $("#resets").on('click', function () {
            $("#form-hidden").hide();
        });
    });
</script>
@stop