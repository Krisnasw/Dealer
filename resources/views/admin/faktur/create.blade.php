@extends('template.index')

@section('title')
    Dealer Information System - Buat Faktur Baru
@stop

@section('style')
    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

@stop

@section('content')
    <div class="main-body">
        <div class="page-wrapper">

            <!-- Page-header start -->
            <div class="page-header card">
                <div class="row align-items-end">
                    <div class="col-lg-8">
                        <div class="page-header-title">
                            <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                            <div class="d-inline">
                                <h4>Buat Faktur</h4>
                                <span>Buat Faktur Baru</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="page-header-breadcrumb">
                            <ul class="breadcrumb-title">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('home/main') }}">
                                        <i class="icofont icofont-home"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">Penjualan - Faktur</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Page-header end -->

            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">

                        <div id="selected_body">
                        <!-- Tooltip Validation card start -->
                        <div class="card">
                            <div class="card-header">
                                <h5>Buat Faktur Baru</h5>
                                <div class="card-header-right">
                                    <i class="icofont icofont-spinner-alt-5"></i>
                                </div>
                            </div>
                            <div class="card-block">
                                {!! Form::open(['id' => 'second', 'method' => 'POST', 'novalidate' => 'novalidate', 'action' => 'FakturController@store']) !!}
                                <div class="form-group row">

                                    <label class="col-sm-2 col-form-label">Pilih Customer</label>
                                    <div class="col-sm-4">
                                        <select class="col-sm-12 js-example-basic-single select2-hidden-accessible" name="customer" onchange="get_sales(this.value)" id="customer">
                                            <option selected>- Pilih Customer -</option>
                                            @foreach($cust as $key)
                                                <option value="{!! $key->customer_id !!}">{!! $key->customer_code !!} - {!! $key->customer_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                    <label class="col-sm-2 col-form-label">Pilih Sales</label>
                                    <div class="col-sm-4">
                                        <select class="col-sm-12 js-example-basic-single select2-hidden-accessible" name="sales" id="sales">
                                            <option selected>- Pilih Sales -</option>
                                            @foreach($sales as $key)
                                                <option value="{!! $key->sales_id !!}">{!! $key->sales_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>

                                <div class="form-group row">                                    
                                    <label class="col-sm-2 col-form-label">Pilih Merk</label>
                                    <div class="col-sm-4">
                                        <select class="col-sm-12 js-example-basic-single select2-hidden-accessible" id="merks" name="merk">
                                            <option selected>- Pilih Merk -</option>
                                            @foreach($brand as $key)
                                                <option value="{!! $key->brand_id !!}">{!! $key->brand_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <label class="col-sm-2 col-form-label">Tipe Pembayaran</label>
                                    <div class="col-sm-4">
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="type" value="0" onclick="tempo(1)"> Debet
                                            </label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="radio" name="type" value="1" onclick="tempo(2)"> Kredit
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nominal</label>
                                    <div class="col-sm-4">
                                        <input type="number" class="form-control" min="0" name="i_tempo">
                                    </div>
                                </div>

                                <div id="view-detail">
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="button" class="btn btn-primary m-b-0" onclick="submitData();">Submit</button>
                                        <button type="button" class="btn btn-info m-b-0" onclick="showData();">Preview</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <!-- Select 2 js -->
    <script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
    <!-- Multiselect js -->
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function formatRepo(data) {
            var markup = "<option value='"+data.id+"'>"+data.text+"</option>";

            return markup;
        }

        function formatRepoSelection(data) {
            return data.text;
        }

        $(document).ready(function () {

            $("#select-barang").select2({
                ajax: {
                    url: "{{ url('home/ajax/items') }}",
                    dataType: 'JSON',
                    delay: 100,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: {
                                more: (params.page * 30) < data.total_count
                            }
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                }, // let our custom formatter work
                placeholder: 'Pilih Barang',
                minimumInputLength: 2,
                templateResult: formatRepo, // omitted for brevity, see the source of this page
                templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
            });
        });

        function get_sales(id) {

            $.ajax({
              type: 'POST',
              url: '{{ url('home/ajax/sales/get') }}',
              data: { id:id },
              dataType: 'JSON',
              success: function(data) {
                $('#sales').html(data.content);
              }
            });

            $.ajax({
              type: 'POST',
              url: '{{ url('home/ajax/brand-dot') }}',
              data: { id:id },
              dataType: 'JSON',
              success: function(data) {
                $('#merks').html(data.content);
              }
            });

        }

        function tempo(id) {

            var brand = $("#merks").val();

            $.ajax({
                type: 'POST',
                url: '{{ url('home/ajax/brand-limit') }}',
                data: { brand : brand },
                dataType: 'JSON',
                success:function (data) {
                    if (id == 1) {
                        $('input[name="i_tempo"]').val(data.cash);
                    } else {
                        $('input[name="i_tempo"]').val(data.kredit);
                    }
                }
            });

        }

        function showData() {
            var sales = $("#sales").val();
            var customer = $("#customer").val();
            var brand = $("#merks").val();
            var i_type         = $('input[name="type"]:checked').val();
            var i_tempo         = $('input[name="i_tempo"]').val();

            $("#view-detail").load('{{ url('home/ajax/faktur-detail') }}/'+brand+'/'+customer+'/'+sales+'/'+i_type+'/'+i_tempo+'/view', function () {
                $("#select-barang").select2({
                    ajax: {
                        url: "{{ url('home/ajax/items') }}",
                        dataType: 'JSON',
                        delay: 100,
                        data: function (params) {
                            return {
                                q: params.term, // search term
                                page: params.page
                            };
                        },
                        processResults: function (data, params) {
                            // parse the results into the format expected by Select2
                            // since we are using custom formatting functions we do not need to
                            // alter the remote JSON data, except to indicate that infinite
                            // scrolling can be used
                            params.page = params.page || 1;

                            return {
                                results: data.items,
                                pagination: {
                                    more: (params.page * 30) < data.total_count
                                }
                            };
                        },
                        cache: true
                    },
                    escapeMarkup: function (markup) {
                        return markup;
                    }, // let our custom formatter work
                    placeholder: 'Pilih Barang',
                    minimumInputLength: 2,
                    templateResult: formatRepo, // omitted for brevity, see the source of this page
                    templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
                });
            });
            $("#selected_body").load('{{ url('home/ajax/faktur-detail') }}/'+brand+'/'+customer+'/'+sales+'/'+i_type+'/'+i_tempo+'/form');
        }

        function get_item_add(id) {
            $.ajax({
                type: 'POST',
                url: '{{ url('home/ajax/get-het') }}',
                data: { id : id},
                dataType: 'JSON',
                success:function (data) {
                    $("#het").val(data.content);
                }
            });
        }

        function add_detail_new() {
            var sales           = $("#sales").val();
            var customer        = $("#customer").val();
            var brand           = $("#merks").val();
            var i_type          = $('input[name="type"]:checked').val();
            var i_tempo         = $('input[name="i_tempo"]').val();
            var i_item          = $('select[name="barang"]').val();
            var i_het           = $('input[name="het"]').val();
            var i_diskon        = $('input[name="diskon-det"]').val();
            var i_fill          = $('input[name="quantity"]').val();

            $.post("{{ url('home/ajax/add-new-detail') }}",
            {
                sales: sales,
                customer: customer,
                brand: brand,
                type: i_type,
                tempo: i_tempo,
                item: i_item,
                het: i_het,
                diskon: i_diskon,
                fill: i_fill
            },
            function(data) {
                $("#view-detail").load('{{ url('home/ajax/faktur-detail') }}/'+brand+'/'+customer+'/'+sales+'/'+i_type+'/'+i_tempo+'/view/item');
                $("#selected_body").load('{{ url('home/ajax/faktur-detail') }}/'+brand+'/'+customer+'/'+sales+'/'+i_type+'/'+i_tempo+'/form');
            });
        }

        function submitData() {
            var sales           = $("#sales").val();
            var customer        = $("#customer").val();
            var brand           = $("#merks").val();
            var i_type          = $('input[name="type"]:checked').val();
            var i_tempo         = $('input[name="i_tempo"]').val();
            var grandTotal      = $('input[name="i_grand_total_real"]').val();
            var netto           = $('input[name="i_total_row_real"]').val();
            var diskon          = $('input[name="i_total_diskon_real"]').val();

            $.post('{{ url('home/penjualan/faktur') }}', {
                sales : sales,
                customer : customer,
                brand : brand,
                type : i_type,
                tempo : i_tempo,
                grandTotal : grandTotal,
                netto : netto,
                discount : diskon
            }, function (data) {
                if (data.status == 'error') {
                    swal('Oops!', data.message, 'error');
                } else {
                    swal('Successfully', data.message, 'success');
                    window.location.href = '{{ url('home/penjualan/faktur') }}';
                }
            });
        }
    </script>
@stop