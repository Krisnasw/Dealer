<script type="text/javascript">
    @foreach ($list as $row)
        function qty{!! $row['memo_detail_id'] !!} (data) {
            var permintaan = parseFloat($('input[name="i_permintaan{!! $row['memo_detail_id'] !!}"]').val());

            if ( data > permintaan ) {
                swal('Oops!', 'Jumlah Kirim Tidak Boleh Melebihi Permintaan', 'error');
                $('input[name="i_fill{!! $row['memo_detail_id'] !!}"]').val(0);
            } else {
                var harga = {!! $row['nota_detail_price'] !!};
                var diskon = {!! $row['nota_detail_discount'] !!};

                var total_diskon = diskon / 100 * harga * data;
                var total_harga = harga * data /*- total_diskon*/;
                var format_number = total_harga.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

                var id = {!! $row['nota_detail_id'] !!};
                var item_id = {!! $row['item_id'] !!};

                $.ajax({
                    type: 'POST',
                    url: '{{ url('home/ajax/faktur-detail/update-tmp') }}',
                    data: {id:id,harga:harga,total_diskon:total_diskon,total_harga:total_harga,data:data,item_id:item_id},
                    dataType: 'JSON',
                    success: function (data) {
                        var cek = (data.content['cek']);
                        var total = (data.content['nota_detail_price']);
                        var diskon = (data.content['nota_detail_discount']);

                        if (cek) {
                            swal('Oops!', 'Barang Tidak Boleh Sama', 'info');
                            $('input[name="i_fill{!! $row['memo_detail_id'] !!}"]').val(0);
                        } else {
                            $('input[name="i_total{!! $row['memo_detail_id'] !!}"]').val(total);
                            $('input[name="i_diskon{!! $row['memo_detail_id'] !!}"]').val(diskon); 
                            $('input[name="i_total_show{!! $row['memo_detail_id'] !!}"]').val(format_number); 

                            var total_row = parseFloat(0) +
                            @foreach ($list as $row2)
                                parseFloat($('input[name="i_total{!! $row2['memo_detail_id'] !!}"]').val()) +
                            @endforeach
                            parseFloat(0);

                            var total_row_format = total_row.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                            $('input[name="i_total_row').val(total_row_format); 
                            $('input[name="i_total_row_real').val(total_row);

                            var total_diskon = parseFloat(0) +
                            @foreach($list as $row3)
                                parseFloat($('input[name="i_diskon{!! $row3['memo_detail_id'] !!}"]').val());
                            @endforeach
                            parseFloat(0);

                            var total_diskon_format = total_diskon.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                            $('input[name="i_total_diskon').val(total_diskon_format); 
                            $('input[name="i_total_diskon_real').val(total_diskon); 

                            var diskon_global = $('input[name="i_discount').val() / 100 * total_row;
                            var total_diskon_global_format = diskon_global.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                            $('input[name="i_total_diskon_global').val(total_diskon_global_format);

                            var grand_total = total_row - total_diskon - diskon_global;
                            var grand_total_format = grand_total.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                            $('input[name="i_grand_total').val(grand_total_format);
                            $('input[name="i_grand_total_real').val(grand_total);
                        }
                    }
                });
            }
        }
    @endforeach

    function get_discount(value) {

        var total           = $('input[name="i_total_row_real"]').val();
        var diskon_detail   = $('input[name="i_total_diskon_real"]').val();

        var diskon = value / 100 * total;

        var total_diskon_format = diskon.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $('input[name="i_total_diskon_global').val(total_diskon_format); 

        var grand_total = total - diskon_detail - diskon;
        var grand_total_format = grand_total.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $('input[name="i_grand_total"]').val(grand_total_format);
        $('input[name="i_grand_total_real"]').val(grand_total);
    }

    function get_item_subs(item_id) {
        $.ajax({
            type: 'GET',
            url: '{{ url('home/ajax/item-sub') }}/'+item_id,
            dataType: 'JSON',
            success:function (data) {
                $("#barangs").html(data.content);
            }
        });
    }

    function postSubstitusiItem() {
        var id = $("#barangs").val();
        var memo_id = $('input[name="memo_id"]').val();
        var itemChanged = $('input[name="itemChanged"]').val();

        $.post('{{ url('home/ajax/updateMemoItem') }}', {
            id : id,
            memo_id : memo_id,
            itemChanged : itemChanged
        }, function(data) {
            if (data.status == 'success') {
                $('#subs-mod'+memo_id).modal('hide');
                $('.modal-backdrop').remove();
                swal('Done', data.message, 'success');
                showData();
            } else {
                $('#subs-mod'+memo_id).modal('hide');
                $('.modal-backdrop').remove();
                swal('Oops!', data.message, 'error');
                showData();
            }
        });
    }

    function deleteSubstitusiItem() {
        var id = $('input[name="itemDeleted"]').val();
        var memo_id = $('input[name="memo_id"]').val();

        $.post('{{ url('home/ajax/deleteMemoItem') }}', {
            id : id,
            memo_id : memo_id
        }, function (data) {
            if (data.status == 'success') {
                $('#delete-target'+memo_id).modal('hide');
                $('.modal-backdrop').remove();
                swal('Done', data.message, 'success');
                showData();
            } else {
                $('#delete-target'+memo_id).modal('hide');
                $('.modal-backdrop').remove();
                swal('Oops!', data.message, 'error');
                showData();
            }
        });
    }
</script>

<div class="card">
    <div class="card-block">
        <div class="dt-responsive table-responsive">
            <table id="basic-btn" class="table table-striped table-bordered nowrap">
                <thead>
                    <tr>
                        <td>Input Manual</td>
                        <td></td>
                        <td>
                            <select class="form-control" id="select-barang" name="barang" onchange="get_item_add(this.value);"></select>
                        </td>
                        <td></td>
                        <td>
                            <input type="number" class="form-control" min="0" name="diskon-det">
                        </td>
                        <td>
                            <input type="number" class="form-control" min="0" readonly="readonly" name="het" id="het">
                        </td>
                        <td></td>
                        <td>
                            <input type="number" min="0" name="quantity" class="form-control" onkeydown="if (event.keyCode == 13) { add_detail_new(); }">
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <th>Kode Memo</th>
                        <th>Tanggal Memo</th>
                        <th>Nama Barang</th>
                        <th>Perm</th>
                        <th>Disk</th>
                        <th>HET</th>
                        <th>Est</th>
                        <th>Dikirim</th>
                        <th>Config</th>
                    </tr>
                </thead>
                <tbody id="tbody-isi">
                @php $total_row = 0; @endphp
                @php $total_diskon = 0; @endphp
                @foreach($list as $val => $keys)

                    @php
                        $minta = $keys['memo_detail_qty'] - $keys['memo_accumulation'] - $keys['memo_detail_cancel'];
                    @endphp

                    @if ($keys['item_id_memo'])
                        @php $item_memo_id = $keys['item_id_memo']; @endphp
                    @else
                        @php $item_memo_id = 0; @endphp
                    @endif

                    @if(isset($keys['user_id']))
                        @php $list_count = Access::list_detail_tmp_count($keys['user_id'], $item_memo_id); @endphp
                        @foreach ($list_count as $row2)
                            @if($row2['nota_detail_id'] == $keys['nota_detail_id'])
                                @php $img = 1; @endphp
                            @else
                                @php $img = 2; @endphp
                            @endif
                        @endforeach
                    @endif

                    @if (isset($img))
                        @php $img_warning = $img; @endphp
                    @else
                        @php $img_warning = 2; @endphp
                    @endif

                    @if ($keys['nota_detail_price'])
                        @php $price = $keys['nota_detail_price']; @endphp
                    @else
                        @php $price = $keys['item_het']; @endphp
                    @endif

                    @if ($keys['nota_detail_discount'])
                        @php $diskon = $keys['nota_detail_discount'] / $keys['nota_detail_total'] * 100; @endphp
                    @else
                        @php $diskon = $keys['memo_detail_discount']; @endphp
                    @endif

                    @php
                        $substitue = Access::cek_substitute($keys['item_id']);
                    @endphp

                    <tr>
                        <td>{!! $keys->memo_code !!}</td>
                        <td>{!! $keys->memo_date !!}</td>
                        <td>{!! $keys->item_name !!}</td>
                        <td>{!! $minta !!}</td>
                        <td>{!! $diskon  !!}</td>
                        <td>{!! number_format($price) !!}</td>
                        <td>{!! $keys->nota_detail_est !!}</td>
                        <td>
                            <input type="number" class="form-control" onchange="qty{!! $keys->memo_detail_id !!}(this.value)" value="{!! $keys->nota_fill_qty !!}" name="i_fill{!! $keys->memo_detail_id !!}">
                            <input type="hidden" name="i_total{!! $keys['memo_detail_id'] !!}" value="0">
                            <input type="hidden" name="i_diskon{!! $keys['memo_detail_id'] !!}" value="0">
                            <input type="hidden" name="i_het{!! $keys['memo_detail_id'] !!}" value="{!! $price !!}">
                            <input type="hidden" name="i_permintaan{!! $keys['memo_detail_id'] !!}" value="{!! $minta !!}">
                        </td>
                        <td>
                            @if ($substitue)
                            <button type="button" class="btn btn-primary btn-outline-primary" data-toggle="modal" data-target="#subs-mod{{{ $keys->memo_detail_id }}}" onclick="get_item_subs({!! $keys->item_id !!});"><i class="icofont icofont-refresh"></i></button>
                            @endif
                            <button type="button" class="btn btn-danger btn-outline-info" data-toggle="modal" data-target="#delete-target{{{ $keys->memo_detail_id }}}"><i class="icofont icofont-delete-alt"></i></button>
                        </td>
                    </tr>

                    <div class="modal fade" id="subs-mod{{{ $keys->memo_detail_id }}}" tabindex="-1" role="dialog">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header bg-info">
                                    <h4 class="modal-title">Tukar Barang</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="card-block">
                                            <div class="form-group row">
                                                <input type="hidden" name="itemChanged" value="{!! $keys->item_id !!}">
                                                <input type="hidden" class="form-control" name="memo_id" value="{!! $keys->memo_detail_id !!}">
                                                <label class="col-sm-2 col-form-label">Pilih Barang Pengganti</label>
                                                <div class="col-sm-10">
                                                    <select class="col-sm-12 form-control" name="item_subs" id="barangs">
                                                        <option value="0" selected>- Pilih Barang -</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary waves-effect waves-light" onclick="postSubstitusiItem();">Submit</button>
                                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="delete-target{{{ $keys->memo_detail_id }}}" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header bg-danger">
                                    <h4 class="modal-title">Hapus Data</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p>Apakah Anda Yakin Akan Menghapus <b>{!! $keys->item_name !!}</b> ?</p>
                                    <input type="hidden" name="itemDeleted" value="{!! $keys->item_id !!}">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary waves-effect waves-light" onclick="deleteSubstitusiItem();">Submit</button>
                                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @php $total_row += $keys['nota_detail_price']; @endphp
                    @php $total_diskon += $keys['nota_detail_discount']; @endphp
                @endforeach
                </tbody>
                <tfoot>
                @php $grand_total = $total_row - $total_diskon; @endphp
                @php $diskon2 = 0; @endphp
                <tr>
                    <th colspan="7" style="text-align:right;">Total</th>
                    <th style="text-align:right;" colspan="2">
                        <input style="text-align: right; border: none; background: transparent;" type="text" readonly="readonly" class="form-control" value="{!! number_format($total_row, 2) !!}" name="i_total_row">
                        <input type="hidden" class="form-control" value="{!! $total_row !!}" name="i_total_row_real">
                    </th>
                </tr>
                <tr>
                    <th colspan="7" style="text-align:right;">Diskon</th>
                    <th style="text-align:right;" colspan="2">
                        <input style="text-align: right; border: none; background: transparent;" type="text" readonly="readonly" class="form-control" value="{!! number_format($total_diskon, 2) !!}" name="i_total_diskon">
                        <input type="hidden" class="form-control" value="{!! $total_diskon !!} " name="i_total_diskon_real"></th>
                    </th>
                </tr>
                <tr>
                    <th colspan="6" style="text-align:right;">Diskon Global</th>
                    <th><input type="number" min="0" style="width: 90px;" class="form-control" value="{!! $diskon2 !!}" name="i_discount" onchange="get_discount(this.value)"></th>
                    <th style="text-align:right" colspan="2">
                        <input style="text-align: right; border: none; background: transparent;" type="text" readonly="readonly" class="form-control" value="{!! number_format($diskon2 / 100 * $total_row, 2) !!}" name="i_total_diskon_global">
                    </th>
                </tr>
                <tr>
                    <th colspan="7" style="text-align:right">Total Nota Akhir</th>
                    <th style="text-align:right" colspan="2"><input style="text-align: right; border: none; background: transparent;" type="text" readonly="readonly" class="form-control" value="{!! number_format($grand_total, 2) !!}" name="i_grand_total">
                        <input type="hidden" class="form-control" value="{!! $grand_total !!}" name="i_grand_total_real">
                    </th>
                </tr>
                </tfoot>
            </table>

        </div>
    </div>
</div>