@extends('template.index')

@section('title')
Dealer Information System - Buat Delivery Baru
@stop

@section('style')

    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

@stop

@section('content')
<div class="main-body">
	<div class="page-wrapper">

        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Buat Delivery</h4>
                            <span>Buat Delivery Baru</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Gudang - Delivery</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

		<div class="page-body">
			<div class="row">
				<div class="col-sm-12">

					<!-- Tooltip Validation card start -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Buat Delivery Baru</h5>
                            <div class="card-header-right">
                            	<i class="icofont icofont-spinner-alt-5"></i>
                            </div>
                        </div>
                        <div class="card-block">
                        	{!! Form::open(['id' => 'second', 'method' => 'POST', 'novalidate' => 'novalidate', 'action' => 'DeliveryController@store']) !!}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Surat Jalan</label>
                                    <div class="col-sm-10">
                                        <select class="js-example-basic-multiple col-sm-12" name="sj" id="sj" multiple="multiple">
                                            <option value="0" selected>-- Pilih Surat Jalan --</option>
                                            @foreach($packaging as $row)
                                            <option value="{!! $row->packaging_id !!}">{!! $row->packaging_sj !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Tanggal Pengiriman</label>
                                    <div class="col-sm-10">
                                        <input type="date" class="form-control" name="tgl" placeholder="Masukkan Tanggal">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Sopir</label>
                                    <div class="col-sm-10">
                                        <select class="js-example-basic-single col-sm-12" name="sopir" id="sopir">
                                            <option value="0">-- Pilih Sopir --</option>
                                            @foreach($sopir as $row)
                                            <option value="{!! $row->employee_id !!}">{!! $row->employee_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Kernet</label>
                                    <div class="col-sm-10">
                                        <select class="js-example-basic-single col-sm-12" name="kernet" id="kernet">
                                            <option value="0">-- Pilih Kernet --</option>
                                            @foreach($kernet as $row)
                                            <option value="{!! $row->employee_id !!}">{!! $row->employee_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-success m-b-0">Simpan</button>
                                        <button type="button" class="btn btn-danger m-b-0" onclick="history.go(-1);">Selesai</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('script')
<!-- Select 2 js -->
<script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
<!-- Multiselect js -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

<script src="{{ asset('assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js') }}"></script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
@stop