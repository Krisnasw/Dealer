<script type="text/javascript">
    @foreach($list_detail as $row)
        function diskon{!! $row['purchase_detail_id'] !!}(diskon) {
            var id = {!! $row['purchase_detail_id'] !!};

            $.post('{{ url('home/pembelian/purchase/edit-diskon') }}', {
                id : id,
                diskon : diskon
            }, function (data) {
                if (data.status == 'error') {
                    swal('Oops!', data.message, 'error');
                } else {

                }
            });

            var harga = "{!! $row['purchase_detail_price'] !!}";
            var qty = $('input[name="i_qty_detail{!! $row['purchase_detail_id'] !!}"]').val();

            var total_diskon = diskon / 100 * harga * qty;
            var total_harga = harga * qty;
            var format_number = total_harga.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            var format_number_diskon = total_diskon.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

            $('input[name="i_total{!! $row['purchase_detail_id'] !!}"]').val(total_harga);
            $('input[name="i_total_show{!! $row['purchase_detail_id'] !!}"]').val(format_number);
            $('input[name="i_total_diskon{!! $row['purchase_detail_id'] !!}"]').val(format_number_diskon); 
            $('input[name="i_total_diskon_real{!! $row['purchase_detail_id'] !!}"]').val(total_diskon);

            var total_row = parseFloat(0) +
            @foreach($list_detail as $row2)
                parseFloat($('input[name="i_total{!! $row2['purchase_detail_id'] !!}"]').val()) +
            @endforeach 
            parseFloat(0);

            var total_row_format = total_row.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            $('input[name="i_total_row').val(total_row_format);
            $('input[name="i_total_row_real').val(total_row);

            var total_diskon = parseFloat(0) +
            @foreach($list_detail as $row3)
                parseFloat($('input[name="i_total_diskon_real{!! $row3['purchase_detail_id'] !!}"]').val()) +
            @endforeach
            parseFloat(0);

            var total_diskon_format = total_diskon.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            $('input[name="i_total_row_diskon').val(total_diskon_format);
            $('input[name="i_total_row_diskon_real').val(total_diskon); 

            var grand_total = total_row - total_diskon;
            var grand_total_format = grand_total.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            $('input[name="i_grand_total').val(grand_total_format);
            $('input[name="i_netto').val(grand_total);
        }

        function qty_detail{!! $row['purchase_detail_id'] !!}(qty) {
            var id = {!! $row['purchase_detail_id'] !!};
            var harga = {!! $row['purchase_detail_price'] !!};

            $.post('{{ url('home/pembelian/purchase/edit-qty') }}', {
                id : id,
                harga : harga,
                qty : qty
            }, function (data) {
                // swal('Oops!', data.message, 'error');
            });

            var diskon = $('input[name="i_diskon{!! $row['purchase_detail_id'] !!}"]').val();"{!! $row['purchase_detail_qty'] !!}";
            var total_diskon = diskon / 100 * harga * qty;
            var total_harga = harga * qty;
            var format_number = total_harga.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            var format_number_diskon = total_diskon.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");

            $('input[name="i_total{!! $row['purchase_detail_id'] !!}"]').val(total_harga);
            $('input[name="i_total_show{!! $row['purchase_detail_id'] !!}"]').val(format_number);
            $('input[name="i_total_diskon{!! $row['purchase_detail_id'] !!}"]').val(format_number_diskon);
            $('input[name="i_total_diskon_real{!! $row['purchase_detail_id'] !!}"]').val(total_diskon);

            var total_row = parseFloat(0) +
            @foreach($list_detail as $row2)
                parseFloat($('input[name="i_total{!! $row2['purchase_detail_id'] !!}"]').val()) +
            @endforeach
            parseFloat(0);

            var total_row_format = total_row.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            $('input[name="i_total_row').val(total_row_format);
            $('input[name="i_total_row_real').val(total_row);

            var total_diskon = parseFloat(0) +
            @foreach($list_detail as $row3)
                parseFloat($('input[name="i_total_diskon_real{!! $row3['purchase_detail_id'] !!}"]').val()) +
            @endforeach 
            parseFloat(0);

            var total_diskon_format = total_diskon.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            $('input[name="i_total_row_diskon').val(total_diskon_format);
            $('input[name="i_total_row_diskon_real').val(total_diskon); 

            var grand_total = total_row - total_diskon;
            var grand_total_format = grand_total.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            $('input[name="i_grand_total').val(grand_total_format);
            $('input[name="i_netto').val(grand_total);
        }
    @endforeach

    function hitung_total_diskon(data) {
        var total = $('input[name="i_total_row_real').val(); 
        var diskon_item = $('input[name="i_total_row_diskon_real').val();
        var total_tmp = total - diskon_item;

        var hitung_diskon =  data / 100 * total_tmp;

        var all_diskon_format = hitung_diskon.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $('input[name="i_all_diskon').val(all_diskon_format);
        $('input[name="i_all_diskon_real').val(hitung_diskon);

        var grand_total = total - diskon_item - hitung_diskon;
        var grand_total_format = grand_total.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        $('input[name="i_grand_total').val(grand_total_format);
        $('input[name="i_netto').val(grand_total);
    }

    function cek_ppn(id) {
        var ppn = $('input[name="i_cek_ppn').val();
        var total = $('input[name="i_total_row_real').val(); 
        var diskon_item = $('input[name="i_total_row_diskon_real').val();
        var all_diskon = $('input[name="i_all_diskon_real').val();

        var total_tmp = total - diskon_item - all_diskon;

        if (id.checked) {
            var total_ppn = 10 / 100 * (total_tmp)
            var ppn_format = total_ppn.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            $('input[name="i_ppn').val(ppn_format);
            $('input[name="i_cek_ppn').val(10);

            var grand_total = total_tmp + total_ppn;
            var grand_total_format = grand_total.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            $('input[name="i_grand_total').val(grand_total_format);
            $('input[name="i_netto').val(grand_total);

        }else{

            var grand_total = total_tmp;
            var grand_total_format = grand_total.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            $('input[name="i_grand_total').val(grand_total_format);
            $('input[name="i_netto').val(grand_total);
          
            $('input[name="i_cek_ppn').val(0);
            $('input[name="i_ppn').val(0);
        }

    }

    function deleteSubstitusiItem(id) {
        $.post('{{ url('home/pembelian/purchase/delete-detail') }}', {
            id : id
        }, function (data) {
            if (data.status == 'success') {
                swal('Done', data.message, 'success');
                window.location.reload();
            } else {
                swal('Oops!', data.message, 'error');
                window.location.reload();
            }
        });
    }
</script>

<div class="card">
    <div class="card-header table-card-header">
        <h5>List Detail Pembelian</h5>
    </div>
    <div class="card-block">
        <div class="dt-responsive table-responsive">
            <table id="basic-btn" class="table table-striped table-bordered nowrap">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Number Part</th>
                        <th>Nama Barang</th>
                        <th>Qty</th>
                        <th>Harga</th>
                        <th>Diskon</th>
                        <th>Jml Diskon</th>
                        <th>Total</th>
                        <th>Config</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td></td>
                        <td>
                            <select class="form-control col-sm-12 js-example-basic-single" name="numberPart" id="numberPart">
                                <option value="0">-- Pilih Kode Barang --</option>
                            </select>
                        </td>
                        <td>
                            <select class="form-control col-sm-12 js-example-basic-single" name="namaBarang" id="select-barang" onchange="get_item_id(1);">
                                <option value="0">-- Pilih Jenis Barang --</option>
                            </select>
                        </td>
                        <td>
                            <input type="number" class="form-control" name="i_qty_detail" id="i_qty_detail" min="0" value="1" onkeydown="if (event.keyCode == 13) { save_tmp(); }">
                        </td>
                        <td></td>
                        <td>
                            <input type="number" class="form-control" name="i_detail_diskon" id="i_detail_diskon" min="0" value="0" onkeydown="if (event.keyCode == 13) { save_tmp(); }">
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                @php $total_row = 0; @endphp
                @php $total_diskon = 0; @endphp
                @foreach($list_detail as $val => $keys)
                @php $diskon = $keys['purchase_detail_discount'] / 100 * $keys['purchase_detail_price'] * $keys['purchase_detail_qty']; @endphp
                    <tr>
                        <td>{!! ++$val !!}</td>
                        <td>{!! $keys->item->item_code !!}</td>
                        <td>{!! $keys->item->item_name !!}</td>
                        <td><input type="text" class="form-control" onchange="qty_detail{!! $keys['purchase_detail_id'] !!}(this.value)" value="{!! $keys['purchase_detail_qty'] !!}" name="i_qty_detail{!! $keys['purchase_detail_id'] !!}" readonly="readonly" style="background: transparent; border: 0;"></td>
                        <td>{!! number_format($keys->purchase_detail_price) !!}</td>
                        <td>
                            <input type="number" onchange="diskon{!! $keys['purchase_detail_id'] !!}(this.value);" class="form-control" name="i_diskon{!! $keys['purchase_detail_id'] !!}" value="{!! $keys->purchase_detail_discount  !!}" min="{!! $keys->purchase_detail_discount  !!}">
                        </td>
                        <td>
                            <input style="text-align: right; border: none; background: transparent;" type="text" readonly="readonly" class="form-control" value="{!! number_format($diskon, 2) !!}" name="i_total_diskon{!! $keys['purchase_detail_id'] !!}">
                            <input type="hidden" name="i_total{!! $keys['purchase_detail_id'] !!}" value="{!! $keys['purchase_detail_total'] !!}">
                            <input type="hidden" name="i_total_diskon_real{!! $keys['purchase_detail_id'] !!}" value="{!! $diskon !!}">
                        </td>
                        <td>
                            <input style="text-align: right; border: none; background: transparent;" type="text" readonly="readonly" class="form-control" value="{!! number_format($keys['purchase_detail_total'], 2) !!}" name="i_total_show{!! $keys['purchase_detail_id'] !!}">
                        </td>
                        <td>
                            <button type="button" class="btn btn-danger btn-outline-info" data-toggle="modal" data-target="#delete-target{{{ $keys->purchase_detail_id }}}"><i class="icofont icofont-delete-alt"></i></button>
                        </td>
                    </tr>

                    <div class="modal fade" id="delete-target{{{ $keys->purchase_detail_id }}}" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header bg-danger">
                                    <h4 class="modal-title">Hapus Data</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p>Apakah Anda Yakin Akan Menghapus <b>{!! $keys->item->item_name !!}</b> ?</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary waves-effect waves-light" onclick="deleteSubstitusiItem({!! $keys->purchase_detail_id !!});">Submit</button>
                                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @php $total_row += $row['purchase_detail_total']; @endphp
                @php $total_diskon += $diskon; @endphp
                @endforeach
                </tbody>
                <tfoot>
                @php $total_tmp = $total_row - $total_diskon; @endphp
                @php $all_diskon = 0 / 100 * $total_tmp; @endphp
                @php $ppn = 0 / 100 * ($total_tmp - $all_diskon); @endphp
                @php $grand_total = $total_tmp - $all_diskon + $ppn; @endphp
                <tr>
                    <th colspan="7" style="text-align:right;">Total</th>
                    <th style="text-align:right" colspan="2">
                        <input style="text-align: right; border: none; background: transparent;" type="text" readonly="readonly" class="form-control" value="{!! number_format($total_row, 2) !!}" name="i_total_row">
                    <input type="hidden" name="i_total_row_real" value="{!! $total_row !!}"></th>
                </tr>
                <tr>
                    <th colspan="7" style="text-align:right;">Diskon</th>
                    <th style="text-align:right" colspan="2">
                        <input style="text-align: right; border: none; background: transparent;" type="text" readonly="readonly" class="form-control" value="{!! number_format($total_diskon, 2) !!}" name="i_total_row_diskon">
                        <input type="hidden" name="i_total_row_diskon_real" value="{!! $total_diskon !!}">
                    </th>
                </tr>
                <tr>
                    <th colspan="6" style="text-align:right;">Diskon Total</th>
                    <th>
                        <input style="text-align: right;" type="number" class="form-control" onchange="hitung_total_diskon(this.value)" value="0" name="i_purchase_diskon" min="0"></th>
                    <th style="text-align:right" colspan="2"><input style="text-align: right; border: none; background: transparent;" type="text" readonly="readonly" class="form-control" value="{!! number_format($all_diskon, 2) !!}" name="i_all_diskon">
                          <input type="hidden" name="i_all_diskon_real" value="{!! $all_diskon !!}">
                    </th>
                </tr>
                <tr>
                    <th colspan="7" style="text-align:right">PPN 
                        <input id="checkboxDark" type="checkbox" value="0" onchange="cek_ppn(this)" class="checkradios checkradios-dark-1">
                    </th>
                    <th style="text-align:right" colspan="2">
                        <input style="text-align: right; border: none; background: transparent;" type="text" readonly="readonly" class="form-control" value="{!! number_format($ppn, 2) !!}" name="i_ppn">
                        <input type="hidden" name="i_cek_ppn" value="0">
                    </th>
                </tr>
                <tr>
                    <th colspan="7" style="text-align:right">Total Akhir</th>
                    <th style="text-align:right" colspan="2">
                        <input style="text-align: right; border: none; background: transparent;" type="text" readonly="readonly" class="form-control" value="{!! number_format($grand_total, 2) !!}" name="i_grand_total">
                    <input type="hidden" name="i_netto" value="{!! $grand_total !!}"></th>
                </tr>
                </tfoot>
            </table>

        </div>
    </div>
</div>