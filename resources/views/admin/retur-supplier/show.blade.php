@extends('template.index')

@section('title')
Pembelian - Retur Supplier
@stop

@section('style')
<!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">
@stop

@section('content')
<div class="main-body">
	<div class="page-wrapper">

        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Buat Pembelian - Retur Supplier</h4>
                            <span>Buat Pembelian - Retur Supplier</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Pembelian - Retur Supplier</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

		<div class="page-body">
			<div class="row">
				<div class="col-sm-12">

					<!-- Tooltip Validation card start -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Buat Pembelian - Retur Supplier</h5>
                            <div class="card-header-right">
                            	<i class="icofont icofont-spinner-alt-5"></i>
                            </div>
                        </div>
                        <div class="card-block">
                        	{!! Form::model($retur, ['id' => 'second', 'method' => 'POST', 'novalidate' => 'novalidate', 'action' => ['ReturSupController@update', base64_encode($retur['retur_id'])]]) !!}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Kode DO</label>
                                    <div class="col-sm-10">
                                        <select class="form-control js-example-basic-single" name="kode_do">
                                            <option value="0">-- Pilih DO --</option>
                                            @foreach($do as $row)
                                            <option value="{!! $row['receipt_id'] !!}" {!! ($row['receipt_id'] == $retur['retur_data_id']) ? 'selected' : '' !!}>{!! $row['receipt_code'] !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Type Retur</label>
                                    <div class="col-sm-10">
                                        <div class="form-radio">
                                            <div class="radio radiofill radio-primary radio-inline">
                                                <label>
                                                    <input type="radio" name="jenis" value="1" {!! ($retur['retur_type_saldo'] == 1) ? 'checked' : '' !!}>
                                                    <i class="helper"></i>Cash
                                                </label>
                                            </div>
                                            <div class="radio radiofill radio-primary radio-inline">
                                                <label>
                                                    <input type="radio" name="jenis" value="2" {!! ($retur['retur_type_saldo'] == 2) ? 'checked' : '' !!}>
                                                    <i class="helper"></i>Saldo
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header table-card-header">
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            <select class="form-control js-example-basic-single" name="i_kodeps" onchange="get_part(this.value)">
                                                                <option value="0" selected>-- Pilih Kode PS --</option>
                                                                @foreach($kodeps as $row)
                                                                <option value="{!! $row['receipt_cardboard_id'] !!}">{!! $row['receipt_cardboard_barcode'] !!}</option>
                                                                @endforeach
                                                            </select>
                                                        </th>
                                                        <th>
                                                            <select name="i_receipt_detail" id="select2Default" class="form-control js-example-basic-single" style="width:100%;" onchange="get_qty(this.value)">
                                                                <option value="0">-- Pilih Number Part --</option>
                                                            </select>
                                                        </th>
                                                        <th>
                                                            <input type="text" id="i_qty" class="form-control" name="i_qty" value="" style="border: none;background: transparent;" readonly="">
                                                        </th>
                                                        <th>
                                                            <select name="i_status_detail" class="form-control js-example-basic-single" style="width:100%;">
                                                                <option value="0">Diterima</option>
                                                                <option value="1">Belum Diterima</option>
                                                            </select>
                                                        </th>
                                                        <td>
                                                            <input type="number" id="i_retur" class="form-control" name="i_retur" value="" onkeypress="if (event.keyCode == 13) { save_detail({!! $retur['retur_id'] !!},this.value); }">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Kode PS</th>
                                                        <th>Number Part</th>
                                                        <th>Dikirim</th>
                                                        <th>Status</th>
                                                        <th>Retur</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @php $grand_total = 0; @endphp
                                                @foreach($list as $data => $row)
                                                @if ($row['retur_detail_status'] == 0)
                                                    @php $status = "Diterima"; @endphp
                                                @else
                                                    @php $status = "Belum Diterima"; @endphp
                                                @endif

                                                @php $total = $row['retur_detail_qty'] * $row['purchase_detail_price']; @endphp
                                                @php $grand_total += $total; @endphp
                                                    <tr>
                                                        <td>{!! $row['receipt_cardboard_barcode'] !!}</td>
                                                        <td>{!! $row['item_code'] !!}</td>
                                                        <td>{!! $row['purchase_detail_qty'] !!}</td>
                                                        <td>{!! $status !!}</td>
                                                        <td align="center">
                                                            {!! $row['retur_detail_qty'] !!}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="button" class="btn btn-primary m-b-0" onclick="history.go(-1);">Kembali</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('script')
    <!-- Select 2 js -->
    <script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
    <!-- Multiselect js -->
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function get_part(id) {
            $.ajax({
              type: 'POST',
              url: '{{ url('home/pembelian/retur-supplier/read-part') }}',
              data: { id : id },
              dataType: 'JSON',
              success: function(data) {
                $('#select2Default').html(data.content);
              } 
            });
        }

        function get_qty(id) {
            $.ajax({
              type: 'POST',
              url: '{{ url('home/pembelian/retur-supplier/read-qty') }}',
              data: { id : id },
              dataType: 'JSON',
              success: function(data) {
                $('input[name="i_qty"]').val(data.content);
              } 
            });
        }

        function save_detail(id,value) {
            var data_id       = $('select[name="i_receipt_detail"]').val();
            var status        = $('select[name="i_status_detail"]').val();

            $.ajax({
                type: 'POST',
                url: '{{ url('home/pembelian/retur-supplier/save-detail') }}',
                data: { id : id, value : value, data_id : data_id, status : status },
                dataType: 'JSON',
                success: function(data) {
                    if (data.status == 'success') {
                        swal('Success', data.message, 'success');
                        window.location.reload();
                    } else {
                        swal('Oops!', data.message, 'error');
                    }
                }
            });
        }
    </script>
@stop