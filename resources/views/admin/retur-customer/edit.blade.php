@extends('template.index')

@section('title')
    Dealer Information System - Edit Retur Customer
@stop

@section('style')

    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/css/buttons.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css') }}">

@stop

@section('content')
    <div class="main-body">
        <div class="page-wrapper">

            <!-- Page-header start -->
            <div class="page-header card">
                <div class="row align-items-end">
                    <div class="col-lg-8">
                        <div class="page-header-title">
                            <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                            <div class="d-inline">
                                <h4>Edit Retur Customer</h4>
                                <span>Edit Retur - {!! $retur->retur_code !!}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="page-header-breadcrumb">
                            <ul class="breadcrumb-title">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('home/main') }}">
                                        <i class="icofont icofont-home"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">Penjualan - Retur Customer</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Page-header end -->

            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">

                        <!-- Tooltip Validation card start -->
                        <div class="card">
                            <div class="card-header">
                                <h5>Edit Retur - {!! $retur->retur_code !!}</h5>
                                <div class="card-header-right">
                                    <i class="icofont icofont-spinner-alt-5"></i>
                                </div>
                            </div>
                            <div class="card-block">
                                {{-- {!! Form::open(['id' => 'second', 'method' => 'PATCH', 'novalidate' => 'novalidate', 'action' => ['ReturCustomerController@update', base64_encode($retur->retur_id)]]) !!} --}}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Kode Nota : </label>
                                    <div class="col-sm-10">
                                        {!! $retur->nota->nota_code !!}
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary m-b-0" onclick="window.location.href ="{{ url('home/penjualan/retur-customer') }}";">Submit</button>
                                    </div>
                                </div>
                                {{-- {!! Form::close() !!} --}}
                            </div>
                        </div>

                        <div id="view_detail">
                            <div class="card">
                                <div class="card-block">
                                    <div class="dt-responsive table-responsive">
                                        <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Barang</th>
                                                <th>Dikirim</th>
                                                <th>Retur</th>
                                                <th>Status</th>
                                                <th>Retur</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($detail as $data => $key)
                                            @php $retur_qty = Access::get_retur($key['retur_detail_data_id']); @endphp
                                                <tr>
                                                    <td width="1%" align="center">{!! ++$data !!}</td>
                                                    <td>{!! $key->item_name !!}</td>
                                                    <td>{!! $key->nota_fill_qty !!}</td>
                                                    <td>{!! $retur_qty->total !!}</td>
                                                    <td>
                                                        <select class="form-control" name="i_unit" onchange="update_status(this.value,{!! $key['retur_detail_id'] !!})">
                                                            <option value="0" {!! ($key['retur_detail_status'] == 0) ? 'selected' : '' !!} >Diterima</option>
                                                            <option value="1" {!! ($key['retur_detail_status'] == 1) ? 'selected' : '' !!}>Belum Diterima</option>
                                                        </select>
                                                    </td>
                                                    <td align="center">
                                                        <input type="number" class="form-control" onchange="update_qty(this.value,{!! $key->retur_detail_id !!},{!! $retur_qty->total !!},{!! $key->nota_fill_qty !!});" value="{!! $key['retur_detail_qty'] !!}">
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Barang</th>
                                                <th>Dikirim</th>
                                                <th>Retur</th>
                                                <th>Status</th>
                                                <th>Retur</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/jszip.min.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>

    <script src="{{ asset('assets/pages/data-table/extensions/buttons/js/extension-btns-custom.js') }}"></script>
    <!-- Select 2 js -->
    <script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
    <!-- Multiselect js -->
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function preview(id) {
            $("#view_detail").load('{{ url('home/ajax/preview-retur') }}/'+id);
        }

        function update_status(value,id) {
          $.ajax({
              type: 'POST',
              url: '{{ url('home/ajax/update-retur-status') }}',
              data: {id:id,value:value},
              dataType: 'JSON',
              success: function(data){

              } 
            });
        }

        function update_qty(value,id,qty_retur,fill) {
          if (qty_retur + value > fill) {
            swal("Oops!","Retur tidak boleh melebihi pengiriman","error");
          }else{
            $.ajax({
              type: 'POST',
              url: '{{ url('home/ajax/update-retur-qty') }}',
              data: {id:id,value:value},
              dataType: 'json',
              success: function(data){

              } 
            });
          }
        }
    </script>
@stop