<script type="text/javascript">
    function update_status(value,id) {
      $.ajax({
          type: 'POST',
          url: '{{ url('home/ajax/update-retur-status') }}',
          data: {id:id,value:value},
          dataType: 'JSON',
          success: function(data){

          } 
        });
    }

    function update_qty(value,id,qty_retur,fill) {
      if (qty_retur + value > fill) {
        swal("Oops!","Retur tidak boleh melebihi pengiriman","error");
      }else{
        $.ajax({
          type: 'POST',
          url: '{{ url('home/ajax/update-retur-qty') }}',
          data: {id:id,value:value},
          dataType: 'json',
          success: function(data){

          } 
        });
      }
    }
</script>

<div class="card">
    <div class="card-block">
        <div class="dt-responsive table-responsive">
            <table id="basic-btn" class="table table-striped table-bordered nowrap">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Dikirim</th>
                    <th>Retur</th>
                    <th>Status</th>
                    <th>Retur</th>
                </tr>
                </thead>
                <tbody>
                @foreach($list as $data => $key)
                @php $retur_qty = Access::get_retur($key['retur_detail_data_id']); @endphp
                    <tr>
                        <td width="1%" align="center">{!! ++$data !!}</td>
                        <td>{!! $key->item_name !!}</td>
                        <td>{!! $key->nota_fill_qty !!}</td>
                        <td>{!! $retur_qty->total !!}</td>
                        <td>
                            <select class="form-control" name="i_unit" onchange="update_status(this.value,{!! $key['retur_detail_id'] !!})">
                                <option value="0" {!! ($key['retur_detail_status'] == 0) ? 'selected' : '' !!} >Diterima</option>
                                <option value="1" {!! ($key['retur_detail_status'] == 1) ? 'selected' : '' !!}>Belum Diterima</option>
                            </select>
                        </td>
                        <td align="center">
                            <input type="number" class="form-control" onchange="update_qty(this.value,{!! $key->retur_detail_id !!},{!! $retur_qty->total !!},{!! $key->nota_fill_qty !!});" value="{!! $key['retur_detail_qty'] !!}">
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Dikirim</th>
                    <th>Retur</th>
                    <th>Status</th>
                    <th>Retur</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>

</div>