@extends('template.index')

@section('title')
    Dealer Information System - Buat Biaya Keliling
@stop

@section('style')

    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/advance-elements/css/bootstrap-datetimepicker.css') }}">
    <!-- Date-range picker css  -->
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/bootstrap-daterangepicker/css/daterangepicker.css') }}" />

@stop

@section('content')
    <div class="main-body">
        <div class="page-wrapper">

            <!-- Page-header start -->
            <div class="page-header card">
                <div class="row align-items-end">
                    <div class="col-lg-8">
                        <div class="page-header-title">
                            <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                            <div class="d-inline">
                                <h4>Buat Biaya Keliling</h4>
                                <span>Buat Biaya Keliling Baru</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="page-header-breadcrumb">
                            <ul class="breadcrumb-title">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('home/main') }}">
                                        <i class="icofont icofont-home"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">Pembelian - Biaya Keliling</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Page-header end -->

            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">

                        <!-- Tooltip Validation card start -->
                        <div class="card">
                            <div class="card-header">
                                <h5>Buat Biaya Keliling Baru</h5>
                                <div class="card-header-right">
                                    <i class="icofont icofont-spinner-alt-5"></i>
                                </div>
                            </div>
                            <div class="card-block">
                                {!! Form::open(['id' => 'second', 'method' => 'POST', 'novalidate' => 'novalidate', 'action' => 'SalesCostController@store', 'onkeypress' => 'return event.keyCode != 13;']) !!}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Sales</label>
                                    <div class="col-sm-4">
                                        <select class="form-control js-example-basic-single select2-hidden-accessible" name="sales" disabled>
                                            <option selected>- Pilih Sales -</option>
                                            @foreach($sales as $key)
                                                <option value="{!! $key['sales_id'] !!}" {!! ($result['sales_id'] == $key['sales_id']) ? 'selected' : '' !!}>{!! $key['sales_name'] !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                    <label class="col-sm-2 col-form-label">Keterangan</label>
                                    <div class="col-sm-4">
                                        <textarea name="keterangan" class="form-control" readonly>
                                            {!! $result['sales_cost_desc'] !!}
                                        </textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Periode</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="daterange" class="form-control" value="{!! $data['sales_cost_period'] !!}" id="date-time-picker" readonly />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Modal Awal</label>
                                    <div class="col-sm-4">
                                        <input type="number" class="form-control" name="modal_awal" value="{!! $result['sales_cost_modal'] !!}" readonly>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label class="pull-left"><b>Total Pengeluaran : <input type="text" name="i_total_view" readonly style="background: transparent;border: 0;text-align: right;font-weight: bold;font-size: 16px;" value="{!! number_format($result['sales_cost_total'],2) !!}"></b></label>
                                        <input type="hidden" name="i_total" value="{!! $result['sales_cost_total'] !!}" >
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group row" id="modal-tambahan" style="display: none;">
                                            <label class="col-form-label col-sm-6"><b>Modal Tambahan : </b></label>
                                            <div class="col-sm-4">
                                                <input type="number" class="form-control" name="i_modal_less" onchange="selisih();" value="0" style="width: 250px;" {!! ($result['sales_cost_lock'] == 1) ? 'readonly' : '' !!}>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        @php $selisih = $result['sales_cost_modal'] + $result['sales_cost_modal_less'] - $result['sales_cost_total']; @endphp
                                        <label class="pull-right"><b>Selisih : <input type="text" name="i_selisih_cur" readonly style="background: transparent;border: 0;text-align: right;font-weight: bold;font-size: 16px;" value="{!! number_format($selisih, 2) !!}"></b></label>
                                        <input type="hidden" name="i_selisih" value="{!! $selisih !!}" >
                                    </div>
                                </div>

                                <!-- HTML5 Export Buttons table start -->
                                <div class="card">
                                    <div class="card-header table-card-header">
                                        <h5>List Biaya Sales</h5>
                                    </div>
                                    <div class="card-block">
                                        <div class="dt-responsive table-responsive">
                                            <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Keperluan</th>
                                                        <th>Tanggal</th>
                                                        <th>Total Biaya</th>
                                                        <th>Keterangan</th>
                                                        <th>Check List</th>
                                                        <th>Lock</th>
                                                    </tr>
                                                    <tr>
                                                        <th></th>
                                                        <th>
                                                            <select class="form-control js-example-basic-single" name="i_cost_id">
                                                                <option value="0">-- Pilih Keperluan --</option>
                                                                @foreach ($cost as $row)
                                                                <option value="{!! $row['cost_id'] !!}">{!! $row['cost_name'] !!}</option>
                                                                @endforeach
                                                            </select>
                                                        </th>
                                                        <th>
                                                            <input type="date" class="form-control" name="i_detail_date" value="{!! date('Y-m-d') !!}">
                                                        </th>
                                                        <th>
                                                            <input type="number" name="i_detail_total" value="" class="form-control" placeholder="Masukkan Total Biaya" onkeypress="if (event.keyCode == 13) { create_detail(); }">
                                                        </th>
                                                        <th>
                                                            <input type="text" name="i_detail_desc" value="" class="form-control" placeholder="Masukkan Keterangan" onkeypress="if (event.keyCode == 13) { create_detail(); }">
                                                        </th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($list_detail as $data => $row)
                                                @if ($row['sales_cost_detail_status'] == 1)
                                                    @php $total = $row['sales_cost_detail_total']; @endphp
                                                @else
                                                    @php $total = 0; @endphp
                                                @endif

                                                @if ($row['sales_cost_id'] != 0)
                                                    @php $lock = "<i class='icofont icofont-ui-lock'></i>"; @endphp
                                                @else
                                                    @php $lock = "<i class='icofont icofont-ui-unlock'></i>" @endphp
                                                @endif
                                                    <tr>
                                                        <td width="1%" align="center">{!! ++$data !!}</td>
                                                        <td>{!! $row['cost_name'] !!}</td>
                                                        <td>{!! $row['sales_cost_detail_date'] !!}</td>
                                                        <td>
                                                            {!! number_format($row['sales_cost_detail_total'], 2) !!}
                                                            <input type="hidden" id="total_detail{!! $row['sales_cost_detail_id'] !!}" name="total_detail{!! $row['sales_cost_detail_id'] !!}" value="{!! $total !!}">
                                                        </td>
                                                        <td>{!! $row['sales_cost_detail_desc'] !!}</td>
                                                        <td>
                                                            <div class="col-sm-9 checkbox">
                                                                <label class="cb-checkbox cb-checkbox-dark-2" onclick="status_detail({!! $row['sales_cost_detail_id'] !!},{!! $row['sales_cost_detail_total'] !!})" >
                                                                    <input name="status_detail{!! $row['sales_cost_detail_id'] !!}" type="checkbox" {!! ($row['sales_cost_detail_status'] == 1) ? 'checked' : '' !!} value="{!! $row['sales_cost_detail_status'] !!}" onclick="status_detail({!! $row['sales_cost_detail_id'] !!},{!! $row['sales_cost_detail_total'] !!})">
                                                                    <input type="hidden" name="status_detail{!! $row['sales_cost_detail_id'] !!}" id="status_detail{!! $row['sales_cost_detail_id'] !!}" value="{!! $row['sales_cost_detail_status'] !!}"></input>
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td align="center">{!! $lock !!}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Keperluan</th>
                                                        <th>Tanggal</th>
                                                        <th>Total Biaya</th>
                                                        <th>Keterangan</th>
                                                        <th>Check List</th>
                                                        <th>Lock</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                    @if ($result['sales_cost_lock'] == 0)
                                        <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                        <button type="button" class="btn btn-info m-b-0" onclick="tambah_modal();">Tambah Modal</button>
                                        <button type="button" class="btn btn-warning m-b-0" onclick="lock();">Lock</button>
                                    @else
                                        <button type="button" class="btn btn-danger m-b-0" onclick="history.go(-1);">Kembali</button>
                                    @endif
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    
    <!-- Bootstrap date-time-picker js -->
    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/moment-with-locales.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/bootstrap-datetimepicker.min.js') }}"></script>
    <!-- Date-range picker js -->
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-daterangepicker/js/daterangepicker.js') }}"></script>

    <!-- Select 2 js -->
    <script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
    <!-- Multiselect js -->
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/custom-picker.js') }}"></script>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function tambah_modal() {
            var div = document.getElementById('modal-tambahan');
            if (div.style.display !== 'none') {
                div.style.display = 'none';
            }
            else {
                $("#modal-tambahan").show();
            }
        }

        function status_detail(id,value) {
          var lock = {!! $result['sales_cost_lock'] !!};
          var status = $('#status_detail'+id).val();

            if (lock == 0) {
                if (status == 0) {
                    $('#status_detail'+id).val(1);
                    $('#total_detail'+id).val(value);

                    $.ajax({
                        type: 'POST',
                        url: '{{ url('home/pembayaran/sales-cost/update-status-detail') }}',
                        data: { id:id, status:1 },
                        dataType: 'JSON',
                        success: function(data) {
                        }
                    });
            } else {
                $('#status_detail'+id).val(0);
                $('#total_detail'+id).val(0);

                $.ajax({
                    type: 'POST',
                    url: '{{ url('home/pembayaran/sales-cost/update-status-detail') }}',
                    data: { id:id, status:0 },
                    dataType: 'JSON',
                    success: function(data) {
                    } 
                  });
                }
                grand_total();
            }

        }

        function grand_total() {

          var grand_total = parseFloat(0) +
            @foreach($list_detail as $row2)
                parseFloat($('input[name="total_detail{!! $row2['sales_cost_detail_id'] !!}"]').val()) +
            @endforeach
            parseFloat(0);

            var grand_total_format = grand_total.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            $('input[name="i_total_view').val(grand_total_format);
            $('input[name="i_total').val(grand_total);  

            selisih();
        }

        function selisih() {
            var total =  parseFloat($('input[name="i_total"]').val());
            var less =  parseFloat($('input[name="i_modal_less"]').val());
            var modal =  parseFloat("{!! $result['sales_cost_modal'] !!}");
            var selisih = parseFloat(less + modal) -  parseFloat(total);

            var selisih_format = selisih.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
            $('input[name="i_selisih_cur').val(selisih_format);
            $('input[name="i_selisih').val(selisih);
        }

        function lock() {
            var id = {!! $data['row_id'] !!};
            var less =  parseFloat($('input[name="i_modal_less"]').val());
            var selisih =  parseFloat($('input[name="i_selisih"]').val());
            var total =  parseFloat($('input[name="i_total"]').val());

            if (selisih < 0) {
                swal('Oops!', 'Modal Kurang Tidak Dapat Lock', 'error');
            } else {
                $.ajax({
                    type: 'POST',
                    url: '{{ url('home/pembayaran/sales-cost/lock') }}',
                    data: {id:id,selisih:selisih,less:less,total:total},
                    dataType: 'JSON',
                    success: function(data) {
                        if (data.status == 'success') {
                            swal('Success', data.message, 'success');
                            window.location.reload();
                        } else {
                            swal('Oops!', data.message, 'error');
                        }
                    } 
                });
            }
        }

        function create_detail() {
            var id = {!! $result['sales_cost_id'] !!};
            var i_detail_sales  = {!! $result['sales_id'] !!};
            var i_cost_id       = $('select[name="i_cost_id"]').val();
            var i_detail_date   = $('input[name="i_detail_date"]').val();
            var i_detail_total  = $('input[name="i_detail_total"]').val();
            var i_detail_desc   = $('input[name="i_detail_desc"]').val();

            $.ajax({
                type: 'POST',
                url: '{{ url('home/pembayaran/sales-cost/create-detail') }}',
                data: {i_detail_sales:i_detail_sales,i_cost_id:i_cost_id,i_detail_date:i_detail_date,i_detail_total:i_detail_total,i_detail_desc:i_detail_desc,id:id},
                dataType: 'JSON',
                success: function(data) {
                    if (data.status == 'success') {
                        swal('Success', data.message, 'success');
                        window.location.reload();
                    } else {
                        swal('Oops!', data.message, 'error');
                    }
                } 
            });
        }
    </script>
@stop