@extends('template.index')

@section('title')
    Dealer Information System - Buat Biaya Keliling
@stop

@section('style')

    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/pages/advance-elements/css/bootstrap-datetimepicker.css') }}">
    <!-- Date-range picker css  -->
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/bootstrap-daterangepicker/css/daterangepicker.css') }}" />

@stop

@section('content')
    <div class="main-body">
        <div class="page-wrapper">

            <!-- Page-header start -->
            <div class="page-header card">
                <div class="row align-items-end">
                    <div class="col-lg-8">
                        <div class="page-header-title">
                            <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                            <div class="d-inline">
                                <h4>Buat Biaya Keliling</h4>
                                <span>Buat Biaya Keliling Baru</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="page-header-breadcrumb">
                            <ul class="breadcrumb-title">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('home/main') }}">
                                        <i class="icofont icofont-home"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">Pembelian - Biaya Keliling</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Page-header end -->

            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">

                        <!-- Tooltip Validation card start -->
                        <div class="card">
                            <div class="card-header">
                                <h5>Buat Biaya Keliling Baru</h5>
                                <div class="card-header-right">
                                    <i class="icofont icofont-spinner-alt-5"></i>
                                </div>
                            </div>
                            <div class="card-block">
                                {!! Form::open(['id' => 'second', 'method' => 'POST', 'novalidate' => 'novalidate', 'action' => 'SalesCostController@store', 'onkeypress' => 'return event.keyCode != 13;']) !!}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Sales</label>
                                    <div class="col-sm-4">
                                        <select class="form-control js-example-basic-single select2-hidden-accessible" name="sales">
                                            <option selected>- Pilih Sales -</option>
                                            @foreach($sales as $key)
                                                <option value="{!! $key['sales_id'] !!}">{!! $key['sales_name'] !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                    <label class="col-sm-2 col-form-label">Keterangan</label>
                                    <div class="col-sm-4">
                                        <textarea name="keterangan" class="form-control">
                                        </textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Periode</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="daterange" class="form-control" value="01/01/2015 - 01/31/2015" id="date-time-picker" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Modal Awal</label>
                                    <div class="col-sm-4">
                                        <input type="number" class="form-control" name="modal_awal" value="0">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <label class="pull-left"><b>Total Pengeluaran : {!! number_format($data['sales_cost_total'], 2) !!}</b></label>
                                        <input type="hidden" name="i_total" value="{!! $data['sales_cost_total'] !!}" >
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group row" id="modal-tambahan" style="display: none;">
                                            <label class="col-form-label col-sm-6"><b>Modal Tambahan : </b></label>
                                            <div class="col-sm-4">
                                                <input type="number" class="form-control" name="modal_tambahan" value="0" style="width: 250px;" {!! ($data['sales_cost_lock'] == 1) ? 'readonly' : '' !!}>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        @php $selisih = $data['sales_cost_modal'] + $data['sales_cost_modal_less'] - $data['sales_cost_total']; @endphp
                                        <label class="pull-right"><b>Selisih : {!! number_format($selisih, 2) !!}</b></label>
                                        <input type="hidden" name="i_selisih" value="{!! $selisih !!}" >
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                        <button type="button" class="btn btn-danger m-b-0" onclick="history.go(-1);">Kembali</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    
    <!-- Bootstrap date-time-picker js -->
    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/moment-with-locales.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/bootstrap-datetimepicker.min.js') }}"></script>
    <!-- Date-range picker js -->
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-daterangepicker/js/daterangepicker.js') }}"></script>

    <!-- Select 2 js -->
    <script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
    <!-- Multiselect js -->
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/custom-picker.js') }}"></script>

    <script type="text/javascript">
        function tambah_modal() {
            var div = document.getElementById('modal-tambahan');
            if (div.style.display !== 'none') {
                div.style.display = 'none';
            }
            else {
                $("#modal-tambahan").show();
            }
        }
    </script>
@stop