@extends('template.index')

@section('title')
    Dealer Information System - Edit User
@stop

@section('style')

    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/switchery/css/switchery.min.css') }}">

@stop

@section('content')
    <div class="main-body">
        <div class="page-wrapper">

            <!-- Page-header start -->
            <div class="page-header card">
                <div class="row align-items-end">
                    <div class="col-lg-8">
                        <div class="page-header-title">
                            <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                            <div class="d-inline">
                                <h4>Edit User</h4>
                                <span>Edit User Baru</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="page-header-breadcrumb">
                            <ul class="breadcrumb-title">
                                <li class="breadcrumb-item">
                                    <a href="{{ url('home/main') }}">
                                        <i class="icofont icofont-home"></i>
                                    </a>
                                </li>
                                <li class="breadcrumb-item"><a href="#!">Setting - User</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Page-header end -->

            <div class="page-body">
                <div class="row">
                    <div class="col-sm-12">

                        <!-- Tooltip Validation card start -->
                        <div class="card">
                            <div class="card-header">
                                <h5>Edit User - {!! $user->karyawan->employee_name !!}</h5>
                                <div class="card-header-right">
                                    <i class="icofont icofont-spinner-alt-5"></i>
                                </div>
                            </div>
                            <div class="card-block">
                                {!! Form::model($user, ['id' => 'second', 'method' => 'PATCH', 'novalidate' => 'novalidate', 'action' => ['UserController@update', base64_encode($user->user_id)]]) !!}
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Pilih Pegawai</label>
                                    <div class="col-sm-10">
                                        <select class="form-control js-example-basic-single select2-hidden-accessible" name="pegawai">
                                            <option selected>- Pilih Pegawai -</option>
                                            @foreach($pegawai as $key)
                                                <option value="{!! $key->employee_id !!}" {!! ($user->employee_id == $key->employee_id) ? 'selected' : '' !!}>{!! $key->employee_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Pilih Hak Akses</label>
                                    <div class="col-sm-10">
                                        <select class="form-control js-example-basic-single select2-hidden-accessible" name="roles">
                                            <option selected>- Pilih Hak Akses -</option>
                                            @foreach($roles as $key)
                                                <option value="{!! $key->user_type_id !!}" {!! ($user->user_type_id == $key->user_type_id) ? 'selected' : '' !!}>{!! $key->user_type_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Username</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="username" placeholder="Masukkan Username" value="{!! $user->user_username !!}" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Password</label>
                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" name="password" placeholder="Masukkan Password" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Password</label>
                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" name="cpassword" placeholder="Masukkan Konfirmasi Password" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Status</label>
                                    <div class="col-sm-10">
                                        <input type="checkbox" class="js-single" name="status" {!! ($user->user_active_status == 'true') ? 'checked' : '' !!} />
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('script')
    <!-- Select 2 js -->
    <script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
    <!-- Multiselect js -->
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

    <script type="text/javascript" src="{{ asset('bower_components/switchery/js/switchery.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/pages/advance-elements/swithces.js') }}"></script>
@stop