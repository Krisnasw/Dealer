@extends('template.index')

@section('title')
Dealer Information System - Edit Barang
@stop

@section('style')

    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{ asset('bower_components/select2/css/select2.min.css') }}"/>
    <!-- Multi Select css -->
    <link rel="stylesheet" type="text/css"
          href="{{ asset('bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/multiselect/css/multi-select.css') }}"/>

@stop

@section('subheader')
Edit Barang
@stop

@section('content')
<div class="main-body">
	<div class="page-wrapper">

        <!-- Page-header start -->
        <div class="page-header card">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="icofont icofont-file-spreadsheet bg-c-green"></i>
                        <div class="d-inline">
                            <h4>Edit Barang</h4>
                            <span>Edit Barang</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="page-header-breadcrumb">
                        <ul class="breadcrumb-title">
                            <li class="breadcrumb-item">
                                <a href="{{ url('home/main') }}">
                            <i class="icofont icofont-home"></i>
                        </a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Master Data - Barang</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-header end -->

		<div class="page-body">
			<div class="row">
				<div class="col-sm-12">

					<!-- Tooltip Validation card start -->
                    <div class="card">
                        <div class="card-header">
                            <h5>Edit Barang</h5>
                            <div class="card-header-right">
                            	<i class="icofont icofont-spinner-alt-5"></i>
                            </div>
                        </div>
                        <div class="card-block">
                        	{!! Form::model($item, ['id' => 'second', 'method' => 'PATCH', 'novalidate' => 'novalidate', 'route' => ['barang.update', base64_encode($item['item_id'])]]) !!}
                                <input type="hidden" name="item_id" id="item_id" value="{!! $item->item_id !!}">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Number Part</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="f_numberPart" placeholder="Masukkan Number Part" value="{!! $item->item_code !!}">
                                    </div>
                                    <label class="col-sm-2 col-form-label">H.E.T</label>
                                    <div class="col-sm-4">
                                    	<input type="text" class="form-control" name="f_het" placeholder="Masukkan H.E.T" value="{!! $item->item_het !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Nama Barang</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="f_namaBarang" placeholder="Masukkan Nama Barang" value="{!! $item->item_name !!}">
                                    </div>
                                    <label class="col-sm-2 col-form-label">Harga Pokok</label>
                                    <div class="col-sm-4">
                                    	<input type="number" class="form-control" name="f_hargaPokok" placeholder="Masukkan Harga Pokok" value="{!! $item->item_price_primary !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Merk</label>
                                    <div class="col-sm-4">
                                        <select class="col-sm-12 js-example-basic-single select2-hidden-accessible" name="f_merk">
                                        	@foreach($brand as $key)
                                            <option value="{!! $key->brand_id !!}" {!! ($key->brand_it == $item->brand_detail_id) ? 'selected' : '' !!}>{!! $key->brand_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <label class="col-sm-2 col-form-label">Harga Beli</label>
                                    <div class="col-sm-4">
                                    	<input type="number" class="form-control" name="f_hargaBeli" placeholder="Masukkan Harga Beli" value="{!! $item->item_price_buy !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Kode Part</label>
                                    <div class="col-sm-4">
                                        <select class="col-sm-12 js-example-basic-single select2-hidden-accessible" name="f_kodePart">
                                        	@foreach($brandDetail as $key)
                                            <option value="{!! $key->brand_detail_id !!}">{!! $key->brand_detail_name !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <label class="col-sm-2 col-form-label">Stok Barang</label>
                                    <div class="col-sm-4">
                                    	<input type="number" class="form-control" name="f_stokBarang" placeholder="Masukkan Stok Barang" value="{!! $item->item_stock !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Motor Fix</label>
                                    <div class="col-sm-3">
                                        <select class="col-sm-12 js-example-basic-single select2-hidden-accessible" id="motorFix" name="f_motorFix">
                                        	@foreach($character as $key)
                                            <option value="{!! $key->character_id !!}" {!! ($key->character_id == $item->character_fix) ? 'selected' : '' !!}>{!! $key->character_code !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <button type="button" class="btn btn-info m-b-0" data-toggle="modal" data-target="#add-motor">+ Motor</button>
                                    <label class="col-sm-2 col-form-label" style="margin-left: 1%;">Minimum Stok</label>
                                    <div class="col-sm-4">
                                    	<input type="number" class="form-control" name="f_minStok" placeholder="Masukkan Minimum Stok" value="{!! $item->item_stock_min !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Motor</label>
                                    <div class="col-sm-4">
                                    	<select class="js-example-basic-multiple col-sm-12" multiple="multiple" name="f_motor[]">
                                            @foreach($detailChar as $key)
                                            <option value="{!! $key->character_id !!}" {!! (in_array($key->character_id, array($item->character_id))) ? 'selected' : '' !!}>{!! $key->character_seri !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <label class="col-sm-2 col-form-label">Maximum Stok</label>
                                    <div class="col-sm-4">
                                    	<input type="number" class="form-control" name="f_maxStok" placeholder="Masukkan Maximum Stok" value="{!! $item->item_stock_max !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Satuan</label>
                                    <div class="col-sm-4">
                                    	<select class="form-control col-sm-12" name="f_satuan">
                                            <option value="3" {!! ($item->unit_id == 3) ? 'selected' : '' !!}>Set</option>
                                            <option value="4" {!! ($item->unit_id == 4) ? 'selected' : '' !!}>Pcs</option>
                                        </select>
                                    </div>
                                    <label class="col-sm-2 col-form-label">Barcode</label>
                                    <div class="col-sm-4">
                                    	<input type="number" class="form-control" name="f_barcode" placeholder="Masukkan Barcode" value="{!! $item->item_barcode !!}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label"></label>
                                    <div class="col-sm-4">
                                    </div>
                                    <label class="col-sm-2 col-form-label">Berat</label>
                                    <div class="col-sm-4">
                                    	<input type="number" class="form-control" name="f_berat" placeholder="Masukkan Berat" value="{!! $item->item_weight !!}">
                                    </div>
                                </div>

                                <div class="row">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-block">
                            <a href="javascript::void(0)" id="addRow" class="btn btn-primary m-b-20">+ Add Detail</a>

                            <div class="card" id="form-hidden">
                                <div class="card-block">
                                {!! Form::open(['method' => 'POST', 'action' => 'ApiController@addDetail', 'id' => 'form-detail']) !!}
                                    <h4 class="sub-title">Form Detail</h4>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Barang</label>
                                            <div class="col-sm-10">
                                                <select class="col-sm-12" id="select-barang" name="f_barang">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Status</label>
                                            <div class="col-sm-10">
                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="radio" name="status" value="0" id="status"> Aktif
                                                    </label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="radio" name="status" value="1" id="status"> Tidak Aktif
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <label class="col-sm-2"></label>
                                            <div class="col-sm-10">
                                                <button type="reset" class="btn btn-danger m-b-0">Reset</button>
                                                <button type="submit" class="btn btn-primary m-b-0">Submit</button>
                                            </div>
                                        </div>
                                {!! Form::close() !!}
                                </div>
                            </div>

                            <div class="dt-responsive table-responsive">
                                <table id="basic-btn" class="table table-striped table-bordered nowrap">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Number Part</th>
                                            <th>Nama Barang</th>
                                            <th>Tanggal Pembuatan</th>
                                            <th>Status</th>
                                            <th>Config</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($barang as $val => $keys)
                                        <tr>
                                            <td>{!! ++$val !!}</td>
                                            <td>{!! $keys->item_code !!}</td>
                                            <td>{!! $keys->item_name !!}</td>
                                            <td>{!! $keys->item_subtitu_date !!}</td>
                                            <td>{!! ($keys->item_subtitu_status == 1) ? 'Tidak Aktif' : 'Aktif' !!}</td>
                                            <td>
                                                <button type="button" class="btn btn-danger btn-outline-info" data-toggle="modal" data-target="#delete-target{{{ $keys->item_subtitu_id }}}"><i class="icofont icofont-delete-alt"></i></button>
                                            </td>
                                        </tr>

                                        <div class="modal fade" id="delete-target{{{ $keys->item_subtitu_id }}}" tabindex="-1" role="dialog">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header bg-danger">
                                                        <h4 class="modal-title">Hapus Data</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Apakah Anda Yakin Akan Menghapus <b>{!! $keys->item_name !!}</b> ?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        {!! Form::open(['method' => 'POST', 'action' => 'ApiController@deleteItemSubstitude']) !!}
                                                            <input type="hidden" name="id" value="{!! base64_encode($keys->item_subtitu_id) !!}">
                                                            {!! Form::submit("Ya", array('class' => 'btn btn-primary waves-effect waves-light')) !!}
                                                        {!! Form::close() !!}
                                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Number Part</th>
                                            <th>Nama Barang</th>
                                            <th>Tanggal Pembuatan</th>
                                            <th>Status</th>
                                            <th>Config</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>

                        <div class="modal fade md-effect-1" id="add-motor" tabindex="-1" role="dialog">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header bg-info">
                                        <h5 class="modal-title">Tambah Motor</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                {!! Form::open(['method' => 'POST', 'action' => 'ApiController@addMotor', 'id' => 'form-motor']) !!}
                                    <div class="modal-body">
                                        <!-- Basic Form Inputs card start -->
                                        <div class="card">
                                            <div class="card-block">
                                                <h4 class="sub-title">Form Tambah Motor Baru</h4>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">Kode Motor</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" name="kode_motor" placeholder="Masukkan Kode Motor">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">Merk</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" name="merk" placeholder="Masukkan Merk">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">Tahun</label>
                                                        <div class="col-sm-4">
                                                            <input type="number" min="0" class="form-control" name="tahun">
                                                        </div>
                                                        <label class="col-sm-2 col-form-label">Sampai</label>
                                                        <div class="col-sm-4">
                                                            <input type="number" min="0" class="form-control" name="sampaiTahun">
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                        <!-- Basic Form Inputs card end -->
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light" id="btnAddMotor">Submit</button>
                                    </div>
                                {!! Form::close() !!}
                                </div>
                            </div>
                        </div>

                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('script')
<!-- Select 2 js -->
<script type="text/javascript" src="{{ asset('bower_components/select2/js/select2.full.min.js') }}"></script>
<!-- Multiselect js -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/multiselect/js/jquery.multi-select.js') }}"></script>

<script type="text/javascript" src="{{ asset('assets/pages/advance-elements/select2-custom.js') }}"></script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function formatRepo(data) {
        var markup = "<option value='"+data.id+"'>"+data.text+"</option>";

        return markup;
    }

    function formatRepoSelection(data) {
        return data.text;
    }

    $(document).ready(function () {
        $("#form-hidden").hide();
        $("#addRow").on('click', function () {
            $("#form-hidden").show();
        });

        $("#select-barang").select2({
            ajax: {
                url: "{{ url('home/ajax/items') }}",
                dataType: 'JSON',
                delay: 100,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function(data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function(markup) {
                return markup;
            }, // let our custom formatter work
            placeholder: 'Pilih Barang',
            minimumInputLength: 1,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });

        $("#form-detail").submit(function (e) {
            e.preventDefault();
            var item_id = $("#item_id").val();
            var status = $('input[name="status"]:checked').val();
            var barang = document.getElementById("select-barang").value;

            $.post("{{ url('home/ajax/items/detail/add') }}", {
                item_id : item_id,
                status : status,
                f_barang : barang
            }, function (data) {
                swal({
                    title:  ((data.status === "error") ? "Opps!" : 'Success'),
                    text:   data.msg,
                    type:   ((data.status === "error") ? "error" : "success"),
                });
                window.location.reload();
            });
        });

        $("#form-motor").submit(function (e) {
            e.preventDefault();
            var formdata = new FormData($("#form-motor")[0]);

            $.ajax({
                url:        $("#form-motor").attr('action'),
                method:     "POST",
                data:       new FormData(this),
                processData: false,
                contentType: false
            })
            .done(function(response) {
                $("#add-motor").modal('toggle');
                swal({
                    title:  ((response.status === "error") ? "Opps!" : 'Motor Berhasil Ditambahkan'),
                    text:   response.msg,
                    type:   ((response.status === "error") ? "error" : "success"),
                });

                $.ajax({
                    url: "{{ url('home/ajax/motors') }}",
                    method: "GET",
                    contentType: false
                })
                .done(function(response) {
                    if (response.status == "error") {
                        swal({ title: "Opss!", text: response.msg, type: "error" });
                    } else {
                        var options = '';
                        for (var i = 0; i < response.data.length; i++) {
                            options += "<option value='"+response.data[i].character_id+"'>"+response.data[i].character_code+"</option>";
                        }
                        $("#motorFix").html(options);
                    }
                })
                .fail(function() {
                    swal({
                        title:  "Opss!",
                        text:   "Ada Yang Salah! , Silahkan Coba Lagi Nanti",
                        type:   "error",
                    });
                })
            })
            .fail(function() {
                $("#add-motor").modal('toggle');
                swal({
                    title:  "Opss!",
                    text:   "Ada Yang Salah! , Silahkan Coba Lagi Nanti",
                    type:   "error",
                });
            })
        });
    });
</script>
@stop