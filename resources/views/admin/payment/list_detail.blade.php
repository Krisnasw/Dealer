<div class="card">
    <div class="card-header table-card-header">
    </div>
    <div class="card-block">
        <div class="dt-responsive table-responsive">
            <table id="basic-btn" class="table table-striped table-bordered nowrap">
                <thead>
                <tr>
                    <th>Cek</th>
                    <th>Tanggal</th>
                    <th>Kode Detail</th>
                    <th>Tagihan</th>
                    <th>Potongan</th>
                    <th>Terbayar</th>
                </tr>
                </thead>
                <tbody>
                @php $grand_total_tagihan = 0; @endphp
                @php $grand_total_potongan = 0; @endphp
                {{-- @foreach($list as $data => $key)
                    <tr>
                        <td width="1%" align="center">{!! ++$data !!}</td>
                        <td>{!! $row['payment_code'] !!} {!! ($grand_total_sisa > 0) ? '<i class="icofont icofont-warning"></i>' : '' !!}</td>
                        <td>{!! $row['payment_date'] !!}</td>
                        <td>{!! number_format($grand_total_tagihan,2) !!}</td>
                        <td>{!! number_format($grand_total_potongan,2) !!}</td>
                        <td>{!! number_format($grand_total_terbayar,2) !!}</td>
                        <td>{!! number_format($grand_total_sisa,2) !!}</td>
                        <td align="center">
                            <a href="{{ url('home/pembayaran/payment') }}/{!! base64_encode($key->payment_id) !!}" class="btn btn-success btn-outline-success"><i class="icofont icofont-edit-alt"></i></a>
                            <button type="button" class="btn btn-danger btn-outline-info" data-toggle="modal" data-target="#delete-Modal{{{ $key->payment_id }}}"><i class="icofont icofont-delete-alt"></i></button>
                        </td>
                    </tr>

                    <div class="modal fade" id="delete-Modal{{{ $key->payment_id }}}" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header bg-danger">
                                    <h4 class="modal-title">Hapus Data</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p>Apakah Anda Yakin Akan Menghapus <b>{!! $key->menus->side_menu_name !!}</b> ?</p>
                                </div>
                                <div class="modal-footer">
                                    {!! Form::open(['method' => 'DELETE', 'route' => array('payment.destroy', base64_encode($key->payment_id))]) !!}
                                    {!! Form::submit("Ya", array('class' => 'btn btn-primary waves-effect waves-light')) !!}
                                    {!! Form::close() !!}
                                    <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach --}}
                </tbody>
            </table>
        </div>
    </div>
</div>