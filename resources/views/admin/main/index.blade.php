@extends('template.index')

@section('title')
Dashboard - Dealer
@stop

@section('style')

@stop

@section('content')
<div class="main-body">
    <div class="page-wrapper">
<div class="row">
    <!-- card1 start -->
    <div class="col-md-6 col-xl-3">
        <div class="card widget-card-1">
            <div class="card-block-small">
                <i class="icofont icofont-user bg-c-blue card1-icon"></i>
                <span class="text-c-blue f-w-600">Total User</span>
                <h4>49/50GB</h4>
            </div>
        </div>
    </div>
    <!-- card1 end -->
    <!-- card1 start -->
    <div class="col-md-6 col-xl-3">
        <div class="card widget-card-1">
            <div class="card-block-small">
                <i class="icofont icofont-people bg-c-pink card1-icon"></i>
                <span class="text-c-pink f-w-600">Total Client</span>
                <h4>$23,589</h4>
            </div>
        </div>
    </div>
    <!-- card1 end -->
    <!-- card1 start -->
    <div class="col-md-6 col-xl-3">
        <div class="card widget-card-1">
            <div class="card-block-small">
                <i class="icofont icofont-files bg-c-green card1-icon"></i>
                <span class="text-c-green f-w-600">Total Project</span>
                <h4>45</h4>
            </div>
        </div>
    </div>
    <!-- card1 end -->
    <!-- card1 start -->
    <div class="col-md-6 col-xl-3">
        <div class="card widget-card-1">
            <div class="card-block-small">
                <i class="icofont icofont-ui-rss bg-c-yellow card1-icon"></i>
                <span class="text-c-yellow f-w-600">Total Subscriber</span>
                <h4>+562</h4>
            </div>
        </div>
    </div>
    <!-- card1 end -->

    <!-- Project overview Start -->
    <div class="col-md-12 col-xl-12">
        <div class="card ">
            <div class="card-header ">
                <div class="card-header-left ">
                    <h5>Recent Comments</h5>
                </div>
                <div class="card-header-right">
                    <ul class="list-unstyled card-option">
                        <li><i class="icofont icofont-simple-left "></i></li>
                        <li><i class="icofont icofont-maximize full-card"></i></li>
                        <li><i class="icofont icofont-minus minimize-card"></i></li>
                        <li><i class="icofont icofont-refresh reload-card"></i></li>
                        <li><i class="icofont icofont-error close-card"></i></li>
                    </ul>
                </div>
            </div>
            <div class="card-block p-0">
                <div class="card-comment ">
                    <div class="card-block-small">
                        <img class="img-radius img-50" src="{{ asset('assets/images/avatar-4.jpg') }}" alt="user-1">
                        <div class="comment-desc">
                            <h6>Luciano Durk</h6>
                            <p class="text-muted ">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
                            <div class="comment-btn">
                                <button class="btn bg-c-blue btn-round btn-comment ">Action</button>
                            </div>
                            <div class="date">
                                <i>04 October 2015</i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-comment ">
                    <div class="card-block-small">
                        <img class="img-radius img-50" src="{{ asset('assets/images/avatar-4.jpg') }}" alt="user-1">
                        <div class="comment-desc">
                            <h6>John Doe</h6>
                            <p class="text-muted ">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
                            <div class="comment-btn">
                                <button class="btn bg-c-pink btn-round btn-comment ">Approved</button>
                            </div>
                            <div class="date">
                                <i>16 December 2015</i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-comment ">
                    <div class="card-block-small">
                        <img class="img-radius img-50" src="{{ asset('assets/images/avatar-3.jpg') }}" alt="user-1">
                        <div class="comment-desc">
                            <h6>Planner Board</h6>
                            <p class="text-muted ">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
                            <div class="comment-btn">
                                <button class="btn bg-c-green btn-round btn-comment ">Rejected</button>
                            </div>
                            <div class="date">
                                <i>12 Saptember 2015</i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-comment ">
                    <div class="card-block-small">
                        <img class="img-radius img-50" src="{{ asset('assets/images/avatar-2.jpg') }}" alt="user-1">
                        <div class="comment-desc">
                            <h6>Luciano Durk</h6>
                            <p class="text-muted ">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
                            <div class="comment-btn">
                                <button class="btn bg-c-pink btn-round btn-comment ">Approved</button>
                            </div>
                            <div class="date">
                                <i>20 October 2015</i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Project overview End -->
</div>
</div>
</div>
@stop

@section('script')

@stop