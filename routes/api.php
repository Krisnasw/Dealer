<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['api', 'CORS', 'XSS'], 'prefix' => 'v1'], function () {
	Route::post('auth/login', 'RestApiController@doLogin');

	Route::group(['middleware' => ['jwt.auth']], function () {

		// Get Data
		Route::post('auth/user', 'RestApiController@getUserData');
		Route::post('list/barang', 'RestApiController@getBunchOfItem');
		Route::post('list/barang/search', 'RestApiController@searchItem');
		Route::post('list/grade', 'RestApiController@getGrade');
		Route::post('list/city', 'RestApiController@getWilayah');
		Route::post('list/customer', 'RestApiController@getBunchOfCustomer');
		Route::post('list/sales', 'RestApiController@getBunchofSales');
		Route::post('list/merk', 'RestApiController@getBunchOfMerk');
		Route::post('list/memo-detail', 'RestApiController@getBunchOfMemoDetail');
		Route::post('cardboard/check', 'RestApiController@searchCardboard');
		Route::post('list/promo', 'RestApiController@getListPromo');

		// Post Data
		Route::post('customer/add', 'RestApiController@addCustomer');
		Route::post('memo-detail/add', 'RestApiController@addMemoDetail');
		Route::post('memo/add', 'RestApiController@addMemo');
		Route::post('cardboard/add', 'RestApiController@createCardboardStatus');

		// Update Data
		Route::post('memo-detail/update', 'RestApiController@updateMemoDetail');

		// Delete Data
		Route::post('memo-detail/delete', 'RestApiController@deleteMemoDetail');
	});
	
});