<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['XSS', 'CORS']], function () {
	Route::get('/', 'AuthController@index')->name('login');
	Route::post('/', 'AuthController@doLogin');

	Route::group(['middleware' => ['XSS', 'CORS', 'auth'], 'prefix' => 'home'], function () {
		Route::get('main', 'HomeController@index');
		Route::get('profile', 'ProfileController@index');
		Route::get('logout', 'HomeController@doLogout');
		Route::get('report', 'LaporanController@index');

		Route::group(['prefix' => 'master'], function () {
			Route::resource('sales', 'SalesController');
			Route::resource('barang', 'BarangController');
			Route::resource('supplier', 'SupplierController');
			Route::resource('promo', 'PromoController');
			Route::resource('supervisior', 'SPVController');
			Route::resource('rak-barang', 'RakController');
			Route::resource('customer-member', 'CustMemController');
			Route::resource('customer-pending', 'CustPdgController');
			Route::resource('paket-barang', 'PaketBarangController');
			Route::resource('pegawai', 'PegawaiController');
		});

		Route::group(['prefix' => 'setup'], function () {
			Route::resource('grade', 'GradeController');
			Route::resource('branch', 'BranchController');
			Route::resource('motor', 'MotorController');
			Route::resource('merk', 'BrandController');
			Route::resource('wilayah', 'WilayahController');
			Route::resource('truck', 'TruckController');
			Route::resource('biaya', 'BiayaController');
			Route::resource('unit', 'UnitController');
			Route::resource('position', 'PositionController');
		});

		Route::group(['prefix' => 'setting'], function () {
			Route::resource('user', 'UserController');
			Route::resource('roles', 'RolesController');
			Route::resource('password', 'PasswordController');
		});

		Route::group(['prefix' => 'penjualan'], function () {
			Route::resource('proses', 'ProsesController');
			Route::resource('faktur', 'FakturController');
			Route::get('faktur-edit/{nota_id}', 'ApiController@editFakturByPassword');
			Route::post('faktur-edit/update', 'ApiController@updateFakturData');
			Route::resource('retur-customer', 'ReturCustomerController');
		});

		Route::group(['prefix' => 'pembelian'], function () {
			Route::resource('purchase', 'PurchaseController');
			Route::post('purchase/get-brand-detail', 'PurchaseController@getBrandDetailById');
			Route::post('purchase/get-item-name', 'PurchaseController@readItemName');
			Route::post('purchase/get-item-code', 'PurchaseController@readItemCode');
			Route::post('purchase/save-tmp', 'PurchaseController@saveTmp');
			Route::post('purchase/delete-tmp', 'PurchaseController@deleteTmp');
			Route::post('purchase/delete-detail', 'PurchaseController@deleteDetail');
			Route::post('purchase/edit-purchase-detail-diskon', 'PurchaseController@updateTmpDiskon');
			Route::post('purchase/edit-purchase-detail-qty', 'PurchaseController@updateTmpPrice');
			Route::post('purchase/edit-qty', 'PurchaseController@editPurchaseDetailPrice');
			Route::post('purchase/edit-diskon', 'PurchaseController@editPurchaseDetailDiskon');
			Route::post('purchase/filter', 'PurchaseController@getBunchOfFilterable');
			Route::post('purchase/save-filter', 'PurchaseController@submitFilterable');

			// Agency
			Route::get('receipt', 'ReceiptController@index');
			Route::get('receipt/form-agency', 'ReceiptController@createAgency');
			Route::get('receipt/form-agency/{id}', 'ReceiptController@generateAgency');
			Route::get('receipt/agency/{id}/{cardboard_id}/entry', 'ReceiptController@entryCardboardAgency');
			Route::post('receipt/postAgency', 'ReceiptController@postAgency');
			Route::post('receipt/addAgencyCardboard', 'ReceiptController@addAgencyCardboard');
			Route::post('receipt/deleteCardboardAgency', 'ReceiptController@deleteCardboardAgency');
			Route::post('receipt/agency/purchase-detail', 'ReceiptController@getPurchaseDetail');
			Route::post('receipt/agency/delete', 'ReceiptController@deleteAgency');

			// Gudang
			Route::get('receipt/form-gudang', 'ReceiptController@createGudang');
			Route::get('receipt/form-gudang/{id}', 'ReceiptController@generateGudang');
			Route::post('receipt/postGudang', 'ReceiptController@postGudang');
			Route::post('receipt/addStorehouseCardboard', 'ReceiptController@addStorehouseCardboard');
			Route::get('receipt/gudang/{id}/{kardus_id}/entry', 'ReceiptController@entryCardboardGudang');
			Route::post('receipt/gudang/addDetailGudang', 'ReceiptController@addDetailGudang');
			Route::post('receipt/gudang/update-qty-detail', 'ReceiptController@updateDetailQtyGudang');
			Route::post('receipt/gudang/delete', 'ReceiptController@deleteGudang');

			// Sync
			Route::get('receipt/form-sync', 'ReceiptController@createSync');
			Route::get('receipt/form-sync/{id}', 'ReceiptController@generateSync');
			Route::post('receipt/postSync', 'ReceiptController@postSyncro');
			Route::post('receipt/sync/delete', 'ReceiptController@deleteSync');

			Route::resource('retur-supplier', 'ReturSupController');
			Route::post('retur-supplier/read-part', 'ReturSupController@readPart');
			Route::post('retur-supplier/read-qty', 'ReturSupController@readQty');
			Route::post('retur-supplier/save-detail', 'ReturSupController@saveDetail');
		});

		Route::group(['prefix' => 'gudang'], function () {
			Route::resource('packaging', 'PackagingController');

			Route::resource('delivery', 'DeliveryController');
			Route::post('delivery/add-detail', 'DeliveryController@saveKardus');
			Route::post('delivery/add-back', 'DeliveryController@addBack');
			Route::get('delivery/scan/{id}/{back_id}/{package}', 'DeliveryController@scanKardus');
			Route::post('delivery/check-kuota', 'DeliveryController@check_kuota');
			Route::post('delivery/add-detail-item', 'DeliveryController@submitScanKardus');
			Route::get('delivery-back/{id}', 'DeliveryController@getHoldedList');
			Route::get('print-barcode/{id}', 'PackagingController@printBarcode');
			Route::get('scan-item/{id}', 'PackagingController@listCardboardDetail');

			Route::resource('penerimaan', 'PenerimaanController');
		});

		Route::group(['prefix' => 'pembayaran'], function () {
			Route::resource('payment', 'PaymentController');
			Route::post('payment/add-detail-new', 'PaymentController@addDetailNew');
			Route::post('payment/bayar', 'PaymentController@doBayar');
			Route::get('payment/cust/{id}', 'PaymentController@preview');

			Route::resource('entrusted', 'EntrustedController');
			Route::resource('debit', 'DebitController');
			
			Route::resource('sales-cost', 'SalesCostController');
			Route::post('sales-cost/create-detail', 'SalesCostController@createDetail');
			Route::post('sales-cost/update-status-detail', 'SalesCostController@updateStatusDetail');
			Route::post('sales-cost/lock', 'SalesCostController@lockData');
			
			Route::resource('oprational', 'OprationalController');
			Route::resource('payment-sup', 'PaymentSupController');
			Route::post('payment-sup/read-po', 'PaymentSupController@read_po');
			Route::post('payment-sup/read-po-detail', 'PaymentSupController@read_po_detail');
			Route::post('payment-sup/save-detail', 'PaymentSupController@saveDetail');
		});

		Route::group(['prefix' => 'ajax'], function () {
			
			Route::post('target/add', 'ApiController@addTarget');
			Route::post('target/update', 'ApiController@updateTarget');
			Route::post('target/delete', 'ApiController@deleteTarget');
			
			Route::post('items', 'ApiController@getBunchOfItem');
			Route::post('items/delete', 'ApiController@deleteItem');
			
			Route::post('motor/add', 'ApiController@addMotor');
			Route::get('motors', 'ApiController@getCharacterFix');
			
			Route::get('items', 'ApiController@getItemsAjax');
			Route::post('items/detail/add', 'ApiController@addDetail');
			Route::post('items/detail/delete', 'ApiController@deleteItemSubstitude');

			Route::get('export', 'ApiController@exportItemToXLS');
			Route::post('import', 'ApiController@importExcelToDB');

			// Promo
			Route::post('promo/bonus', 'ApiController@addPromoBonus');
			Route::post('promo/delete', 'ApiController@deleteBonusDetail');

			// Customer
			Route::get('export-customer', 'ApiController@exportCustomerToXLS');
			Route::post('members', 'ApiController@getBunchOfMember');
			Route::post('cust-pending', 'ApiController@getBunchOfPendingMember');
			Route::post('members/delete', 'ApiController@deleteMember');

			// Wilayah
			Route::get('cities', 'ApiController@getBunchOfCity');
			Route::post('city/add', 'ApiController@addWilayah');

			// Item Package
			Route::post('item-packages', 'ApiController@getBunchOfItemPackages');
			Route::post('item-packages/delete', 'ApiController@deleteItemPackages');
			Route::post('formula/delete', 'ApiController@deleteFormulas');
			Route::post('formula/add', 'ApiController@addFormulas');
			Route::post('formula/proses', 'ApiController@prosesFormulas');

			// Pegawai
			Route::post('galeri/upload', 'ApiController@uploadGaleriPegawai');
			Route::post('galeri/delete', 'ApiController@deleteGallery');

			// Brand Detail
			Route::post('det-brand/add', 'ApiController@addBrandDetail');
			Route::post('det-brand/update', 'ApiController@updateBrandDetail');
			Route::post('det-brand/delete', 'ApiController@deleteBrandDetail');

			// City Detail
			Route::post('city-det/add', 'ApiController@addCityDetail');
			Route::post('city-det/delete', 'ApiController@deleteCityDetail');

			// Scan
			Route::post('truck-scan/upload', 'ApiController@uploadTruckScan');
			Route::post('truck-scan/delete', 'ApiController@deleteTruckScan');

			// Memo Detail
			Route::get('memo-detail/create', 'ApiController@getMemoDetailCreate');
			Route::get('memo-detail/update/{id}', 'ApiController@getMemoDetailUpdate');
            Route::post('memo-detail/add', 'ApiController@addMemoDetail');
            Route::post('memo-detail/delete', 'ApiController@deleteMemoDetail');

            // Custom
            Route::post('sales/get', 'ApiController@getFakturSalesByCustomer');
            Route::post('brand-dot', 'ApiController@getFakturBrand');
            Route::post('brand-limit', 'ApiController@getBrandLimit');
            Route::get('faktur-detail/{brand}/{customer}/{sales}/{type}/{tempo}/view', 'ApiController@getMemoDetailByCustomer');
            Route::get('faktur-detail/{brand}/{customer}/{sales}/{type}/{tempo}/view/item', 'ApiController@getMemoDetailByCustomerNew');
            Route::get('faktur-detail/{brand}/{customer}/{sales}/{type}/{tempo}/{form}', 'ApiController@load_style_select');
            Route::post('get-het', 'ApiController@getItemHet');
            Route::post('add-new-detail', 'ApiController@addDetailNew');
            Route::get('item-sub/{item_id}', 'ApiController@getItemSub');
            Route::post('updateMemoItem', 'ApiController@updateMemoItem');
            Route::post('deleteMemoItem', 'ApiController@deleteMemoItem');
            Route::get('nota-detail/{nota_id}', 'ApiController@getNotaDetail');
            Route::post('cek-password', 'ApiController@checkPasswordMenu');
            Route::post('update-nota-detail-item', 'ApiController@updateNotaDetailItem');
            Route::get('preview-retur/{id}', 'ApiController@getPreviewRetur');
            Route::post('update-retur-status', 'ApiController@updateReturStatus');
            Route::post('update-retur-qty', 'ApiController@updateReturQty');

            Route::get('cardboard/{id}', 'PackagingController@list_cardboard');
            Route::post('add-kardus', 'PackagingController@generateCardboard');
            Route::post('update-kardus-type', 'PackagingController@updateCardboardType');
            Route::post('add-detail-item', 'PackagingController@addDetailItem');
		});
	});
});