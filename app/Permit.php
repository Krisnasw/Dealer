<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permit extends Model
{
    //
    protected $table = 'permits';
    protected $primaryKey = 'permit_id';
    protected $fillable = ['user_type_id', 'side_menu_id', 'permit_acces'];

    public $timestamps = false;

    public function roles()
    {
        return $this->belongsTo('App\UserType', 'user_type_id', 'user_type_id');
    }

    public function menus()
    {
        return $this->belongsTo('App\Menu', 'side_menu_id', 'side_menu_id');
    }
}
