<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemPackageHistory extends Model
{
    //
    protected $table = 'item_package_histories';
    protected $primaryKey = 'item_package_history_id';
    protected $fillable = ['item_package_id', 'item_package_history_date', 'item_package_history_qty', 'user_id'];

    public $timestamps = false;

    public function packages()
    {
    	return $this->belongsTo('App\ItemPackage', 'item_package_id', 'item_package_id');
    }

    public function users()
    {
    	return $this->belongsTo('App\User', 'user_id', 'user_id');
    }
}
