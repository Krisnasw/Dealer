<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    //
    protected $table = 'side_menus';
    protected $primaryKey = 'side_menu_id';
    protected $fillable = ['side_menu_name', 'side_menu_url', 'side_menu_parent', 'side_menu_icon', 'side_menu_level', 'side_menu_type_parent'];

    public $timestamps = false;
}
