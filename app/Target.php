<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Target extends Model
{
    //
    protected $table = 'targets';
    protected $fillable = ['brand_id', 'sales_id', 'target_value'];
    public $timestamps = false;
}
