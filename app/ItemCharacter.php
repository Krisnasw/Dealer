<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemCharacter extends Model
{
    //
    protected $table = 'item_characters';
    protected $primaryKey = 'item_character_id';
    protected $fillable = ['item_id', 'character_id', 'item_package_id'];

    public $timestamps = false;
}
