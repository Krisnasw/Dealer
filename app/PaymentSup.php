<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentSup extends Model
{
    //
    protected $table = 'payment_sups';
    protected $primaryKey = 'payment_sup_id';
    protected $fillable = ['payment_sup_date', 'payment_sup_code', 'payment_sup_total', 'payment_sup_total_bayar', 'supplier_id', 'purchase_id', 'payment_sup_cash', 'payment_sup_rest', 'payment_sup_desc'];

    public $timestamps = false;
}
