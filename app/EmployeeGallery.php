<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeGallery extends Model
{
    //
    protected $table = 'employee_galeries';
    protected $primaryKey = 'employee_galery_id';
    protected $fillable = ['employee_id', 'employee_galery_file'];

    public $timestamps = false;
}
