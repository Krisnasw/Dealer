<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryPackaging extends Model
{
    //
    protected $table = 'delivery_packagings';
    protected $primaryKey = 'delivery_packaging_id';
    protected $fillable = ['delivery_id', 'packaging_id'];

    public $timestamps = false;
}
