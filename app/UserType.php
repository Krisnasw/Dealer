<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    //
    protected $table = 'user_types';
    protected $primaryKey = 'user_type_id';
    protected $fillable = ['user_type_name'];

    public $timestamps = false;
}
