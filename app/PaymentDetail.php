<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentDetail extends Model
{
    //
    protected $table = 'payment_details';
    protected $primaryKey = 'payment_detail_id';
    protected $fillable = ['payment_id', 'nota_id', 'payment_detail_bill', 'payment_detail_retur', 'payment_detail_entrusted', 'payment_detail_pay', 'payment_detail_payoff', 'payment_detail_discount', 'payment_detail_discount_total', 'payment_detail_type', 'payment_detail_nominal', 'payment_detail_rek', 'payment_detail_bank', 'payment_detail_date', 'payment_detail_tempo', 'user_id', 'payment_detail_status'];

    public $timestamps = false;
}
