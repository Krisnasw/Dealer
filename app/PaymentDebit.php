<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentDebit extends Model
{
    //
    protected $table = 'payment_debits';
    protected $primaryKey = 'payment_debit_id';
    protected $fillable = ['payment_id', 'debit_id'];

    public $timestamps = false;
}
