<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    //
    protected $table = 'employees';
    protected $primaryKey = 'employee_id';
    protected $fillable = ['employee_name', 'position_id', 'employee_gender', 'employee_birth_place', 'employee_birth_date', 'employee_ktp', 'employee_address_ktp', 'employee_no_telp', 'employee_email', 'employee_sim', 'employee_first_salary', 'employee_photo'];

    protected $attributes = [
    	'employee_nik' => 0,
    	'employee_npwp' => 0,
    	'employee_dept_id' => 0,
    	'employee_group_id' => 0,
    	'employee_status_id' => 0,
    	'employee_address_domisili' => 0,
    	'employee_no_phone' => 0,
    	'employee_bank_name' => 0,
    	'employee_bank_branch' => 0,
    	'employee_bank_rek' => 0,
    	'employee_bank_inisial' => 0,
    	'employee_emergency_name' => 0,
    	'employee_emergency_relation' => 0,
    	'employee_emergency_telp' => 0,
    	'employee_emergency_phone' => 0,
    	'employee_first_job_date' => '1994-05-01',
    	'employee_last_salary' => 0,
    	'employee_status_date' => '1994-05-01',
    	'employee_target' => 0,
    	'condition_id' => 0,
    	'religion_id' => 0,
    	'employee_province_ktp' => 0,
    	'employee_city_ktp' => 0,
    	'employee_postal_ktp' => 0,
    	'employee_province_domisili' => 0,
    	'employee_city_domisili' => 0,
    	'employee_postal_domisili' => 0,
    	'employee_email_company' => '0',
    	'employee_note' => '0'
    ];

    public $timestamps = false;
}
