<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesCostDetail extends Model
{
    //
    protected $table = 'sales_cost_details';
    protected $primaryKey = 'sales_cost_detail_id';
    protected $fillable = ['sales_cost_id', 'cost_id', 'sales_id', 'sales_cost_detail_date', 'sales_cost_detail_total', 'sales_cost_detail_desc', 'sales_cost_detail_status'];

    public $timestamps = false;

    public function cost() {
    	return $this->belongsTo('App\Cost', 'cost_id', 'cost_id');
    }

    public function sales() {
    	return $this->belongsTo('App\Sales', 'sales_id', 'sales_id');
    }
}