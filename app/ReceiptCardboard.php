<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReceiptCardboard extends Model
{
    //
    protected $table = 'receipt_cardboards';
    protected $primaryKey = 'receipt_cardboard_id';
    protected $fillable = ['receipt_cardboard_barcode', 'receipt_cardboard_code', 'receipt_id', 'cardboard_type_id'];

    public $timestamps = false;
}
