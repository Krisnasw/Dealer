<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nota extends Model
{
    //
    protected $table = 'notas';
    protected $primaryKey = 'nota_id';
    protected $fillable = ['nota_code', 'nota_date', 'sales_id', 'customer_id', 'brand_id', 'nota_status', 'nota_type_pay',
        'nota_tempo', 'nota_total', 'nota_accumulation', 'nota_commission', 'nota_discount', 'nota_discount2',
        'nota_netto'];

    public $timestamps = false;

    public function sales()
    {
        return $this->belongsTo('App\Sales', 'sales_id', 'sales_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer', 'customer_id', 'customer_id');
    }

    public function brand()
    {
        return $this->belongsTo('App\Brand', 'brand_id', 'brand_id');
    }
}
