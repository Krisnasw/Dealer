<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
    //
    protected $table = 'receipts';
    protected $primaryKey = 'receipt_id';
    protected $fillable = ['receipt_date', 'receipt_sj', 'receipt_code', 'purchase_id'];

    public $timestamps = false;
}
