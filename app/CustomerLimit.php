<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerLimit extends Model
{
    //
    protected $table = 'customer_limits';
    protected $primaryKey = 'customer_limit_id';
    protected $fillable = ['customer_id', 'brand_id', 'customer_limit_bill', 'customer_limit_due'];

    public $timestamps = false;
}
