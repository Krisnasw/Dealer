<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cardboard extends Model
{
    //
    protected $table = 'cardboards';
    protected $primaryKey = 'cardboard_id';
    protected $fillable = ['cardboard_barcode', 'packaging_id', 'cardboard_qty', 'cardboard_type_id', 'cardboard_status'];

    public $timestamps = false;
}
