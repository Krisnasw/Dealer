<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityDetail extends Model
{
    //
    protected $table = 'city_details';
    protected $primaryKey = 'city_detail_id';
    protected $fillable = ['city_id', 'customer_id'];

    public $timestamps = false;

    public function cities()
    {
    	return $this->belongsTo('App\City', 'city_id', 'city_id');
    }

    public function customers()
    {
    	return $this->belongsTo('App\Customer', 'customer_id', 'customer_id');
    }
}