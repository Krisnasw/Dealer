<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentSupDetail extends Model
{
    //
    protected $table = 'payment_sup_details';
    protected $primaryKey = 'payment_sup_detail_id';
    protected $fillable = ['payment_sup_id', 'payment_sup_detail_date', 'payment_sup_detail_desc', 'payment_sup_detail_pay'];

    public $timestamps = false;
}
