<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Access;
use Alert;
use Validator;
use App\Character;

class MotorController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,44);

            $this->permit = $akses->permit_acces;
                
            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            $motor = Character::orderBy('character_id', 'asc')->get();
            return view('admin.motor.index', compact('motor'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            # code...
            return view('admin.motor.create');
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'kode' => 'required',
            'nama' => 'required',
            'merk' => 'required',
            'tahun' => 'required',
            'tahunAkhir' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Character::create([
                'character_code' => $request->input('kode'),
                'character_seri' => $request->input('nama'),
                'character_merk' => $request->input('merk'),
                'character_date1' => $request->input('tahun'),
                'character_date2' => $request->input('tahunAkhir')
            ]);

            if ($create) {
                # code...
                Alert::success('Motor Berhasil Dibuat', 'Success');
                return redirect('home/setup/motor');
            } else {
                Alert::error('Gagal Membuat Motor', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strpos($this->permit, 'u') !== null) {
            # code...
            $motor = Character::where('character_id', base64_decode($id))->first();
            return view('admin.motor.edit', compact('motor'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'kode' => 'required',
            'nama' => 'required',
            'merk' => 'required',
            'tahun' => 'required',
            'tahunAkhir' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Character::where('character_id', base64_decode($id))->update([
                'character_code' => $request->input('kode'),
                'character_seri' => $request->input('nama'),
                'character_merk' => $request->input('merk'),
                'character_date1' => $request->input('tahun'),
                'character_date2' => $request->input('tahunAkhir')
            ]);

            if ($create) {
                # code...
                Alert::success('Motor Berhasil Diupdate', 'Success');
                return redirect('home/setup/motor');
            } else {
                Alert::error('Gagal Update Motor', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            # code...
            $del = Character::findOrFail(base64_decode($id));
            if ($del->delete()) {
                # code...
                Alert::success('Motor Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menghapus Motor', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403);
        }
    }
}
