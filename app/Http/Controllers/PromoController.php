<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Access;
use Alert;
use Validator;
use App\Promo;
use App\Item;
use App\PromoDetail;

class PromoController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,42);

            $this->permit = $akses->permit_acces;
                
            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            $promo = Promo::orderBy('bonus_id', 'ASC')->get();
            return view('admin.promo.index', compact('promo'));
        } else {
            abort(403, 'Unauthorized Access.');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            # code...
            return view('admin.promo.create');
        } else {
            abort(403, 'Unauthorized Access.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'status' => 'required',
            'f_kodePromo' => 'required',
            'f_nama' => 'required',
            'datefilter' => 'required'
        ]);

        $date = explode(' - ', $request->datefilter);
        $first_period = date('Y-m-d', strtotime($date[0]));
        $last_period = date('Y-m-d', strtotime($date[1]));

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Promo::create([
                'bonus_code' => $request->input('f_kodePromo'),
                'bonus_name' => $request->input('f_nama'),
                'bonus_period_first' => $first_period,
                'bonus_period_last' => $last_period,
                'bonus_status' => $request->input('status')
            ]);

            if ($create) {
                # code...
                Alert::success('Promo Berhasil Ditambahkan', 'Success');
                return redirect('home/master/promo');
            } else {
                Alert::error('Gagal Menambahkan Promo', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strpos($this->permit, 'u') !== null) {
            # code...
            $promo = Promo::where('bonus_id', base64_decode($id))->first();
            $barang = PromoDetail::where('bonus_id', base64_decode($id))->with('items')->with('barang')->where('bonus_detail_status', 0)->get();
            return view('admin.promo.edit', compact('promo', 'barang'));
        } else {
            abort(403, 'Unauthorized Access.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'status' => 'required',
            'f_kodePromo' => 'required',
            'f_nama' => 'required',
            'datefilter' => 'required'
        ]);

        $date = explode(' - ', $request->datefilter);
        $first_period = date('Y-m-d', strtotime($date[0]));
        $last_period = date('Y-m-d', strtotime($date[1]));

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Promo::where('bonus_id', base64_decode($id))->update([
                'bonus_code' => $request->input('f_kodePromo'),
                'bonus_name' => $request->input('f_nama'),
                'bonus_period_first' => $first_period,
                'bonus_period_last' => $last_period,
                'bonus_status' => $request->input('status')
            ]);

            if ($create) {
                # code...
                Alert::success('Promo Berhasil Diupdate', 'Success');
                return redirect('home/master/promo');
            } else {
                Alert::error('Gagal Update Promo', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            # code...
            $del = Promo::findOrFail(base64_decode($id));
            if ($del->delete()) {
                # code...
                Alert::success('Bonus Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menghapus Bonus', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403, 'Unauthorized Access.');
        }
    }
}
