<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use Alert;
use File;
use Validator;
use Hash;
use App\Sales;
use App\Target;
use App\Item;
use App\ItemCharacter;
use App\ItemPackageFormulas;
use App\ItemPackageHistory;
use App\Character;
use App\ItemSubstitude;
use App\ItemPackage;
use App\PromoDetail;
use App\Customer;
use App\City;
use App\EmployeeGallery;
use App\BrandDetail;
use App\CityDetail;
use App\TruckScan;
use App\Memo;
use App\MemoDetail;
use App\Brand;
use App\NotaDetailTmp;
use App\Password;
use App\Nota;
use App\NotaDetail;
use App\Retur;
use App\ReturDetail;

class ApiController extends Controller
{
    //
    public function addTarget(Request $request)
    {
    	$valid = Validator::make($request->all(), [
    		'sales_id' => 'required',
    		'merk' => 'required',
    		'target' => 'required'
    	]);

    	if ($valid->fails()) {
    		# code...
    		Alert::info('Form Tidak Lengkap', 'Info');
    		return redirect()->back()->withErrors($valid);
    	} else {
    		$target = Target::create([
    			'brand_id' => $request->input('merk'),
    			'sales_id' => $request->input('sales_id'),
    			'target_value' => $request->input('target')
    		]);

    		if ($target) {
    			# code...
    			Alert::success('Target Berhasil Dibuat', 'Success');
    			return redirect()->back();
    		} else {
    			Alert::error('Gagal Membuat Target', 'Error');
    			return redirect()->back();
    		}
    	}
    }

    public function updateTarget(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'target_id' => 'required',
            'merk' => 'required',
            'target' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back()->withErrors($valid);
        } else {
            $target = Target::where('target_id', $request->input('target_id'))->update([
                'brand_id' => $request->input('merk'),
                'target_value' => $request->input('target')
            ]);

            if ($target) {
                # code...
                Alert::success('Target Berhasil Diupdate', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Update Target', 'Error');
                return redirect()->back();
            }
        }
    }

    public function deleteTarget(Request $request)
    {
        $del = Target::where('target_id', base64_decode($request->input('target_id')))->delete();
        if ($del) {
            # code...
            Alert::success('Target Berhasil Dihapus', 'Success');
            return redirect()->back();
        } else {
            Alert::error('Gagal Menghapus Target', 'Error');
            return redirect()->back();
        }
    }

    public function getBunchOfItem(Request $request)
    {
        $columns = array( 
                            0 => 'item_id', 
                            1 => 'item_code',
                            2 => 'item_name',
                            3 => 'item_het',
                            4 => 'item_stock',
                            5 => 'item_stock_min',
                            6 => 'config'
                        );
  
        $totalData = Item::count();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
            $posts = Item::offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->get();
        }
        else {
            $search = $request->input('search.value'); 

            $posts =  Item::where('item_name','LIKE',"%{$search}%")
                            ->orWhere('item_code', 'LIKE',"%{$search}%")
                            ->where('item_status', 0)
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = Item::where('item_name','LIKE',"%{$search}%")
                             ->orWhere('item_code', 'LIKE',"%{$search}%")
                             ->where('item_status', 0)
                             ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $show =  route('barang.show',base64_encode($post->item_id));
                $edit =  route('barang.edit',base64_encode($post->item_id));

                $nestedData['item_id'] = $post->item_id;
                $nestedData['item_code'] = $post->item_code;
                $nestedData['item_name'] = $post->item_name;
                $nestedData['item_het'] = $post->item_het;
                $nestedData['item_stock'] = $post->item_stock;
                $nestedData['item_stock_min'] = $post->item_stock_min;
                $nestedData['config'] = "&emsp;<a href='{$edit}' class='btn btn-success btn-outline-success'><i class='icofont icofont-edit-alt'></i>EDIT</a>
                                          &emsp;<button type='button' class='btn btn-danger btn-outline-info' onclick='showDeleteModal($post->item_id);'><i class='icofont icofont-delete-alt'></i>DELETE</button>";
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data
                    );
            
        echo json_encode($json_data);
    }

    public function deleteItem(Request $request)
    {
        $item = Item::where('item_id', $request->input('item_id'))->update([
            'item_status' => 1
        ]);

        if ($item) {
            # code...
            return response(['status' => 'success', 'message' => 'Barang Berhasil Dihapus'], 200);
        } else {
            return response(['status' => 'error', 'message' => 'Gagal Menghapus Barang'], 500);
        }
    }

    public function addMotor(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'kode_motor' => 'required',
            'merk' => 'required',
            'tahun' => 'required',
            'sampaiTahun' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => 'error', 'msg' => 'Form Tidak Lengkap'], 200);
        } else {
            $motor = Character::create([
                'character_code' => $request->input('kode_motor'),
                'character_merk' => $request->input('merk'),
                'character_seri' => '',
                'character_date1' => $request->input('tahun'),
                'character_date2' => $request->input('sampaiTahun')
            ]);

            if ($motor) {
                # code...
                return response(['status' => 'success', 'msg' => 'Motor Berhasil Ditambahkan'], 200);
            } else {
                return response(['status' => 'error', 'msg' => 'Gagal Membuat Motor Baru'], 200);
            }
        }
    }

    public function getCharacterFix()
    {
        $character = Character::select('character_id', 'character_code')->get();

        if (count($character) <= 0) {
            # code...
            return response(['status' => 'error', 'message' => 'Motor Kosong', 'data' => []], 200);
        } else {
            return response(['status' => 'success', 'message' => 'Motor Ditemukan', 'data' => $character], 200);
        }
    }

    public function getItemsAjax()
    {
        $query = Item::where('item_name', 'LIKE', '%' . Input::get('q') . '%')->orWhere('item_code', 'LIKE', '%' . Input::get('q') . '%')->orderBy('item_name', 'ASC')->get();
        $response['items'] = array();
        if ($query<>false) {
            foreach ($query as $val) {
                $response['items'][] = array(
                    'id'    => $val->item_id,
                    'text'  => $val->item_name.' - '.$val->item_code
                );
            }
            $response['status'] = '200';
        }

        return response($response);
    }

    public function addDetail(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'item_id' => 'required',
            'status' => 'required',
            'f_barang' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => 'error', 'msg' => 'Form Tidak Lengkap'], 200);
        } else {
            $detail = ItemSubstitude::create([
                'item_id' => $request->input('item_id'),
                'item_subtitu_code' => '0',
                'item_subtitu_date' => date('Y-m-d'),
                'item_subtitu_status' => $request->input('status'),
                'item_detail_id' => $request->input('f_barang')
            ]);

            if ($detail) {
                # code...
                return response(['status' => 'success', 'msg' => 'Detail Berhasil Ditambahkan'], 200);
            } else {
                return response(['status' => 'error', 'msg' => 'Gagal Menambahkan Detail'], 200);
            }
        }
    }

    public function deleteItemSubstitude(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $item = ItemSubstitude::where('item_subtitu_id', base64_decode($request->input('id')))->update([
                'item_subtitu_status' => 1
            ]);

            if ($item) {
                # code...
                Alert::success('Item Detail Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menghapus Item Detail', 'Error');
                return redirect()->back();
            }
        }
    }

    public function exportItemToXLS()
    {
        // Create new empty excel document
        $namaFile = 'Export' . date('Y-m-d H:i:s');
        
        $export = \Excel::create($namaFile, function($excel) {
            $excel->sheet('Sheet1', function($sheet) {
                Item::chunk(500, function($modelInstance) use($sheet) {

                    $modelAsArray = $modelInstance->toArray();
                    $sheet->fromArray($modelAsArray);
                });
            });
        })->export('xls');

        if ($export) {
            # code...
            Alert::success('Export Berhasil', 'Success');
            return redirect()->back();
        } else {
            Alert::error('Gagal Melakukan Export Data', 'Error');
            return redirect()->back();
        }
    }

    public function exportCustomerToXLS()
    {
        // Create new empty excel document
        $namaFile = 'Export' . date('Y-m-d H:i:s');
        
        $export = \Excel::create($namaFile, function($excel) {
            $excel->sheet('Sheet1', function($sheet) {
                Customer::chunk(500, function($modelInstance) use($sheet) {

                    $modelAsArray = $modelInstance->toArray();
                    $sheet->fromArray($modelAsArray);
                });
            });
        })->export('xls');

        if ($export) {
            # code...
            Alert::success('Export Berhasil', 'Success');
            return redirect()->back();
        } else {
            Alert::error('Gagal Melakukan Export Data', 'Error');
            return redirect()->back();
        }
    }

    public function importExcelToDB(Request $request)
    {
        if($request->file('files'))
        {
            $path = $request->file('files')->getRealPath();
            $data = \Excel::load($path, function($reader) {})->get();
            // dd($data);
            if(!empty($data) && $data->count())
            {
                foreach ($data->toArray() as $row)
                {
                    if(!empty($row))
                    {
                        $dataArray[] =
                        [
                          'item_id' => $row['item_id'],
                          'item_code' => $row['item_code'],
                          'brand_detail_id' => $row['brand_detail_id'],
                          'item_name' => $row['item_name'],
                          'unit_id' => $row['unit_id'],
                          'character_id' => $row['character_id'],
                          'item_het' => $row['item_het'],
                          'item_price_primary' => $row['item_price_primary'],
                          'item_price_buy' => $row['item_price_buy'],
                          'item_stock' => $row['item_stock'],
                          'item_stock_min' => $row['item_stock_min'],
                          'item_stock_max' => $row['item_stock_max'],
                          'item_barcode' => $row['item_barcode'],
                          'item_weight' => $row['item_weight'],
                          'character_fix' => $row['character_fix'],
                          'item_status' => 0
                        ];
                    }
                }
                if(!empty($dataArray))
                {
                    Item::insert($dataArray);
                    return back();
                }
            }
        }
    }

    public function addPromoBonus(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'bonus_id' => 'required',
            'f_barang' => 'required',
            'min_buy' => 'required',
            'harga_khusus' => 'required',
            'bonus_barang' => 'required',
            'bonus_barang_qty' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = PromoDetail::create([
                'bonus_id' => $request->input('bonus_id'),
                'item_id' => $request->input('f_barang'),
                'bonus_detail_min' => $request->input('min_buy'),
                'bonus_detail_price' => $request->input('harga_khusus'),
                'bonus_detail_item_id' => $request->input('bonus_barang'),
                'bonus_detail_qty_item' => $request->input('bonus_barang_qty'),
                'bonus_detail_status' => 0
            ]);

            if ($create) {
                # code...
                Alert::success('Promo Detail Berhasil Ditambahkan', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Promo Detail Gagal Ditambahkan', 'Error');
                return redirect()->back();
            }
        }
    }

    public function deleteBonusDetail(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $item = PromoDetail::where('bonus_detail_status', base64_decode($request->input('id')))->update([
                'bonus_detail_status' => 1
            ]);

            if ($item) {
                # code...
                Alert::success('Promo Detail Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menghapus Promo Detail', 'Error');
                return redirect()->back();
            }
        }
    }

    public function getBunchOfMember(Request $request)
    {
         $columns = array( 
                            0 => 'customer_id', 
                            1 => 'customer_code',
                            2 => 'customer_name',
                            3 => 'customer_addres',
                            4 => 'customer_phone',
                            5 => 'grade_id',
                            6 => 'customer_rekening',
                            7 => 'customer_id_bank',
                            8 => 'customer_npwp',
                            9 => 'customer_nama_npwp',
                            10 => 'customer_expedisi',
                            11 => 'customer_hp',
                            12 => 'customer_fax',
                            13 => 'city_id',
                            14 => 'config'
                        );
  
        $totalData = Customer::count();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
            $posts = Customer::offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->where('customer_status', 0)
                         ->get();
        } else {
            $search = $request->input('search.value');

            $posts =  Customer::where('customer_status', 0)
                            ->where('customer_name','LIKE',"%{$search}%")
                            ->orWhere('customer_code', 'LIKE',"%{$search}%")
                            ->with('kota')
                            ->with('grades')
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = Customer::where('customer_name','LIKE',"%{$search}%")
                             ->orWhere('customer_code', 'LIKE',"%{$search}%")
                             ->where('customer_status', 0)
                             ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $show =  route('customer-member.show',base64_encode($post->customer_id));
                $edit =  route('customer-member.edit',base64_encode($post->customer_id));

                $nestedData['customer_id'] = $post->customer_id;
                $nestedData['customer_code'] = $post->customer_code;
                $nestedData['customer_name'] = $post->customer_name;
                $nestedData['customer_addres'] = $post->customer_addres;
                $nestedData['customer_phone'] = $post->customer_phone;
                $nestedData['grade_id'] = (!empty($post->grades->grade_nama) ? $post->grades->grade_nama : 'Grade Kosong');
                $nestedData['customer_rekening'] = $post->customer_rekening;
                $nestedData['customer_id_bank'] = $post->customer_id_bank;
                $nestedData['customer_npwp'] = $post->customer_npwp;
                $nestedData['customer_nama_npwp'] = $post->customer_nama_npwp;
                $nestedData['customer_expedisi'] = $post->customer_expedisi;
                $nestedData['customer_hp'] = $post->customer_hp;
                $nestedData['customer_fax'] = $post->customer_fax;
                $nestedData['city_id'] = (!empty($post->kota->city_name) ? $post->kota->city_name : 'Kota Tidak Ada');
                $nestedData['config'] = "&emsp;<a href='{$edit}' class='btn btn-success btn-outline-success'><i class='icofont icofont-edit-alt'></i>EDIT</a>
                                          &emsp;<button type='button' class='btn btn-danger btn-outline-info' onclick='showDeleteModal($post->customer_id);'><i class='icofont icofont-delete-alt'></i>DELETE</button>";
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data
                    );
            
        echo json_encode($json_data);
    }

    public function deleteMember(Request $request)
    {
        $item = Customer::where('customer_id', $request->input('customer_id'))->update([
            'customer_status' => 1
        ]);

        if ($item) {
            # code...
            return response(['status' => 'success', 'message' => 'Customer Berhasil Dihapus'], 200);
        } else {
            return response(['status' => 'error', 'message' => 'Gagal Menghapus Customer'], 500);
        }
    }

    public function addWilayah(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'namaWilayah' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => 'error', 'msg' => 'Form Tidak Lengkap'], 200);
        } else {
            $city = City::create([
                'province_id' => 0,
                'city_name' => $request->input('namaWilayah')
            ]);

            if ($city) {
                # code...
                return response(['status' => 'success', 'msg' => 'Wilayah Berhasil Ditambahkan'], 200);
            } else {
                return response(['status' => 'error', 'msg' => 'Gagal Membuat Wilayah Baru'], 200);
            }
        }
    }

    public function getBunchOfCity()
    {
        $city = City::select('city_id', 'city_name')->get();
        if (count($city) <= 0) {
            # code...
            return response(['status' => 'error', 'msg' => 'Wilayah Kosong', 'city' => []], 200);
        } else {
            return response(['status' => 'success', 'msg' => 'Wilayah Ditemukan', 'city' => $city], 200);
        }
    }

    public function getBunchOfPendingMember(Request $request)
    {
         $columns = array( 
                            0 => 'customer_id', 
                            1 => 'customer_code',
                            2 => 'customer_name',
                            3 => 'customer_addres',
                            4 => 'customer_phone',
                            5 => 'grade_id',
                            6 => 'customer_rekening',
                            7 => 'customer_id_bank',
                            8 => 'customer_npwp',
                            9 => 'customer_nama_npwp',
                            10 => 'customer_expedisi',
                            11 => 'customer_hp',
                            12 => 'customer_fax',
                            13 => 'city_id',
                            14 => 'config'
                        );
  
        $totalData = Customer::count();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
            $posts = Customer::offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->where('customer_status', 1)
                         ->get();
        } else {
            $search = $request->input('search.value');

            $posts =  Customer::where('customer_status', 1)
                            ->where('customer_name','LIKE',"%{$search}%")
                            ->orWhere('customer_code', 'LIKE',"%{$search}%")
                            ->with('kota')
                            ->with('grades')
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = Customer::where('customer_name','LIKE',"%{$search}%")
                             ->orWhere('customer_code', 'LIKE',"%{$search}%")
                             ->where('customer_status', 1)
                             ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $show =  route('customer-pending.show',base64_encode($post->customer_id));
                $edit =  route('customer-pending.edit',base64_encode($post->customer_id));

                $nestedData['customer_id'] = $post->customer_id;
                $nestedData['customer_code'] = $post->customer_code;
                $nestedData['customer_name'] = $post->customer_name;
                $nestedData['customer_addres'] = $post->customer_addres;
                $nestedData['customer_phone'] = $post->customer_phone;
                $nestedData['grade_id'] = (!empty($post->grades->grade_nama) ? $post->grades->grade_nama : 'Grade Kosong');
                $nestedData['customer_rekening'] = $post->customer_rekening;
                $nestedData['customer_id_bank'] = $post->customer_id_bank;
                $nestedData['customer_npwp'] = $post->customer_npwp;
                $nestedData['customer_nama_npwp'] = $post->customer_nama_npwp;
                $nestedData['customer_expedisi'] = $post->customer_expedisi;
                $nestedData['customer_hp'] = $post->customer_hp;
                $nestedData['customer_fax'] = $post->customer_fax;
                $nestedData['city_id'] = (!empty($post->kota->city_name) ? $post->kota->city_name : 'Kota Tidak Ada');
                $nestedData['config'] = "&emsp;<a href='{$edit}' class='btn btn-success btn-outline-success'><i class='icofont icofont-edit-alt'></i>EDIT</a>
                                          &emsp;<button type='button' class='btn btn-danger btn-outline-info' onclick='showDeleteModal($post->customer_id);'><i class='icofont icofont-delete-alt'></i>DELETE</button>";
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data
                    );
            
        echo json_encode($json_data);
    }

    public function getBunchOfItemPackages(Request $request)
    {
        $columns = array( 
                            0 => 'item_package_id', 
                            1 => 'item_package_code',
                            2 => 'brand_detail_id',
                            3 => 'item_package_name',
                            4 => 'unit_id',
                            5 => 'character_id',
                            6 => 'item_package_het',
                            7 => 'item_package_price_primary',
                            8 => 'item_package_price_buy',
                            9 => 'item_package_stock',
                            10 => 'item_package_stock_min',
                            11 => 'item_package_stock_max',
                            12 => 'item_package_barcode',
                            13 => 'item_package_weight',
                            14 => 'config'
                        );
  
        $totalData = ItemPackage::count();
            
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
            
        if(empty($request->input('search.value')))
        {            
            $posts = ItemPackage::offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->get();
        } else {
            $search = $request->input('search.value');

            $posts =  ItemPackage::where('item_package_name','LIKE',"%{$search}%")
                            ->orWhere('item_package_code', 'LIKE',"%{$search}%")
                            ->with('brandDetails')
                            ->with('units')
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = ItemPackage::where('item_package_name','LIKE',"%{$search}%")
                             ->orWhere('item_package_code', 'LIKE',"%{$search}%")
                             ->count();
        }

        $data = array();

        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $show =  route('paket-barang.show',base64_encode($post->item_package_id));
                $edit =  route('paket-barang.edit',base64_encode($post->item_package_id));

                $nestedData['item_package_id'] = $post->item_package_id;
                $nestedData['item_package_code'] = $post->item_package_code;
                $nestedData['brand_detail_id'] = (!empty($post->brandDetails->brand_detail_name) ? $post->brandDetails->brand_detail_name : 'Brand Detail Tidak Ditemukan');
                $nestedData['item_package_name'] = $post->item_package_name;
                $nestedData['unit_id'] = (!empty($post->units->unit_name) ? $post->units->unit_name : 'Unit Tidak Ditemukan');
                $nestedData['character_id'] = $post->character_id;
                $nestedData['item_package_het'] = $post->item_package_het;
                $nestedData['item_package_price_primary'] = $post->item_package_price_primary;
                $nestedData['item_package_price_buy'] = $post->item_package_price_buy;
                $nestedData['item_package_stock'] = $post->item_package_stock;
                $nestedData['item_package_stock_min'] = $post->item_package_stock_min;
                $nestedData['item_package_stock_max'] = $post->item_package_stock_max;
                $nestedData['item_package_barcode'] = $post->item_package_barcode;
                $nestedData['item_package_weight'] = $post->item_package_weight;
                $nestedData['config'] = "&emsp;<a href='{$edit}' class='btn btn-success btn-outline-success'><i class='icofont icofont-edit-alt'></i>EDIT</a>
                                          &emsp;<button type='button' class='btn btn-danger btn-outline-info' onclick='showDeleteModal($post->item_package_id);'><i class='icofont icofont-delete-alt'></i>DELETE</button>";
                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data
                    );
            
        echo json_encode($json_data);
    }

    public function deleteItemPackages(Request $request)
    {
        $del = ItemPackage::findOrFail($request->item_package_id)->delete();
        if ($del) {
            # code...
            return response(['status' => 'success', 'msg' => 'Paket Barang Berhasil Dihapus'], 200);
        } else {
            return response(['status' => 'error', 'msg' => 'Gagal Menghapus Paket Barang'], 200);
        }
    }

    public function addFormulas(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'f_barang' => 'required',
            'qty' => 'required',
            'id' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = ItemPackageFormulas::create([
                'item_package_id' => $request->input('id'),
                'item_id' => $request->input('f_barang'),
                'item_package_formula_qty' => $request->input('qty')
            ]);

            if ($request->input('qty') != 0) {
                # code...
                $qty = $request->input('qty');
                $items = ItemPackageFormulas::with('items')->where('item_package_formulas.item_package_id', $request->input('id'))->get();
                foreach($items as $key => $val) {
                    $qty2 = $qty * $val->item_package_formula_qty;

                    Item::where('item_id', $val->item_id)->decrement('item_stock', $qty2);
                }

                ItemPackage::where('item_package_id', $request->input('id'))->update([
                    'item_package_stock' => $qty
                ]);
            }

            $character_id = explode(',', $request->input('motor'));
            $arrLength = count($character_id);

            for ($i=0; $i < $arrLength; $i++) {
                # code...
                ItemCharacter::create([
                    'item_id' => 0,
                    'item_package_id' => $create->item_package_id,
                    'character_id' => $character_id[$i]
                ]);
            }

            if ($create) {
                # code...
                Alert::success('Formula Berhasil Ditambahkan', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menambahkan Formula', 'Error');
                return redirect()->back();
            }
        }
    }

    public function deleteFormulas(Request $request)
    {
        $del = ItemPackageFormulas::findOrFail(base64_decode($request->id))->delete();
        if ($del) {
            # code...
            Alert::success('Formula Berhasil Dihapus', 'Success');
            return redirect()->back();
        } else {
            Alert::error('Gagal Menghapus Formula', 'Error');
            return redirect()->back();
        }
    }

    public function prosesFormulas(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'jumlahPaket' => 'required',
            'id' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = ItemPackageHistory::create([
                'item_package_id' => $request->input('id'),
                'item_package_history_date' => date('Y-m-d'),
                'item_package_history_qty' => $request->input('jumlahPaket'),
                'user_id' => Auth::user()->user_id
            ]);

            if ($create) {
                # code...
                Alert::success('Formula Berhasil Diproses', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Memproses Formula', 'Error');
                return redirect()->back();
            }
        }
    }

    public function uploadGaleriPegawai(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'images' => 'required|image|mimes:jpg,jpeg,png',
            'id' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = EmployeeGallery::create([
                'employee_id' => $request->input('id'),
                'employee_galery_file' => $this->savePhoto($request->file('images'))
            ]);

            if ($create) {
                # code...
                Alert::success('Galeri Berhasil Dibuat', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Upload Galeri', 'Error');
                return redirect()->back();
            }
        }
    }

    public function deleteGallery(Request $request)
    {
        $exist = EmployeeGallery::where('employee_id', $request->input('id'))->first();
        $del = EmployeeGallery::where('employee_id', $request->input('id'))->delete();
        $this->deletePhoto($exist->employee_galery_file);
        if ($del) {
            # code...
            Alert::success('Galeri Berhasil Dihapus', 'Success');
            return redirect()->back();
        } else {
            Alert::error('Galeri Gagal Dihapus', 'Error');
            return redirect()->back();
        }
    }

    public function addBrandDetail(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'kode' => 'required',
            'target' => 'required',
            'id_brand' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = BrandDetail::create([
                'brand_detail_name' => $request->kode,
                'brand_detail_target' => $request->target,
                'brand_id' => $request->id_brand
            ]);

            if ($create) {
                Alert::success('Detail Merk Berhasil Dibuat', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Membuat Detail Merk', 'Error');
                return redirect()->back();
            }         
        }
    }

    public function updateBrandDetail(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'kode' => 'required',
            'target' => 'required',
            'id_brand' => 'required',
            'det_id' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = BrandDetail::where('brand_detail_id', $request->det_id)->update([
                'brand_detail_name' => $request->kode,
                'brand_detail_target' => $request->target,
                'brand_id' => $request->id_brand
            ]);

            if ($create) {
                Alert::success('Detail Merk Berhasil Diupdate', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Update Detail Merk', 'Error');
                return redirect()->back();
            }         
        }
    }

    public function deleteBrandDetail(Request $request)
    {
        $del = BrandDetail::where('brand_detail_id', base64_decode($request->input('id')))->delete();
        if ($del) {
            # code...
            Alert::success('Detail Merk Berhasil Dihapus', 'Success');
            return redirect()->back();
        } else {
            Alert::error('Detail Merk Gagal Dihapus', 'Error');
            return redirect()->back();
        }
    }

    public function addCityDetail(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'city' => 'required',
            'customer' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = CityDetail::create([
                'city_id' => $request->input('city'),
                'customer_id' => $request->input('customer')
            ]);

            if ($create) {
                # code...
                Alert::success('Detail Berhasil Ditambahkan', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menambahkan Detail', 'Error');
                return redirect()->back();
            }
        }
    }

    public function deleteCityDetail(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $del = CityDetail::where('city_detail_id', base64_decode($request->input('id')))->delete();
            if ($del) {
                # code...
                Alert::success('Detail Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menghapus Detail', 'Error');
                return redirect()->back();
            }
        }
    }

    public function uploadTruckScan(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'images' => 'required|image|mimes:jpg,jpeg,png',
            'id' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = TruckScan::create([
                'truck_scan_img' => ($request->hasFile('images') ? $this->savePhoto($request->file('images')) : 'images/placeholder/1.png'),
                'truck_id' => $request->input('id')
            ]);

            if ($create) {
                # code...
                Alert::success('Scan Berhasil Diupload', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Scan Gagal Diupload', 'Error');
                return redirect()->back();
            }
        }
    }

    public function deleteTruckScan(Request $request)
    {
        $exist = TruckScan::where('truck_scan_id', $request->input('id'))->first();
        // dd($exist);
        $del = TruckScan::where('truck_scan_id', $request->input('id'))->delete();
        $this->deletePhoto($exist->truck_scan_img);
        if ($del) {
            # code...
            Alert::success('Scan Berhasil Dihapus', 'Success');
            return redirect()->back();
        } else {
            Alert::error('Scan Gagal Dihapus', 'Error');
            return redirect()->back();
        }
    }

    public function getMemoDetailCreate()
    {
        $data = MemoDetail::join('items as b', 'b.item_id', 'memo_details.item_id')->where('memo_details.memo_id', 0)->where('memo_details.user_id', Auth::user()->user_id)->get();
        if (count($data) <= 0) {
            # code...
            return response(['data' => []], 200);
        } else {
            return response($data, 200);
        }
    }

    public function getMemoDetailUpdate($id)
    {
        $data = MemoDetail::join('items as b', 'b.item_id', 'memo_details.item_id')->where('memo_details.memo_id', $id)->where('memo_details.user_id', Auth::user()->user_id)->get();
        if (count($data) <= 0) {
            # code...
            return response(['data' => []], 200);
        } else {
            return response($data, 200);
        }
    }

    public function addMemoDetail(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'f_barang' => 'required',
            'qty' => 'required',
            'diskon' => 'required',
            'type' => 'required'
        ]);

        if ($valid->fails()) {
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            if ($request->type == 'create') {
                # code...
                $create = MemoDetail::create([
                    'memo_id' => 0,
                    'item_id' => $request->input('f_barang'),
                    'memo_detail_qty' => $request->input('qty'),
                    'memo_accumulation' => 0,
                    'memo_detail_discount' => $request->input('diskon'),
                    'memo_detail_cancel' => 0,
                    'user_id' => Auth::user()->user_id
                ]);

                if ($create) {
                    return response(['status' => 'success', 'message' => 'Detail Berhasil Ditambahkan'], 200);
                } else {
                    return response(['status' => 'error', 'message' => 'Gagal Menambahkan Detail'], 200);
                }
            } else {
                $create = MemoDetail::create([
                    'memo_id' => $request->input('ids'),
                    'item_id' => $request->input('f_barang'),
                    'memo_detail_qty' => $request->input('qty'),
                    'memo_accumulation' => 0,
                    'memo_detail_discount' => $request->input('diskon'),
                    'memo_detail_cancel' => 0,
                    'user_id' => Auth::user()->user_id
                ]);

                if ($create) {
                    return response(['status' => 'success', 'message' => 'Detail Berhasil Ditambahkan'], 200);
                } else {
                    return response(['status' => 'error', 'message' => 'Gagal Menambahkan Detail'], 200);
                }
            }
        }
    }

    public function deleteMemoDetail(Request $request)
    {
        $exist = MemoDetail::where('memo_detail_id', base64_decode($request->input('id')))->first();
        $del = MemoDetail::where('memo_detail_id', base64_decode($request->input('id')))->delete();
        if ($del) {
            # code...
            Alert::success('Detail Berhasil Dihapus', 'Success');
            return redirect()->back();
        } else {
            Alert::error('Detail Gagal Dihapus', 'Error');
            return redirect()->back();
        }
    }

    public function getFakturSalesByCustomer(Request $request)
    {
        $sales = Memo::join('sales as s', 's.sales_id', 'memos.sales_id')->where('memos.customer_id', $request->id)->groupBy('s.sales_id')->get();
        // dd($sales);
        $data = "<option value='0'>-- Pilih Sales --</option>";
        foreach ($sales as $row) {
            $data .= "<option value='".$row['sales_id']."'>".$row['sales_name']."</option>";
        }

        $response['type']       = 'spbe';
        $response['content']    = $data;
        $response['param']      = '';
        return response($response);
    }

    public function getFakturBrand(Request $request)
    {
        $brand = Brand::all();
        $data = "<option value='0'>-- Pilih Merk --</option>";
        foreach ($brand as $row) {

            $pending = $this->outstanding_brand($row['brand_id'], $request->id);
            if ($pending != 0) {
                # code...
                $dot = "&#9888;";
            } else {
                $dot = "";
            }

            $data .= "<option value='".$row['brand_id']."'>".$row['brand_name'].' '.$dot."</option>";

        }

        $response['type']       = 'spbe';
        $response['content']    = $data;
        $response['param']      = '';
        return response($response);
    }

    public function getBrandLimit(Request $request)
    {
        $brand = Brand::where('brand_id', $request->brand)->first();
        if (!empty($brand)) {
            # code...
            return response(['status' => 'error', 'cash' => 0, 'kredit' => 0], 200);
        } else {
            return response(['status' => 'success', 'cash' => $brand->brand_limit_cash, 'kredit' => $brand->brand_limit_kredit], 200);
        }
    }

    public function outstanding_brand($brand_id, $id)
    {
        $mem = MemoDetail::select(\DB::raw("(sum(memo_details.memo_detail_qty) - sum(memo_details.memo_accumulation) - sum(memo_details.memo_detail_cancel)) as pending"))
                            ->join('memos as b', 'b.memo_id', 'memo_details.memo_id')
                            ->where('b.brand_id', $brand_id)
                            ->where('b.customer_id', $id)
                            ->get();

        $result = null;
        foreach($mem as $row) $result = ($row);
        return $result['pending'];
    }

    public function getMemoDetailByCustomer($brand = 0,$customer = 0,$sales = 0,$type = 0,$tempo = 0,$password = 0)
    {
        NotaDetailTmp::where('user_id', Auth::user()->user_id)->delete();
        $list_detail = MemoDetail::select('memo_details.*', 'c.item_name', 'c.item_het', 'c.item_stock')
                            ->join('memos as b', 'b.memo_id', 'memo_details.memo_id')
                            ->join('items as c', 'c.item_id', 'memo_details.item_id')
                            ->where('b.customer_id', $customer)
                            ->where('b.sales_id', $sales)
                            ->where('b.brand_id', $brand)
                            ->groupBy('memo_details.memo_detail_id')
                            ->get();

        foreach ($list_detail as $row) :
            $qty_kirim  = $this->hitung_kirim($row['item_id'],$row['memo_detail_id']);
            $qty_perm   = $row['memo_detail_qty'] - $row['memo_accumulation'] - $row['memo_detail_cancel'];
            $harga      = $row['item_het'];

            if($qty_kirim > $qty_perm) {
                $fill = $qty_perm;
            } else {
                $fill = $qty_kirim;
            }

            $diskon     = $row['memo_detail_discount'] / 100 * $harga * $fill;
            $total      = $harga * $fill;

            if ($qty_perm != 0) {
                # code...
                $query = NotaDetailTmp::create([
                    'nota_id' => 0,
                    'memo_detail_id' => $row['memo_detail_id'],
                    'nota_fill_qty' => 0,
                    'nota_detail_total' => $total,
                    'nota_detail_discount' => 0,
                    'nota_detail_price' => $harga,
                    'user_id' => Auth::user()->user_id,
                    'nota_detail_est' => $fill,
                    'item_id' => $row['item_id']
                ]);

            }
            
        endforeach;

        $list = NotaDetailTmp::select('nota_detail_tmp.*', 'd.item_name', 'd.item_het', 'd.item_stock', 'c.memo_detail_qty', 'c.memo_detail_discount', 'c.memo_accumulation', 'c.memo_detail_cancel', 'e.memo_code', 'e.memo_date', 'c.memo_detail_id', 'c.item_id')
                                ->join('memo_details as c', 'c.memo_detail_id', 'nota_detail_tmp.memo_detail_id')
                                ->join('items as d', 'd.item_id', 'c.item_id')
                                ->join('memos as e', 'e.memo_id', 'c.memo_id')
                                ->where('nota_detail_tmp.user_id', Auth::user()->user_id)
                                ->get();

        $subs = ItemSubstitude::all();

        return \View::make('admin.faktur.list-detail', ['list' => $list, 'subs' => $subs]);
    }

    public function getMemoDetailByCustomerNew($brand = 0,$customer = 0,$sales = 0,$type = 0,$tempo = 0,$password = 0)
    {
        $list = NotaDetailTmp::select('nota_detail_tmp.*', 'd.item_name', 'd.item_het', 'd.item_stock', 'c.memo_detail_qty', 'c.memo_detail_discount', 'c.memo_accumulation', 'c.memo_detail_cancel', 'e.memo_code', 'e.memo_date', 'c.memo_detail_id', 'c.item_id')
                                ->join('memo_details as c', 'c.memo_detail_id', 'nota_detail_tmp.memo_detail_id')
                                ->join('items as d', 'd.item_id', 'c.item_id')
                                ->join('memos as e', 'e.memo_id', 'c.memo_id')
                                ->where('nota_detail_tmp.user_id', Auth::user()->user_id)
                                ->get();

        $subs = ItemSubstitude::all();

        return \View::make('admin.faktur.list-detail', ['list' => $list, 'subs' => $subs]);
    }

    public function getNotaDetail($nota_id)
    {
        $list = NotaDetail::select('nota_detail.*', 'd.item_name', 'd.item_het', 'd.item_stock', 'c.memo_detail_qty', 'c.memo_detail_discount', 'c.memo_accumulation', 'c.memo_detail_cancel', 'e.memo_code', 'e.memo_date', 'c.memo_detail_id', 'c.item_id')
                                ->join('memo_details as c', 'c.memo_detail_id', 'nota_detail_tmp.memo_detail_id')
                                ->join('items as d', 'd.item_id', 'c.item_id')
                                ->join('memos as e', 'e.memo_id', 'c.memo_id')
                                ->where('nota_details.nota_id', $nota_id)
                                ->get();

        $subs = ItemSubstitude::all();

        return \View::make('admin.faktur.list-detail', ['list' => $list, 'subs' => $subs]);
    }

    public function getItemHet(Request $request)
    {
        $item = Item::where('item_id', $request->id)->first();
        if (empty($item)) {
            # code...
            return response(['status' => 'error', 'content' => 0], 200);
        } else {
            return response(['status' => 'success', 'content' => $item->item_het], 200);
        }
    }

    public function addDetailNew(Request $request)
    {
        $diskon     = $request->diskon / 100 * $request->het * $request->fill;
        $total      = $request->het * $request->fill;

        $query = NotaDetailTmp::create([
                    'nota_id' => 0,
                    'memo_detail_id' => 0,
                    'nota_fill_qty' => $request->fill,
                    'nota_detail_total' => $total,
                    'nota_detail_discount' => $diskon,
                    'nota_detail_price' => $request->het,
                    'user_id' => Auth::user()->user_id,
                    'nota_detail_est' => 0,
                    'item_id' => $request->item
                ]);

        if ($query) {
            # code...
            return response(['status' => 'success', 'message' => 'Item Berhasil Ditambahkan'], 200);
        } else {
            return response(['status' => 'error', 'message' => 'Gagal Menambahkan Item'], 200);
        }
    }

    public function load_style_select($brand_id = 0, $customer_id = 0, $sales_id = 0, $type = 0, $tempo = 0, $password = 0,$id = 0)
    {
        $customer = Customer::all();
        $brand = Brand::all();
        $sales = Sales::all();

        $data = array();

        $data['row_id']                         = $id;
        $data['sales_id']                       = $sales_id;
        $data['customer_id']                    = $customer_id;
        $data['brand_id']                       = $brand_id ;
        $data['nota_type_pay']                  = $type;
        $data['nota_tempo']                     = $tempo;
        $data['nota_discount2']                 = 0;

        return \View::make('admin.faktur.style_select', ['data' => $data, 'cust' => $customer, 'brand' => $brand, 'sales' => $sales]);
    }

    public function getItemSub($item_id)
    {
        $brand = ItemSubstitude::where('item_id', $item_id)->get();
        $data = "<option value='0'>-- Pilih Barang --</option>";
        foreach ($brand as $row) {

            $data .= "<option value='".$row['item_subtitu_id']."'>".$row['item']['item_name']."</option>";

        }

        $response['type']       = 'spbe';
        $response['content']    = $data;
        $response['param']      = '';
        return response($response);
    }

    public function updateMemoItem(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'id' => 'required',
            'memo_id' => 'required',
            'itemChanged' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => 'error', 'message' => 'Form Tidak Lengkap'], 200);
        } else {
            $exist = MemoDetail::where('memo_detail_id', $request->memo_id)->where('item_id', $request->itemChanged)->where('user_id', Auth::user()->user_id)->get();
            if (count($exist) <= 0) {
                # code...
                return response(['status' => 'error', 'message' => 'Memo Tidak Ditemukan'], 200);
            } else {
                $up = MemoDetail::where('memo_detail_id', $request->memo_id)->where('item_id', $request->itemChanged)->where('user_id', Auth::user()->user_id)->update([
                    'item_id' => $request->id
                ]);

                if ($up) {
                    # code...
                    return response(['status' => 'success', 'message' => 'Barang Berhasil Ditukar'], 200);
                } else {
                    return response(['status' => 'error', 'message' => 'Gagal Menukar Barang'], 200);
                }
            }
        }
    }

    public function deleteMemoItem(Request $request)
    {
        $del = MemoDetail::where('memo_detail_id', $request->memo_id)->where('item_id', $request->id)->where('user_id', Auth::user()->user_id)->delete();
        if ($del) {
            # code...
            return response(['status' => 'success', 'message' => 'Barang Berhasil Dihapus'], 200);
        } else {
            return response(['status' => 'error', 'message' => 'Gagal Menghapus Barang'], 200);
        }
    }

    public function checkPasswordMenu(Request $request)
    {
        $pass = Password::where('menu_id', $request->menu)->first();
        if (!empty($pass)) {
            # code...
            $cek = Hash::check($request->password, $pass->password_value);
            if ($cek) {
                # code...
                return response(['status' => 'success', 'message' => 'Password Benar'], 200);
            } else {
                return response(['status' => 'error', 'message' => 'Password Salah'], 200);
            }
        } else {
            return response(['status' => 'error', 'message' => 'Password Menu Tidak Ditemukan'], 200);
        }
    }

    public function editFakturByPassword($nota_id)
    {
        $brand = Brand::select('brand_id', 'brand_name')->get();
        $sales = Sales::select('sales_id', 'sales_name')->get();
        $cust = Customer::select('customer_id', 'customer_name', 'customer_code')->get();
        $nota = Nota::where('nota_id', $nota_id)->first();
        $list = NotaDetail::select('nota_details.*', 'd.item_name', 'd.item_het', 'd.item_stock', 'c.memo_detail_qty', 'c.memo_detail_discount', 'c.memo_accumulation', 'c.memo_detail_cancel', 'c.memo_detail_id', 'c.item_id')
                            ->join('memo_details as c', 'c.memo_detail_id', 'nota_details.memo_detail_id')
                            ->join('items as d', 'd.item_id', 'c.item_id')
                            ->where('nota_details.nota_id', $nota_id)
                            ->get();

        return view('admin.faktur.edit-pass', compact('nota', 'brand', 'sales', 'cust', 'list'));
    }

    public function updateFakturData(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'brand' => 'required',
            'sales' => 'required',
            'customer' => 'required',
            'type' => 'required',
            'tempo' => 'required',
            'grandTotal' => 'required',
            'discount' => 'required',
            'netto' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => 'error', 'message' => 'Form Tidak Lengkap'], 200);
        } else {
            $cek_rows = NotaDetailTmp::where('user_id', Auth::user()->user_id)->where('nota_fill_qty', '<>', 0)->get();
            $create = Nota::where('nota_id', $request->nota_id)->update([
                'sales_id' => $request->sales,
                'customer_id' => $request->customer,
                'brand_id' => $request->brand,
                'nota_type_pay' => $request->type,
                'nota_status' => 0,
                'nota_tempo' => $request->tempo,
                'nota_total' => $request->grandTotal,
                'nota_accumulation' => $request->grandTotal,
                'nota_commission' => 0,
                'nota_discount' => 0,
                'nota_discount2' => $request->discount,
                'nota_netto' => $request->netto
            ]);

            $lastInsertedId = Nota::select('notas.*', 'b.customer_name', 'b.customer_expedisi', 'b.customer_store', 'b.customer_addres', 'b.customer_code', 'c.sales_name', 'c.sales_code',
                                            \DB::raw('COUNT(d.nota_detail_id) as jml_detail'), 'e.city_name')
                                    ->join('customers as b', 'b.customer_id', 'notas.customer_id')
                                    ->join('sales as c', 'c.sales_id', 'notas.sales_id')
                                    ->join('cities as e', 'e.city_id', 'b.city_id')
                                    ->join('nota_details as d', 'd.nota_id', 'notas.nota_id')
                                    ->where('notas.nota_id', $request->nota_id)
                                    ->get();

            $list = NotaDetailTmp::select('nota_detail_tmp.*', 'd.item_name', 'd.item_het', 'd.item_stock', 'c.memo_detail_qty', 'c.memo_detail_discount', 'c.memo_accumulation', 'c.memo_detail_cancel', 'e.memo_code', 'e.memo_date', 'c.memo_detail_id', 'c.item_id')
                                ->join('memo_details as c', 'c.memo_detail_id', 'nota_detail_tmp.memo_detail_id')
                                ->join('items as d', 'd.item_id', 'c.item_id')
                                ->join('memos as e', 'e.memo_id', 'c.memo_id')
                                ->where('nota_detail_tmp.user_id', Auth::user()->user_id)
                                ->get();

            foreach ($list as $row):

                $data2['nota_id']                   = $request->nota_id;
                
                $data2['nota_fill_qty']             = $row['nota_fill_qty'];
                $data2['nota_detail_total']         = $row['nota_detail_total'];
                $data2['nota_detail_discount']      = $row['nota_detail_discount'];
                $data2['nota_detail_price']         = $row['nota_detail_price'];

                $stock_akhir        = $row['item_stock'] - $data2['nota_fill_qty'];
                $akumulasi_akhir    = $row['memo_accumulation'] + $data2['nota_fill_qty'];

                Item::where('item_id', $row['item_id'])->update([
                    'item_stock' => $stock_akhir
                ]);

                if ($row['memo_detail_id']) {
                    # code...
                    MemoDetail::where('memo_detail_id', $row['memo_detail_id'])->update([
                        'memo_accumulation' => $akumulasi_akhir
                    ]);

                    $data2['memo_detail_id']            = $row['memo_detail_id'];
                    $data2['item_id']                   = $row['item_id'];
                } else {
                    $data2['item_id']                   = $row['item_id'];
                }

                // if ($data2['nota_fill_qty'] != 0) {
                    NotaDetail::where('nota_id', $request->nota_id)->update([
                        'memo_detail_id' => $data2['memo_detail_id'],
                        'nota_detail_price' => $data2['nota_detail_price'],
                        'nota_fill_qty' => $data2['nota_fill_qty'],
                        'nota_detail_total' => $data2['nota_detail_total'],
                        'nota_detail_discount' => $data2['nota_detail_discount'],
                        'item_id' => $data2['item_id']
                    ]);
                // }

            endforeach;

            NotaDetailTmp::where('user_id', Auth::user()->user_id)->delete();

            if ($create) {
                # code...
                return response(['status' => 'success', 'message' => 'Nota Berhasil Diupdate'], 200);
            } else {
                return response(['status' => 'error', 'message' => 'Gagal Update Nota'], 200);
            }
        }
    }

    public function updateNotaDetailItem(Request $request)
    {
        if ($request->dikirim > $request->permint) {
            # code...
            Alert::info('Barang Kiriman Tidak Boleh Melebihi Permintaan', 'Info');
        } else {
            $detail = NotaDetail::where('nota_detail_id', $request->detail_id)->where('item_id', $request->item_id)->update([
                'nota_fill_qty' => $request->dikirim
            ]);

            if ($detail) {
                # code...
                Alert::success('Barang Kiriman Berhasil Diupdate', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Update Barang Kiriman', 'Error');
                return redirect()->back();
            }
        }
    }

    public function getPreviewRetur($id)
    {
        ReturDetail::where('user_id', Auth::user()->user_id)->delete();
        $list_detail = NotaDetail::where('nota_id', $id)->get();
        foreach($list_detail as $row):
            $data2['retur_id']                  = 0;
            $data2['retur_detail_data_id']      = $row['nota_detail_id'];
            $data2['retur_detail_qty']          = 0;
            $data2['retur_detail_status']       = 0;
            $data2['user_id']                   = Auth::user()->user_id;
            $data2['retur_detail_type']         = 1;

            ReturDetail::create($data2);
        endforeach;

        $list = ReturDetail::select('retur_details.*', 'e.unit_name', 'd.item_code', 'd.item_name', 'd.item_het', 'd.item_stock', 'c.memo_detail_qty', 'c.memo_detail_discount',
            'b.nota_fill_qty', 'b.nota_detail_price', 'b.nota_detail_total', 'b.nota_detail_discount')
                            ->join('nota_details as b', 'b.nota_detail_id', 'retur_details.retur_detail_data_id')
                            ->join('memo_details as c', 'c.memo_detail_id', 'b.memo_detail_id')
                            ->join('items as d', 'd.item_id', 'c.item_id')
                            ->join('units as e', 'e.unit_id', 'd.unit_id')
                            ->where('retur_details.retur_id', 0)
                            ->where('retur_details.user_id', Auth::user()->user_id)->get();

        return \View::make('admin.retur-customer.view_detail', ['list' => $list]);
    }

    public function updateReturStatus(Request $request)
    {
        $update = ReturDetail::where('retur_detail_id', $request->id)->update([
            'retur_detail_status' => $request->value
        ]);

        if ($update) {
            # code...
            return response(['status' => 'success', 'message' => 'Status Berhasil Diupdate'], 200);
        } else {
            return response(['status' => 'error', 'message' => 'Gagal Update Status'], 200);
        }
    }

    public function updateReturQty(Request $request)
    {
        $update = ReturDetail::where('retur_detail_id', $request->id)->update([
            'retur_detail_qty' => $request->value
        ]);

        if ($update) {
            # code...
            return response(['status' => 'success', 'message' => 'Status Berhasil Diupdate'], 200);
        } else {
            return response(['status' => 'error', 'message' => 'Gagal Update Status'], 200);
        }
    }

    protected function hitung_kirim($item_id, $detail_id)
    {
        $stock      = $this->getItemStockById($item_id);
        $pending    = $this->hitung_pending($item_id);
        $permintaan = $this->hitung_permintaan($detail_id);
        if ($stock) {   $stock = $stock; } else{ $stock = 0; }
        if ($pending) { $pending = $pending;    }else{ $pending = 0; }
        if ($permintaan) {  $permintaan = $permintaan;  }else{ $permintaan = 0; }

        $bagi = $pending * $permintaan;
        
        if ($bagi) {
            $qty = $stock / $pending * $permintaan;
        }else{
            $qty = 0;
        }
        
        return $qty;
    }

    protected function getItemStockById($id)
    {
        $query = Item::select('item_stock')->where('item_id', $id)->get();
        $result = null;
        foreach($query as $row) $result = ($row);
        return $result['item_stock'];
    }

    protected function hitung_pending($id)
    {
        $query = MemoDetail::select(\DB::raw('(sum(memo_details.memo_detail_qty) - sum(memo_details.memo_accumulation) - sum(memo_details.memo_detail_cancel)) as pending'))
                                ->where('memo_details.item_id', $id)
                                ->get();
        $result = null;
        foreach($query as $row) $result = ($row);
        return $result['pending'];
    }

    protected function hitung_permintaan($id)
    {
        $query = MemoDetail::select(\DB::raw('(sum(memo_details.memo_detail_qty) - sum(memo_details.memo_accumulation) - sum(memo_details.memo_detail_cancel)) as pending'))
                                ->where('memo_details.memo_detail_id', $id)
                                ->get();
        $result = null;
        foreach($query as $row) $result = ($row);
        return $result['pending'];
    }

    protected function savePhoto($photo)
    {
        $destinationPath = 'images';
        $subdestinationPath = 'gallery';
        $extension = $photo->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        $photo->move($destinationPath. '/' . $subdestinationPath , $fileName);
        $create['employee_galery_file'] = $destinationPath. '/' . $subdestinationPath . '/' . $fileName;

        return $create['employee_galery_file'];
    }

    protected function deletePhoto($photo)
    {
        File::delete($photo);
        return $photo;
    }
}