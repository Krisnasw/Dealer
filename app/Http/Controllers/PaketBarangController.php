<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Access;
use Validator;
use Alert;
use App\Item;
use App\ItemPackageFormulas;
use App\ItemPackageHistory;
use App\Brand;
use App\BrandDetail;
use App\Character;
use App\Unit;
use App\ItemPackage;

class PaketBarangController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,55);

            $this->permit = $akses->permit_acces;
                
            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            return view('admin.paket.index');
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            # code...
            $brand = Brand::select('brand_id', 'brand_name')->get();
            $BrandDetail = BrandDetail::select('brand_detail_id', 'brand_detail_name')->get();
            $character = Character::select('character_id', 'character_seri')->get();
            $unit = Unit::all();
            return view('admin.paket.create', compact('brand', 'character', 'unit', 'BrandDetail'));
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'number_part' => 'required',
            'f_nama' => 'required',
            'merk' => 'required',
            'kodePart' => 'required',
            'motor' => 'required',
            'unit' => 'required',
            'het' => 'required',
            'harga_pokok' => 'required',
            'harga_beli' => 'required',
            'stok' => 'required',
            'min_stok' => 'required',
            'max_stok' => 'required',
            'barcode' => 'required',
            'berat' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = ItemPackage::create([
                'item_package_code' => $request->input('number_part'),
                'brand_detail_id' => $request->input('kodePart'),
                'item_package_name' => $request->input('f_nama'),
                'unit_id' => $request->input('unit'),
                'character_id' => implode(',', $request->input('motor')),
                'item_package_het' => $request->input('het'),
                'item_package_price_primary' => $request->input('harga_pokok'),
                'item_package_price_buy' => $request->input('harga_beli'),
                'item_package_stock' => $request->input('stok'),
                'item_package_stock_min' => $request->input('min_stok'),
                'item_package_stock_max' => $request->input('max_stok'),
                'item_package_barcode' => $request->input('barcode'),
                'item_package_weight' => $request->input('berat')
            ]);

            if ($create) {
                # code...
                Alert::success('Paket Barang Berhasil Ditambahkan', 'Success');
                return redirect('home/master/paket-barang');
            } else {
                Alert::error('Gagal Membuat Paket Barang', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strpos($this->permit, 'u') !== null) {
            # code...
            $item = ItemPackage::where('item_package_id', base64_decode($id))->first();
            $brand = Brand::select('brand_id', 'brand_name')->get();
            $BrandDetail = BrandDetail::select('brand_detail_id', 'brand_detail_name')->get();
            $character = Character::select('character_id', 'character_seri')->get();
            $unit = Unit::all();
            $formulas = ItemPackageFormulas::where('item_package_id', base64_decode($id))->with('items')->with('packages')->get();
            $history = ItemPackageHistory::where('item_package_id', base64_decode($id))->with('users')->with('packages')->get();
            return view('admin.paket.edit', compact('item', 'brand', 'character', 'unit', 'BrandDetail', 'formulas', 'history'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'number_part' => 'required',
            'f_nama' => 'required',
            'merk' => 'required',
            'kodePart' => 'required',
            'motor' => 'required',
            'unit' => 'required',
            'het' => 'required',
            'harga_pokok' => 'required',
            'harga_beli' => 'required',
            'stok' => 'required',
            'min_stok' => 'required',
            'max_stok' => 'required',
            'barcode' => 'required',
            'berat' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = ItemPackage::where('item_package_id', base64_decode($id))->update([
                'item_package_code' => $request->input('number_part'),
                'brand_detail_id' => $request->input('kodePart'),
                'item_package_name' => $request->input('f_nama'),
                'unit_id' => $request->input('unit'),
                'character_id' => implode(',', $request->input('motor')),
                'item_package_het' => $request->input('het'),
                'item_package_price_primary' => $request->input('harga_pokok'),
                'item_package_price_buy' => $request->input('harga_beli'),
                'item_package_stock' => $request->input('stok'),
                'item_package_stock_min' => $request->input('min_stok'),
                'item_package_stock_max' => $request->input('max_stok'),
                'item_package_barcode' => $request->input('barcode'),
                'item_package_weight' => $request->input('berat')
            ]);

            if ($create) {
                # code...
                Alert::success('Paket Barang Berhasil Diupdate', 'Success');
                return redirect('home/master/paket-barang');
            } else {
                Alert::error('Gagal Update Paket Barang', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
