<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alert;
use Auth;
use Access;
use Validator;
use App\Receipt;
use App\ReceiptStorehouse;
use App\ReceiptSychron;
use App\ReceiptPurchase;
use App\ReceiptCardboard;
use App\ReceiptAgency;
use App\ReceiptCardStorehouse;
use App\ReceiptStorehouseDetail;
use App\Cardboard;
use App\Purchase;
use App\PurchaseDetail;
use App\Item;

class ReceiptController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,60);

            $this->permit = $akses->permit_acces;
                
            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            $agency = Receipt::orderBy('receipt_id', 'desc')->get();
            $gudang = ReceiptStorehouse::orderBy('receipt_storehouse_id', 'desc')->get();
            $sync = ReceiptSychron::orderBy('receipt_sychron_id', 'desc')->get();
            return view('admin.receipt.index', compact('agency', 'sync', 'gudang'));
        } else {
            abort(403);
        }
    }

    public function createAgency()
    {
        if (strpos($this->permit, 'c') !== null) {
            # code...
            $purchase = Purchase::orderBy('purchase_id', 'desc')->get();
            return view('admin.receipt.agency.create', compact('purchase'));
        } else {
            abort(403);
        }
    }

    public function postAgency(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'no_beli' => 'required',
            'date' => 'required',
            'suratJalan' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $purchase_id = $request->input('no_beli');

            $create = Receipt::create([
                'receipt_sj' => $request->input('suratJalan'),
                'receipt_date' => $request->input('date'),
                'receipt_code' => $this->formatAgencyCode(),
                'purchase_id' => (is_array($purchase_id) ? implode(',', $request->input('no_beli')) : $request->input('no_beli')),
            ]);

            $count = (is_array($purchase_id) ? count($purchase_id) : 1);

            ReceiptPurchase::where('receipt_id', $create->receipt_id)->delete();

            for ($i = 0; $i < $count; $i++) {
                ReceiptPurchase::create([
                    'receipt_id' => $create->receipt_id,
                    'purchase_id' => $purchase_id[$i]
                ]);
            }

            if ($create) {
                # code...
                Alert::success('Submit Berhasil', 'Success');
                return redirect('home/pembelian/receipt/form-agency/'.base64_encode($create->receipt_id));
            } else {
                Alert::error('Gagal Submit Data', 'Error');
                return redirect()->back();
            }
        }
    }

    public function generateAgency($id)
    {
        $receipt = Receipt::where('receipt_id', base64_decode($id))->first();
        $purchased = ReceiptPurchase::where('receipt_id', base64_decode($id))->get();
        $purchase = Purchase::orderBy('purchase_id', 'desc')->get();
        $cardboard = ReceiptCardboard::select('receipt_cardboards.*', 'b.cardboard_type_name')
                                    ->join('cardboard_types as b', 'b.cardboard_type_id', 'receipt_cardboards.cardboard_type_id')
                                    ->where('receipt_cardboards.receipt_id', base64_decode($id))->get();
        return view('admin.receipt.agency.show', compact('receipt', 'purchase', 'purchased', 'cardboard'));
    }

    public function addAgencyCardboard(Request $request)
    {
        $create = ReceiptCardboard::create([
            'receipt_cardboard_barcode' => $request->barcode,
            'receipt_cardboard_code' => '',
            'receipt_id' => $request->id,
            'cardboard_type_id' => 1
        ]);

        if ($create) {
            # code...
            return response(['status' => 'success', 'message' => 'Kardus Berhasil Ditambahkan'], 200);
        } else {
            return response(['status' => 'error', 'message' => 'Gagal Menambahkan Kardus'], 200);
        }
    }

    public function deleteCardboardAgency(Request $request)
    {
        $del = ReceiptCardboard::where('receipt_cardboard_id', $request->kardus)->where('receipt_id', $request->id)->delete();
        if ($del) {
            # code...
            return response(['status' => 'success', 'message' => 'Kardus Berhasil Dihapus'], 200);
        } else {
            return response(['status' => 'error', 'message' => 'Gagal Menghapus Kardus'], 200);
        }
    }

    public function entryCardboardAgency($id, $cardboard_id)
    {
        $list = ReceiptCardboard::where('receipt_id', base64_decode($id))->get();
        $item = PurchaseDetail::select('purchase_details.*', 'c.item_code', 'c.item_name')
                                ->join('receipt_purchases as b', 'b.purchase_id', 'purchase_details.purchase_id')
                                ->join('items as c', 'c.item_id', 'purchase_details.item_id')
                                ->where('b.receipt_id', base64_decode($id))->get();
        $list_detail = $this->list_detail_agency($id, $cardboard_id);
        return view('admin.receipt.agency.entry', compact('list', 'item', 'list_detail', 'cardboard_id', 'id'));
    }

    public function getPurchaseDetail(Request $request)
    {
        $id = $request->id;
        $purchase = $this->get_purchase_detail_id($id);
        $data['item_code']  = $purchase['item_code'];
        $data['item_name']  = $purchase['item_name'];
        $data['item_het']   = $purchase['item_het'];
        $data['item_id']    = $purchase['item_id'];
        $data['outstanding'] = Access::hitung_outstanding($id);

        return response(['status' => true, 'message' => 'Ditemukan', 'content' => $data], 200);
    }

    public function addAgencyDetail(Request $request)
    {
        PurchaseDetail::where('purchase_detail_id', $request->detail_id)->update([
            'purchase_detail_accumulation' => $request->qty
        ]);

        $create = ReceiptAgency::create([
            'receipt_cardboard_id' => $request->dos,
            'purchase_detail_id' => $request->detail_id,
            'receipt_agency_qty' => $request->qty,
            'item_id' => $request->item_id
        ]);

        Item::where('item_id', $request->item_id)->update([
            'item_het' => $request->het
        ]);

        if ($create) {
            # code...
            return response(['status' => 'success', 'message' => 'Berhasil Menambahkan Detail'], 200);
        } else {
            return response(['status' => 'error', 'message' => 'Gagal Menambahkan Detail'], 200);
        }
    }

    public function createGudang()
    {
        if (strpos($this->permit, 'c') !== null) {
            # code...
            return view('admin.receipt.gudang.create');
        } else {
            abort(403);
        }
    }

    public function postGudang(Request $request)
    {
        $cek = ReceiptStorehouse::where('receipt_storehouse_date', $request->date)->get();
        if (count($cek) <= 0) {
            # code...
            $create = ReceiptStorehouse::create([
                'receipt_storehouse_date' => $request->date
            ]);

            if ($create) {
                # code...
                Alert::success('Data Berhasil Dimuat', 'Success');
                return redirect('home/pembelian/receipt/form-gudang/'.base64_encode($create->receipt_storehouse_id));
            } else {
                Alert::error('Gagal Memuat Data', 'Error');
                return redirect()->back();
            }
        } else {
            Alert::info('Tanggal Sudah Ada', 'Info');
            return redirect()->back();
        }
    }

    public function generateGudang($id)
    {
        $receipt = ReceiptStorehouse::where('receipt_storehouse_id', base64_decode($id))->first();
        $list = ReceiptCardStorehouse::select('receipt_card_storehouses.*', 'b.*')->join('receipt_cardboards as b', 'b.receipt_cardboard_id', 'receipt_card_storehouses.receipt_cardboard_id')->where('receipt_card_storehouses.receipt_storehouse_id', base64_decode($id))->get();
        return view('admin.receipt.gudang.show', compact('receipt', 'list'));
    }

    public function addStorehouseCardboard(Request $request)
    {
        $cek = ReceiptCardboard::where('receipt_cardboard_barcode', $request->barcode)->first();
        if (empty($cek)) {
            # code...
            return response(['status' => 'error', 'message' => 'Gagal! Barang Tidak Termasuk Dalam Pesanan'], 200);
        } else {
            $create = ReceiptCardStorehouse::create([
                'receipt_cardboard_id' => $cek['receipt_cardboard_id'],
                'receipt_storehouse_id' => $request->id
            ]);

            if ($create) {
                # code...
                return response(['status' => 'success', 'message' => 'Barang Berhasil Ditambahkan'], 200);
            } else {
                return response(['status' => 'error', 'message' => 'Gagal Menambahkan Barang'], 200);
            }
        }
    }

    public function entryCardboardGudang($id, $kardus_id)
    {
        $list = ReceiptStorehouseDetail::select('receipt_storehouse_details.*', 'b.item_code', 'b.item_name', 'b.item_barcode')
                                    ->join('items as b', 'b.item_id', 'receipt_storehouse_details.item_id')
                                    ->where('receipt_storehouse_details.receipt_card_storehouse_id', $kardus_id)->get();

        return view('admin.receipt.gudang.entry', compact('list', 'kardus_id'));
    }

    public function addDetailGudang(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'i_number_part' => 'required',
            'cardboard_id' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Gagal Menambahkan Barang', 'Info');
            return redirect()->back();
        } else {
            $item = Item::where('item_barcode', $request->i_number_part)->first();
            if (empty($item)) {
                # code...
                Alert::error('Gagal! Barang Tidak Termasuk Dalam Pesanan', 'Error');
                return redirect()->back();
            } else {
                $cardboard_details = ReceiptStorehouseDetail::where('receipt_card_storehouse_id', $request->cardboard_id)->where('item_id', $item['item_id'])->first();
                if ($cardboard_details) {
                    # code...
                    $update = ReceiptStorehouseDetail::where('receipt_storehouse_detail_id', $cardboard_details['receipt_storehouse_detail_id'])->update([
                        'receipt_storehouse_detail_qty' => $cardboard_details['receipt_storehouse_detail_qty'] + 1
                    ]);

                    if ($update) {
                        # code...
                        Alert::success('Barang Berhasil Diperbarui', 'Success');
                        return redirect()->back();
                    } else {
                        Alert::error('Gagal Memperbarui Barang', 'Error');
                        return redirect()->back();
                    }
                } else {
                    $create = ReceiptStorehouseDetail::create([
                        'item_id' => $item['item_id'],
                        'receipt_storehouse_detail_qty' => 1,
                        'receipt_card_storehouse_id' => $request->cardboard_id
                    ]);

                    if ($create) {
                        # code...
                        Alert::success('Barang Berhasil Ditambahkan', 'Success');
                        return redirect()->back();
                    } else {
                        Alert::error('Gagal Menambahkan Barang', 'Error');
                        return redirect()->back();
                    }
                }
            }
        }
    }

    public function updateDetailQtyGudang($id, $qty)
    {
        $valid = Validator::make($request->all(), [
            'id' => 'required',
            'qty' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => 'error', 'message' => 'Form Tidak Lengkap'], 200);
        } else {
            $update = ReceiptStorehouseDetail::where('receipt_storehouse_detail_id', $id)->update([
                'receipt_storehouse_detail_qty' => $qty
            ]);

            if ($update) {
                # code...
                return response(['status' => 'success', 'message' => 'Qty Berhasil Diperbarui'], 200);
            } else {
                return response(['status' => 'error', 'message' => 'Gagal Memperbarui Qty Barang'], 200);
            }
        }
    }

    public function createSync()
    {
        if (strpos($this->permit, 'c') !== null) {
            # code...
            return view('admin.receipt.sync.create');
        } else {
            abort(403);
        }
    }

    public function postSyncro(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'date' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $cek = ReceiptStorehouse::where('receipt_storehouse_date', $request->date)->get();
            if (count($cek) <= 0) {
                # code...
                $create = ReceiptSychron::create([
                    'receipt_sychron_date' => $request->date
                ]);

                if ($create) {
                    # code...
                    Alert::success('Data Berhasil Diproses', 'Success');
                    return redirect('home/pembelian/receipt/form-sync/'.base64_encode($create->receipt_sychron_id));
                } else {
                    Alert::error('Gagal Memproses Data', 'Error');
                    return redirect()->back();
                }
            } else {
                Alert::info('Sinkronisasi Tanggal Tsb Sudah Ada', 'Info');
                return redirect()->back();
            }
        }
    }

    public function generateSync($id)
    {
        $receipt = ReceiptSychron::where('receipt_sychron_id', base64_decode($id))->first();
        $list = ReceiptCardboard::select('receipt_cardboards.*', \DB::raw('sum(c.receipt_agency_qty) as kantor'), 'b.receipt_date')
                                ->join('receipts as b', 'b.receipt_id', 'receipt_cardboards.receipt_id')
                                ->join('receipt_agencies as c', 'c.receipt_cardboard_id', 'receipt_cardboards.receipt_cardboard_id')
                                ->where('b.receipt_date', $receipt['receipt_storehouse_date'])
                                ->groupBy('receipt_cardboards.receipt_cardboard_id')
                                ->get();
        return view('admin.receipt.sync.show', compact('receipt', 'list'));
    }

    public function deleteAgency(Request $request)
    {
        $del = Receipt::where('receipt_id', $request->id)->delete();
        ReceiptCardboard::where('receipt_id', $request->id)->delete();
        if ($del) {
            # code...
            return response(['status' => 'success', 'message' => 'Penerimaan Berhasil Dihapus'], 200);
        } else {
            return response(['status' => 'error', 'message' => 'Gagal Menghapus Penerimaan'], 200);
        }
    }

    public function deleteGudang(Request $request)
    {
        $del = ReceiptStorehouse::where('receipt_storehouse_id', $request->id)->delete();
        ReceiptCardStorehouse::where('receipt_storehouse_id', $request->id)->delete();
        if ($del) {
            # code...
            return response(['status' => 'success', 'message' => 'Penerimaan Berhasil Dihapus'], 200);
        } else {
            return response(['status' => 'error', 'message' => 'Gagal Menghapus Penerimaan'], 200);
        }
    }

    public function deleteSync(Request $request)
    {
        $del = ReceiptSychron::where('receipt_sychron_id', $request->id)->delete();
        if ($del) {
            # code...
            return response(['status' => 'success', 'message' => 'Penerimaan Berhasil Dihapus'], 200);
        } else {
            return response(['status' => 'error', 'message' => 'Gagal Menghapus Penerimaan'], 200);
        }
    }

    protected function read_id_receipt_agency($id)
    {
        $query = ReceiptAgency::where('receipt_agency_id', $id)->get();
        $result = null;
        foreach($query as $row) $result = ($row);
        return $result;
    }

    protected function get_purchase_detail_id($id)
    {
        $query = PurchaseDetail::select('purchase_details.*', 'c.item_code', 'c.item_name', 'c.item_het')
                                ->join('items as c', 'c.item_id', 'purchase_details.item_id')
                                ->where('purchase_details.purchase_detail_id', $id)->get();
        $result = null;
        foreach ($query as $row) $result = ($row);
        return $result;
    }

    protected function list_detail_agency($id, $dos)
    {
        $query = ReceiptAgency::select('receipt_agencies.*', 'b.item_id as item_id_detail', 'c.item_code', 'c.item_name', 'c.item_het', 'd.item_code as item_code2', 'd.item_name as item_name2', 'd.item_het as item_het2')->join('purchase_details as b', 'b.purchase_detail_id', 'receipt_agencies.purchase_detail_id')->join('items as c', 'c.item_id', 'b.item_id')->join('items as d', 'd.item_id', 'receipt_agencies.item_id')->where('receipt_agencies.receipt_cardboard_id', $dos)->get();

        if (count($query) == 0)
        return array();
        $data = $query;
        foreach ($data as $index => $row) {}
        return $data;
    }

    protected function listPurchase($id)
    {
        $query = ReceiptPurchase::where('receipt_id', base64_decode($id))->get();
        $result = null;
        foreach ($query as $row) $result = ($row);
        return $result;
    }

    protected function formatAgencyCode()
    {
        $code = '';
        $year =date('y');

        switch (date('m')) {
            case '01': $month = "A"; break;
            case '02': $month = "B"; break;
            case '03': $month = "C"; break;
            case '04': $month = "D"; break;
            case '05': $month = "E"; break;
            case '06': $month = "F"; break;
            case '07': $month = "G"; break;
            case '08': $month = "H"; break;
            case '09': $month = "I"; break;
            case '10': $month = "J"; break;
            case '11': $month = "K"; break;
            case '12': $month = "L"; break;
        }

        $likes = 'DO-'.$year.$month;
        $findMemo = Receipt::select('receipt_code')->where('receipt_code', 'LIKE', '%'.$likes.'%')->orderBy('receipt_code', 'desc')->first();

        $number = 0;

        if (empty($findMemo)) {
            $number = sprintf("%0" . 4 . "d", $number + 1);
            $code = 'DO-'.$year.$month.$number;
            return $code;
        }

        $number = intval(substr($findMemo->receipt_code, -4));
        $number = sprintf("%0" . 4 . "d", $number + 1);
        $code = 'DO-'.$year.$month.$number;
        return $code;
    }
}
