<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Access;
use Alert;
use Validator;
use App\UserType;
use App\Permit;
use App\Menu;

class RolesController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,30);

            $this->permit = $akses->permit_acces;

            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            $roles = UserType::orderBy('user_type_name', 'asc')->get();
            return view('admin.roles.index', compact('roles'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            $menu = Menu::where('side_menu_level', 1)->get();
            $side = Menu::where('side_menu_parent', '!=', 0)->get();
            $child = Menu::where('side_menu_level', 3)->get();
            return view('admin.roles.create', compact('menu', 'side', 'child'));
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'nama' => 'required'
        ]);

        if ($valid->fails()) {
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = UserType::create([
                'user_type_name' => $request->input('nama')
            ]);

            $menu = Menu::all();

            foreach($menu as $key => $val) {
                $crud = $request->input('permit'.$val['side_menu_id']);
                if (is_array($crud)) {
                    $crud_val = implode(',', $crud);
                } else {
                    $crud_val = '';
                }

                Permit::create([
                    'user_type_id' => $create->user_type_id,
                    'side_menu_id' => $val->side_menu_id,
                    'permit_acces' => $crud_val
                ]);
            }

            if ($create) {
                Alert::success('Role Berhasil Ditambahkan', 'Success');
                return redirect('home/setting/roles');
            } else {
                Alert::error('Gagal Menambahkan Role', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strpos($this->permit, 'u') !== null) {
            $roles = UserType::where('user_type_id', base64_decode($id))->first();
            $menu = Menu::where('side_menu_level', 1)->get();
            $side = Menu::select('side_menus.*', 'b.permit_acces')->join('permits as b', 'b.side_menu_id', 'side_menus.side_menu_id')->where('b.user_type_id', base64_decode($id))->where('side_menu_parent', '!=', 0)->get();
            $child = Menu::select('side_menus.*', 'b.permit_acces')->join('permits as b', 'b.side_menu_id', 'side_menus.side_menu_id')->where('b.user_type_id', base64_decode($id))->where('side_menu_level', 3)->get();
            return view('admin.roles.edit', compact('menu', 'side', 'child', 'roles'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'nama' => 'string'
        ]);

//        dd($request->all());

        if ($valid->fails()) {
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = UserType::where('user_type_id', base64_decode($id))->update([
                'user_type_name' => $request->input('nama')
            ]);

            $menu = Menu::all();

            foreach($menu as $key => $val) {
                $crud = $request->input('permit'.$val['side_menu_id']);
                if (is_array($crud)) {
                    $crud_val = implode(',', $crud);
                } else if($val->side_menu_type_parent == 1) {
                    $crud_val = 1;
                } else {
                    $crud_val = '';
                }

                    \DB::table('permits')->where('user_type_id', base64_decode($id))->where('side_menu_id', $val->side_menu_id)->update([
                        'user_type_id' => base64_decode($id),
                        'side_menu_id' => $val->side_menu_id,
                        'permit_acces' => $crud_val
                    ]);
            }

            if ($create) {
                Alert::success('Role Berhasil Diupdate', 'Success');
                return redirect('home/setting/roles');
            } else {
                Alert::error('Gagal Update Role', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            Permit::where('user_type_id', base64_decode($id))->delete();
            $del = UserType::findOrFail(base64_decode($id));
            if ($del->delete()) {
                Alert::success('Role Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menghapus Role', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403);
        }
    }
}
