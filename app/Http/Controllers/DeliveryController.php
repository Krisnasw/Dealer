<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Access;
use Auth;
use Alert;
use Validator;
use App\Employee;
use App\Delivery;
use App\DeliveryDetail;
use App\Cardboard;
use App\DeliveryBack;
use App\DeliveryPackaging;
use App\DeliveryBackDetail;
use App\Packaging;

class DeliveryController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,58);

            $this->permit = $akses->permit_acces;
                
            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            $delivery = Delivery::select('deliveries.*', 'b.employee_name as sopir', 'c.employee_name as kernet')
                                ->join('employees as b', 'b.employee_id', 'deliveries.delivery_sopir')
                                ->join('employees as c', 'c.employee_id', 'deliveries.delivery_kernet')
                                ->get();
            return view('admin.delivery.index', compact('delivery'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            # code...
            $packaging = Packaging::orderBy('packaging_sj', 'desc')->get();
            $employee = Employee::all();
            $kernet = $employee->where('position_id', 3);
            $sopir = $employee->where('position_id', 2);
            return view('admin.delivery.create', compact('kernet', 'packaging', 'sopir'));
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'sj' => 'required',
            'tgl' => 'required',
            'sopir' => 'required',
            'kernet' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Delivery::create([
                'delivery_code' => $this->format_code(),
                'delivery_date' => $request->tgl,
                'delivery_sopir' => $request->sopir,
                'delivery_kernet' => $request->kernet,
                'packaging_id' => $request->sj
            ]);

            $arrLength = is_array($request->sj) ? count($request->sj) : 1;
            DeliveryPackaging::where('delivery_id', $create->delivery_id)->delete();

            for($i = 0; $i < $arrLength; $i++) {
                DeliveryPackaging::create([
                    'delivery_id' => $create->delivery_id,
                    'packaging_id' => $request->sj[$i]
                ]);

                Packaging::where('packaging_id', $request->sj[$i])->update([
                    'packaging_status' => 1
                ]);
            }

            if ($create) {
                # code...
                return redirect('home/gudang/delivery-back/'.$create->delivery_id);
            } else {
                Alert::error('Gagal Membuat Pengiriman', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strpos($this->permit, 'u') !== null) {
            # code...
            $deliv = Delivery::where('delivery_id', base64_decode($id))->first();

            $packaging = Packaging::orderBy('packaging_sj', 'desc')->get();
            $employee = Employee::all();
            $kernet = $employee->where('position_id', 3);
            $sopir = $employee->where('position_id', 2);

            $list_packaging = DeliveryPackaging::select('delivery_packagings.*', 'b.packaging_sj')
                                                ->join('packagings as b', 'b.packaging_id', 'delivery_packagings.packaging_id')
                                                ->where('delivery_packagings.delivery_id', base64_decode($id))->get();

            $list_detail = DeliveryDetail::select('b.*', 'c.packaging_sj', 'd.cardboard_type_name')
                                        ->join('cardboards as b', 'b.cardboard_id', 'delivery_details.cardboard_id')
                                        ->join('packagings as c', 'c.packaging_id', 'b.packaging_id')
                                        ->join('cardboard_types as d', 'd.cardboard_type_id', 'b.cardboard_type_id')
                                        ->where('delivery_details.delivery_id', base64_decode($id))->get();

            $list_back = DeliveryBack::select('delivery_backs.*', 'b.packaging_sj')
                                ->join('packagings as b', 'b.packaging_id', 'delivery_backs.packaging_id')
                                ->where('delivery_backs.delivery_id', base64_decode($id))->get();

            return view('admin.delivery.edit', compact('deliv', 'packaging', 'kernet', 'sopir', 'list_packaging', 'list_detail', 'list_back'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'sj' => 'required',
            'tgl' => 'required',
            'sopir' => 'required',
            'kernet' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Delivery::where('delivery_id', base64_decode($id))->update([
                'delivery_date' => $request->tgl,
                'delivery_sopir' => $request->sopir,
                'delivery_kernet' => $request->kernet,
                'packaging_id' => $request->sj
            ]);

            $arrLength = is_array($request->sj) ? count($request->sj) : 1;
            DeliveryPackaging::where('delivery_id', base64_decode($id))->delete();

            for($i = 0; $i < $arrLength; $i++) {
                DeliveryPackaging::where('delivery_id', base64_decode($id))->update([
                    'packaging_id' => $request->sj[$i]
                ]);

                Packaging::where('packaging_id', $request->sj[$i])->update([
                    'packaging_status' => 1
                ]);
            }

            if ($create) {
                # code...
                return redirect('home/gudang/delivery-back/'.$create->delivery_id);
            } else {
                Alert::error('Gagal Update Pengiriman', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            # code...
            $del = Delivery::findOrFail(base64_decode($id));
            if ($del->delete()) {
                # code...
                Alert::success('Delivery Berhasil Dihapus', 'success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menghapus Delivery', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403);
        }
    }

    public function getHoldedList($id)
    {
        $data = Delivery::where('delivery_id', $id)->first();

        $packaging = Packaging::orderBy('packaging_sj', 'desc')->get();
        $employee = Employee::all();
        $kernet = $employee->where('position_id', 3);
        $sopir = $employee->where('position_id', 2);

        $list_packaging = DeliveryPackaging::select('delivery_packagings.*', 'b.packaging_sj')
                                            ->join('packagings as b', 'b.packaging_id', 'delivery_packagings.packaging_id')
                                            ->where('delivery_packagings.delivery_id', $id)->get();

        $list_detail = DeliveryDetail::select('b.*', 'c.packaging_sj', 'd.cardboard_type_name')
                                    ->join('cardboards as b', 'b.cardboard_id', 'delivery_details.cardboard_id')
                                    ->join('packagings as c', 'c.packaging_id', 'b.packaging_id')
                                    ->join('cardboard_types as d', 'd.cardboard_type_id', 'b.cardboard_type_id')
                                    ->where('delivery_details.delivery_id', $id)->get();

        $list_back = DeliveryBack::select('delivery_backs.*', 'b.packaging_sj')
                            ->join('packagings as b', 'b.packaging_id', 'delivery_backs.packaging_id')
                            ->where('delivery_backs.delivery_id', $id)->get();

        return view('admin.delivery.holded', compact('list_back', 'data', 'packaging', 'kernet', 'sopir', 'list_detail', 'list_packaging'));
    }

    public function saveKardus(Request $request)
    {
        $item = Cardboard::select('cardboards.*')->join('delivery_packagings as b', 'b.packaging_id', 'cardboards.packaging_id')->where('cardboards.cardboard_barcode', $request->data)
                        ->where('b.delivery_id', $request->id)->first();

        $cek = DeliveryDetail::where('cardboard_id', $item['cardboard_id'])->where('delivery_id', $request->id)->get();

        if ($item) {
            # code...
            if ($cek) {
                # code...
                return response(['status' => 'error', 'message' => 'Kuota Barang Sudah Terpenuhi'], 200);
            } else {
                $create = DeliveryDetail::create([
                    'delivery_id' => $request->id,
                    'cardboard_id' => $item['cardboard_id']
                ]);

                if ($create) {
                    # code...
                    return response(['status' => 'success', 'message' => 'Kardus Berhasil Ditambahkan'], 200);
                } else {
                    return response(['status' => 'error', 'message' => 'Gagal Menambahkan Kardus'], 200);
                }
            }
        } else {
            return response(['status' => 'error', 'message' => 'Kardus Tidak Termasuk Dalam Surat Jalan'], 200);
        }
    }

    public function addBack(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'id' => 'required',
            'data' => 'required',
            'packaging' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => 'error', 'message' => 'Form Tidak Lengkap'], 200);
        } else {
            $create = DeliveryBack::create([
                'delivery_id' => $request->id,
                'packaging_id' => $request->packaging,
                'delivery_back_note' => $request->data
            ]);

            Packaging::where('packaging_id', $request->packaging)->update([
                'packaging_status' => 0
            ]);

            if ($create) {
                # code...
                return response(['status' => 'success', 'message' => 'Pengembalian Berhasil'], 200);
            } else {
                return response(['status' => 'error', 'message' => 'Gagal Melakukan Pengembalian'], 200);
            }
        }
    }

    public function scanKardus($id = 0, $back_id = 0, $packaging = 0)
    {
        $list = DeliveryBackDetail::select('b.*', 'c.cardboard_type_name')->join('cardboards as b', 'b.cardboard_id', 'delivery_back_details.cardboard_id')
                            ->join('cardboard_types as c', 'c.cardboard_type_id', 'b.cardboard_type_id')
                            ->where('delivery_back_details.delivery_back_id', $id)->get();
        $data['delivery_id'] = $id;
        $data['back_id'] = $back_id;
        $data['packaging_id'] = $packaging;
        return view('admin.delivery.scan', compact('list', 'data'));
    }

    public function submitScanKardus(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'val' => 'required',
            'id' => 'required',
            'back' => 'required',
            'pack' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => 'error', 'message' => 'Form Tidak Lengkap'], 200);
        } else {
            $item = Cardboard::where('cardboard_barcode', $request->val)->where('packaging_id', $request->pack)->first();
            $cek = DeliveryBackDetail::where('cardboard_id',  $item['cardboard_id'])->where('delivery_back_id', $request->back)->get();

            if ($item) {
                # code...
                if ($cek) {
                    # code...
                    return response(['status' => 'error', 'message' => 'Kuota Barang Sudah Terpenuhi'], 200);
                } else {
                    $create = DeliveryBackDetail::create([
                        'delivery_back_id' => $request->back,
                        'cardboard_id' => $item['cardboard_id']
                    ]);

                    if ($create) {
                        # code...
                        return response(['status' => 'success', 'message' => 'Kardus Berhasil Ditambahkan'], 200);
                    } else {
                        return response(['status' => 'error', 'message' => 'Gagal Menambahkan Kardus'], 200);
                    }
                }
            } else {
                return response(['status' => 'error', 'message' => 'Gagal! Barang Tidak Termasuk Dalam Pesanan'], 200);
            }
        }
    }

    public function check_kuota(Request $request)
    {
        $cek = Cardboard::select(\DB::raw('COUNT(cardboards.cardboard_id) as total'))
                                        ->join('packagings as b', 'b.packaging_id', 'cardboards.packaging_id')
                                        ->join('delivery_packagings as c', 'c.packaging_id', 'cardboards.packaging_id')
                                        ->where('c.delivery_id', $request->id)->get();
        $result = null;
        foreach($cek as $row) $result = ($row);
        $data['packaging'] = $result['total'];

        $cek2 = DeliveryDetail::select(\DB::raw('COUNT(delivery_detail_id) as total'))->where('delivery_id', $request->id)->get();
        $result2 = null;
        foreach($cek2 as $rows) $result2 = ($rows);
        $data['delivery'] = $result2['total'];

        $response['type']       = 'spbe';
        $response['content']    = $data;
        $response['param']      = '';

        return response($response);
    }

    protected function format_code()
    {
        $code = '';
        $year =date('y');

        switch (date('m')) {
            case '01': $month = "A"; break;
            case '02': $month = "B"; break;
            case '03': $month = "C"; break;
            case '04': $month = "D"; break;
            case '05': $month = "E"; break;
            case '06': $month = "F"; break;
            case '07': $month = "G"; break;
            case '08': $month = "H"; break;
            case '09': $month = "I"; break;
            case '10': $month = "J"; break;
            case '11': $month = "K"; break;
            case '12': $month = "L"; break;
        }

        $likes = 'DV-'.$year.$month;
        $findMemo = Delivery::select('delivery_code')->where('delivery_code', 'LIKE', '%'.$likes.'%')->orderBy('delivery_id', 'desc')->first();

        $number = 0;

        if (empty($findMemo)) {
            $number = sprintf("%0" . 4 . "d", $number + 1);
            $code = 'DV-'.$year.$month.$number;
            return $code;
        }

        $number = intval(substr($findMemo->delivery_code, -4));
        $number = sprintf("%0" . 4 . "d", $number + 1);
        $code = 'DV-'.$year.$month.$number;
        return $code;
    }
}