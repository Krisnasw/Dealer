<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Alert;

class HomeController extends Controller
{
    //
	public function __construct()
	{
		$this->middleware('auth');
	}

    public function index() {
    	return view('admin.main.index');
    }

    public function doLogout() {
    	Auth::logout();
    	Alert::info('Logout Berhasil', 'Info');
    	return redirect('/');
    }
}