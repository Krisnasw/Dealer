<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Access;
use Alert;
use Validator;
use App\ItemRak;

class RakController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,56);

            $this->permit = $akses->permit_acces;
                
            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            $rak = ItemRak::orderBy('item_rak_id', 'ASC')->get();
            return view('admin.rak.index', compact('rak'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            # code...
            return view('admin.rak.create');
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = ItemRak::create([
                'item_rak_name' => $request->input('name')
            ]);

            if ($create) {
                # code...
                Alert::success('Rak Berhasil Dibuat', 'Success');
                return redirect('home/master/rak-barang');
            } else {
                Alert::error('Gagal Membuat Rak Baru', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strpos($this->permit, 'u') !== null) {
            # code...
            $rak = ItemRak::where('item_rak_id', base64_decode($id))->first();
            return view('admin.rak.edit', compact('rak'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = ItemRak::where('item_rak_id', base64_decode($id))->update([
                'item_rak_name' => $request->input('name')
            ]);

            if ($create) {
                # code...
                Alert::success('Rak Berhasil Diupdate', 'Success');
                return redirect('home/master/rak-barang');
            } else {
                Alert::error('Gagal Update Rak', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            # code...
            $del = ItemRak::findOrFail(base64_decode($id));
            if ($del->delete()) {
                # code...
                Alert::success('Rak Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Rak Gagal Dihapus', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403);
        }
    }
}
