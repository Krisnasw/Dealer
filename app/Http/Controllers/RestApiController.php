<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Item;
use App\Customer;
use App\Grade;
use App\City;
use App\Brand;
use App\Sales;
use App\Memo;
use App\MemoDetail;
use App\Cardboard;
use App\CardboardStatus;
use App\Promo;
use Auth;
use Validator;
use JWTAuth;

class RestApiController extends Controller
{
    //
    public function doLogin(Request $request)
    {
    	$credentials = $request->only('user_username', 'password');
        
        $rules = [
            'user_username' => 'required',
            'password' => 'required'
        ];

        $validator = Validator::make($credentials, $rules);

        if($validator->fails()) {
            return response()->json(['status'=> false, 'message'=> 'Form tidak valid']);
        }
        
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['status' => false, 'message' => 'Akun Tidak Ditemukan.'], 200);
            }
        } catch (JWTException $e) {
            return response()->json(['status' => false, 'message' => 'Failed to login, please try again.'], 200);
        }

        return response()->json(['status' => true, 'message'=> $token], 200);
    }

    public function getUserData(Request $request)
    {
    	$this->validate($request, ['token' => 'required']);

    	$user = JWTAuth::toUser($request->token);

    	return response(['status' => true, 'message' => 'User Ditemukan', 'data' => $user['user_first_name']], 200);
    }

    public function getBunchOfItem(Request $request)
    {
    	$this->validate($request, ['token' => 'required']);

    	$item = Item::select('item_name', 'item_stock', 'item_code')->where('item_name', '!=', "")->orderBy('item_name', 'asc')->get();
    	if (count($item) <= 0) {
    		# code...
    		return response(['status' => false, 'message' => 'Item Kosong', 'data' => []], 200);
    	} else {
    		return response(['status' => true, 'message' => 'Item Ditemukan', 'data' => $item], 200);
    	}
    }

    public function searchItem(Request $request)
    {
    	$this->validate($request, ['token' => 'required', 'title' => 'required']);

    	$item = Item::select('item_name', 'item_stock', 'item_code')->where('item_name', 'LIKE', '%'.$request->title.'%')->orWhere('item_code', 'LIKE', '%'.$request->title.'%')->orderBy('item_name', 'asc')->get();
    	if (count($item) <= 0) {
    		# code...
    		return response(['status' => false, 'message' => 'Item Tidak Ada', 'data' => []], 200);
    	} else {
    		return response(['status' => true, 'message' => 'Item Ditemukan', 'data' => $item], 200);
    	}
    }

    public function addCustomer(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'token' => 'required',
            'customerCode' => 'required',
            'customerName' => 'required',
            'customerToko' => 'required',
            'customerAddress' => 'required',
            'customerPhone' => 'required',
            'customerEmail' => 'required',
            'customerGrade' => 'required',
            'customerRekening' => 'required',
            'customerBank' => 'required',
            'customerNPWP' => 'required',
            'customerNpwpName' => 'required',
            'customerEkspedisi' => 'required',
            'customerHp' => 'required',
            'customerFax' => 'required',
            'customerWilayah' => 'required',
            'customerLat' => 'required',
            'customerLong' => 'required',
            'customerImg' => 'required|image'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => false, 'message' => 'Form Tidak Lengkap'], 200);
        } else {

            $grade = Grade::where('grade_nama', $request->customerGrade)->first();
            $kota = City::where('city_name', $request->customerWilayah)->first();

            $create = Customer::create([
                'customer_code' => $request->input('customerCode'),
                'customer_name' => $request->input('customerName'),
                'customer_phone' => $request->input('customerPhone'),
                'customer_addres' => $request->input('customerAddress'),
                'customer_email' => $request->input('customerEmail'),
                'customer_npwp' => $request->input('customerNPWP'),
                'grade_id' => $grade['grade_id'],
                'customer_rekening' => $request->input('customerRekening'),
                'customer_id_bank' => $request->input('customerBank'),
                'customer_nama_npwp' => $request->input('customerNpwpName'),
                'customer_expedisi' => $request->input('customerEkspedisi'),
                'customer_tempo' => 0,
                'customer_limit' => 0,
                'customer_hp' => $request->input('customerHp'),
                'customer_fax' => $request->input('customerFax'),
                'city_id' => $kota['city_id'],
                'customer_store' => $request->input('customerToko'),
                'customer_latitude' => $request->customerLat,
                'customer_longitude' => $request->customerLong,
                'customer_status' => 1,
                'customer_img' => ($request->hasFile('customerImg') ? $this->savePhoto($request->file('customerImg')) : NULL)
            ]);

            if ($create) {
                # code...
                return response(['status' => true, 'message' => 'Customer Berhasil Dibuat'], 200);
            } else {
                return response(['status' => false, 'message' => 'Gagal Membuat Customer'], 200);
            }
        }
    }

    public function getGrade(Request $request)
    {
        $this->validate($request, ['token' => 'required']);

        $brand = Grade::select('grade_id', 'grade_nama')->orderBy('grade_nama', 'asc')->get();
        if (count($brand) <= 0) {
            # code...
            return response(['status' => false, 'message' => 'Grade Tidak Ada', 'data' => []], 200);
        } else {
            return response(['status' => true, 'message' => 'Grade Ditemukan', 'data' => $brand], 200);
        }
    }

    public function getWilayah(Request $request)
    {
        $this->validate($request, ['token' => 'required']);

        $city = City::select('city_id', 'city_name')->orderBy('city_name', 'asc')->get();
        if (count($city) <= 0) {
            # code...
            return response(['status' => false, 'message' => 'Wilayah Tidak Ditemukan', 'data' => []], 200);
        } else {
            return response(['status' => true, 'message' => 'Wilayah Ditemukan', 'data' => $city], 200);
        }
    }

    public function addMemo(Request $request)
    {
        $this->validate($request, ['token' => 'required', 'customer' => 'required', 'merk' => 'required', 'sales' => 'required']);

        $customer = Customer::where('customer_name', $request->customer)->first();
        $merk = Brand::where('brand_name', $request->merk)->first();
        $sales = Sales::where('sales_name', $request->sales)->first();

        if ($customer != null && $merk != null && $sales != null) {
            # code...
            $num = $this->formatMemoCode($request->merk);
            $create = Memo::create([
                'memo_code' => $num,
                'memo_date' => date('Y-m-d'),
                'sales_id' => $sales['sales_id'],
                'customer_id' => $customer['customer_id'],
                'memo_type_pay' => 0,
                'brand_id' => $merk['brand_id']
            ]);

            $cek = MemoDetail::where('memo_id', 0)->get();

            for ($i=0; $i < count($cek); $i++) { 
                # code...
                MemoDetail::where('memo_id', 0)->update([
                    'memo_id' => $create->memo_id
                ]);
            }

            if ($create) {
                return response(['status' => true, 'message' => 'Memo Berhasil Dibuat'], 200);
            } else {
                return response(['status' => false, 'message' => 'Gagal Membuat Memo'], 200);
            }
        } else {
            return response(['status' => false, 'message' => 'Form Tidak Lengkap'], 200);
        }
    }

    public function getBunchOfMemoDetail(Request $request)
    {
        $this->validate($request, ['token' => 'required']);

        $memoDet = MemoDetail::select('b.item_name', 'memo_details.memo_detail_qty', 'memo_details.memo_accumulation', 'memo_details.memo_detail_discount', 'memo_details.memo_detail_id')->join('items as b', 'b.item_id', 'memo_details.item_id')->where('memo_id', 0)->get();
        if (count($memoDet) <= 0) {
            # code...
            return response(['status' => false, 'message' => 'Barang Belum Diisi', 'data' => []], 200);
        } else {
            return response(['status' => true, 'message' => 'Barang Ditemukan', 'data' => $memoDet], 200);
        }
    }

    public function addMemoDetail(Request $request)
    {
        $this->validate($request, ['token' => 'required', 'item' => 'required', 'qty' => 'required', 'diskon' => 'required']);

        $user = JWTAuth::toUser($request->token);
        $exp = $request->item;
        $exp = explode(' - ', $exp);

        $item = Item::select('item_id')->where('item_code', $exp[0])->first();

        $create = MemoDetail::create([
                    'memo_id' => 0,
                    'item_id' => $item['item_id'],
                    'memo_detail_qty' => $request->input('qty'),
                    'memo_accumulation' => 0,
                    'memo_detail_discount' => $request->input('diskon'),
                    'memo_detail_cancel' => 0,
                    'user_id' => $user['user_id']
                ]);

        if ($create) {
            # code...
            return response(['status' => true, 'message' => 'Barang Berhasil Ditambahkan'], 200);
        } else {
            return response(['status' => false, 'message' => 'Gagal Menambahkan Barang'], 200);
        }
    }

    public function deleteMemoDetail(Request $request)
    {
        $this->validate($request, ['token' => 'required', 'code' => 'required']);

        $user = JWTAuth::toUser($request->token);

        $del = MemoDetail::where('memo_detail_id', $request->code)->where('user_id', $user['user_id'])->delete();
        if ($del) {
            # code...
            return response(['status' => true, 'message' => 'Barang Berhasil Dihapus'], 200);
        } else {
            return response(['status' => false, 'message' => 'Gagal Menghapus Barang'], 200);
        }
    }

    public function updateMemoDetail(Request $request)
    {
        $this->validate($request, ['token' => 'required', 'code' => 'required', 'qty' => 'required']);

        $user = JWTAuth::toUser($request->token);

        $update = MemoDetail::where('memo_detail_id', $request->code)->where('user_id', $user['user_id'])->update([
            'memo_detail_qty' => $request->qty
        ]);

        if ($update) {
            # code...
            return response(['status' => true, 'message' => 'Barang Berhasil Diupdate'], 200);
        } else {
            return response(['status' => false, 'message' => 'Gagal Update Barang'], 200);
        }
    }

    public function getBunchOfCustomer(Request $request)
    {
        $this->validate($request, ['token' => 'required']);

        $customer = Customer::select('customer_id', 'customer_name')->orderBy('customer_name', 'asc')->where('customer_name', '!=', '')->get();
        if (count($customer) <= 0) {
            # code...
            return response(['status' => false, 'message' => 'Customer Tidak Ada', 'data' => []], 200);
        } else {
            return response(['status' => true, 'message' => 'Customer Ditemukan', 'data' => $customer], 200);
        }
    }

    public function getBunchOfSales(Request $request)
    {
        $this->validate($request, ['token' => 'required']);

        $sales = Sales::select('sales_id', 'sales_name')->orderBy('sales_name', 'asc')->get();
        if (count($sales) <= 0) {
            # code...
            return response(['status' => false, 'message' => 'Sales Tidak Ada', 'data' => []], 200);
        } else {
            return response(['status' => true, 'message' => 'Sales Ditemukan', 'data' => $sales], 200);
        }
    }

    public function getBunchOfMerk(Request $request)
    {
        $this->validate($request, ['token' => 'required']);

        $merk = Brand::select('brand_id', 'brand_name')->orderBy('brand_name', 'asc')->get();
        if (count($merk) <= 0) {
            # code...
            return response(['status' => false, 'message' => 'Merk Tidak Ada', 'data' => []], 200);
        } else {
            return response(['status' => true, 'message' => 'Merk Ditemukan', 'data' => $merk], 200);
        }
    }

    public function searchCardboard(Request $request)
    {
        $this->validate($request, ['token' => 'required', 'code' => 'required']);

        $cek = Cardboard::where('cardboard_barcode', $request->code)->get();
        if (count($cek) <= 0) {
            # code...
            return response(['status' => false, 'message' => 'Kode Tidak Ditemukan'], 200);
        } else {
            return response(['status' => true, 'message' => 'Kode Ditemukan'], 200);
        }
    }

    public function createCardboardStatus(Request $request)
    {
        $this->validate($request, ['token' => 'required', 'code' => 'required', 'type' => 'required']);

        $user = JWTAuth::toUser($request->token);
        $cek = Cardboard::where('cardboard_barcode', $request->code)->first();
        if ($cek == null) {
            # code...
            return response(['status' => false, 'message' => 'Kode Tidak Ditemukan'], 200);
        } else {
            if ($request->type == "Terima") {
                # code...
                $create = CardboardStatus::create([
                    'cardboard_id' => $cek['cardboard_id'],
                    'user_id' => $user['user_id'],
                    'cardboard_status_type' => 'terima'
                ]);

                if ($create) {
                    # code...
                    return response(['status' => true, 'message' => 'Penerimaan Berhasil'], 200);
                } else {
                    return response(['status' => false, 'message' => 'Gagal Melakukan Penerimaan'], 200);
                }
            } else {
                $create = CardboardStatus::create([
                    'cardboard_id' => $cek['cardboard_id'],
                    'user_id' => $user['user_id'],
                    'cardboard_status_type' => 'tolak'
                ]);

                if ($create) {
                    # code...
                    return response(['status' => true, 'message' => 'Penolakan Berhasil'], 200);
                } else {
                    return response(['status' => false, 'message' => 'Gagal Melakukan Penolakan'], 200);
                }
            }
        }
    }

    public function getListPromo(Request $request)
    {
        $this->validate($request, ['token' => 'required']);

        $promo = Promo::orderBy('bonus_name', 'asc')->get();
        if (count($promo) <= 0) {
            # code...
            return response(['status' => false, 'message' => 'Promo Tidak Ada', 'data' => []], 200);
        } else {
            return response(['status' => true, 'message' => 'Promo Ditemukan', 'data' => $promo], 200);
        }
    }

    public function logout(Request $request)
    {
        $this->validate($request, ['token' => 'required']);
        
        try {
            JWTAuth::invalidate($request->input('token'));
            return response()->json(['success' => true, 'message'=> "You have successfully logged out."]);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'message' => 'Failed to logout, please try again.'], 500);
        }
    }

    protected function savePhoto($photo)
    {
        $destinationPath = 'images';
        $subdestinationPath = 'customer';
        $extension = $photo->getClientOriginalExtension();
        $fileName = rand(11111,99999).'.'.$extension;
        // $fileName = array_pop(explode(DIRECTORY_SEPARATOR, $photo));
        $photo->move($destinationPath. '/' . $subdestinationPath , $fileName);
        $create['customer_img'] = $destinationPath. '/' . $subdestinationPath . '/' . $fileName;

        return $create['customer_img'];
    }

    protected function deletePhoto($photo)
    {
        File::delete($photo);
        return $photo;
    }

    protected function formatMemoCode($brand)
    {
        $code = '';
        $cek = Brand::where('brand_id', $brand)->first();
        if (empty($cek)) {
            return $code;
        } else {
            $year =date('y');

            switch (date('m')) {
                case '01': $month = "A"; break;
                case '02': $month = "B"; break;
                case '03': $month = "C"; break;
                case '04': $month = "D"; break;
                case '05': $month = "E"; break;
                case '06': $month = "F"; break;
                case '07': $month = "G"; break;
                case '08': $month = "H"; break;
                case '09': $month = "I"; break;
                case '10': $month = "J"; break;
                case '11': $month = "K"; break;
                case '12': $month = "L"; break;
            }

            $likes = $cek->brand_code.$year.$month;
            $findMemo = Memo::select('memo_code')->where('memo_code', 'LIKE', '%'.$likes.'%')->orderBy('memo_id', 'desc')->first();

            $number = 0;

            if (empty($findMemo)) {
                $number = sprintf("%0" . 4 . "d", $number + 1);
                $code = $cek->brand_code.$year.$month.$number;
                return $code;
            }

            $number = intval(substr($findMemo->memo_code, -4));
            $number = sprintf("%0" . 4 . "d", $number + 1);
            $code = $cek->brand_code.$year.$month.$number;
            return $code;
        }
    }
}
