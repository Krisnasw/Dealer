<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Alert;
use Access;
use Validator;
use App\Grade;

class GradeController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,38);

            $this->permit = $akses->permit_acces;
                
            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            $grade = Grade::orderBy('grade_id', 'asc')->get();
            return view('admin.grade.index', compact('grade'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            # code...
            return view('admin.grade.create');
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'kode' => 'required',
            'name' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Grade::create([
                'grade_kode' => $request->input('kode'),
                'grade_nama' => $request->input('name')
            ]);

            if ($create) {
                # code...
                Alert::success('Grade Berhasil Dibuat', 'Success');
                return redirect('home/setup/grade');
            } else {
                Alert::error('Gagal Membuat Grade', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strpos($this->permit, 'u') !== null) {
            # code...
            $grade = Grade::where('grade_id', base64_decode($id))->first();
            return view('admin.grade.edit', compact('grade'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'kode' => 'required',
            'name' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Grade::where('grade_id', base64_decode($id))->update([
                'grade_kode' => $request->input('kode'),
                'grade_nama' => $request->input('name')
            ]);

            if ($create) {
                # code...
                Alert::success('Grade Berhasil Diupdate', 'Success');
                return redirect('home/setup/grade');
            } else {
                Alert::error('Gagal Update Grade', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            # code...
            $del = Grade::findOrFail(base64_decode($id));
            if ($del->delete()) {
                # code...
                Alert::success('Grade Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menghapus Grade', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403);
        }
    }
}
