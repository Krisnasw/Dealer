<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Access;
use Alert;
use Validator;
use App\User;
use App\Employee;
use App\UserType;

class UserController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,29);

            $this->permit = $akses->permit_acces;
                
            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            $user = User::orderBy('user_username', 'asc')->get();
            return view('admin.user.index', compact('user'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            $pegawai = Employee::orderBy('employee_name', 'asc')->get();
            $roles = UserType::orderBy('user_type_name', 'asc')->get();
            return view('admin.user.create', compact('pegawai', 'roles'));
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'pegawai' => 'required',
            'roles' => 'required',
            'username' => 'required',
            'password' => 'required',
            'cpassword' => 'required|same:password'
        ]);

        if ($valid->fails()) {
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = User::create([
                'user_last_name' => '',
                'user_img' => 'NULL',
                'employee_id' => $request->input('pegawai'),
                'user_type_id' => $request->input('roles'),
                'user_username' => $request->input('username'),
                'password' => bcrypt($request->input('password')),
                'user_active_status' => ($request->input('status') == null) ? 'false' : 'true'
            ]);

            if ($create) {
                Alert::success('User Berhasil Ditambahkan', 'Success');
                return redirect('home/setting/user');
            } else {
                Alert::error('Gagal Menambahkan User', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strpos($this->permit, 'u') !== null) {
            $user = User::where('user_id', base64_decode($id))->first();
            $pegawai = Employee::orderBy('employee_name', 'asc')->get();
            $roles = UserType::orderBy('user_type_name', 'asc')->get();
            return view('admin.user.edit', compact('user', 'pegawai', 'roles'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'pegawai' => 'required',
            'roles' => 'required',
            'username' => 'required',
            'password' => 'required',
            'cpassword' => 'required|same:password'
        ]);

//        dd($request->status);

        if ($valid->fails()) {
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = User::where('user_id', base64_decode($id))->update([
                'user_last_name' => '',
                'user_img' => 'NULL',
                'employee_id' => $request->input('pegawai'),
                'user_type_id' => $request->input('roles'),
                'user_username' => $request->input('username'),
                'password' => bcrypt($request->input('password')),
                'user_active_status' => ($request->input('status') == null) ? 'false' : 'true'
            ]);

            if ($create) {
                Alert::success('User Berhasil Diupdate', 'Success');
                return redirect('home/setting/user');
            } else {
                Alert::error('Gagal Update User', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            $del = User::findOrFail(base64_decode($id));
            if ($del->delete()) {
                Alert::success('User Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menghapus User', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403);
        }
    }
}
