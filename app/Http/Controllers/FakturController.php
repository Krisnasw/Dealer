<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Alert;
use Access;
use Validator;
use App\Item;
use App\Nota;
use App\Sales;
use App\Brand;
use App\Customer;
use App\NotaDetail;
use App\NotaDetailTmp;
use App\MemoDetail;

class FakturController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,52);

            $this->permit = $akses->permit_acces;

            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            $nota = Nota::orderBy('nota_id', 'desc')->get();
            return view('admin.faktur.index', compact('nota'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            $brand = Brand::select('brand_id', 'brand_name')->get();
            $sales = Sales::select('sales_id', 'sales_name')->get();
            $cust = Customer::select('customer_id', 'customer_name', 'customer_code')->get();
            return view('admin.faktur.create', compact('brand', 'sales', 'cust'));
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'brand' => 'required',
            'sales' => 'required',
            'customer' => 'required',
            'type' => 'required',
            'tempo' => 'required',
            'grandTotal' => 'required',
            'discount' => 'required',
            'netto' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => 'error', 'message' => 'Form Tidak Lengkap'], 200);
        } else {
            $cek_rows = NotaDetailTmp::where('user_id', Auth::user()->user_id)->where('nota_fill_qty', '<>', 0)->get();
            $num = $this->format_code($request->brand);
            $create = Nota::create([
                'nota_code' => $num,
                'nota_date' => date('Y-m-d'),
                'sales_id' => $request->sales,
                'customer_id' => $request->customer,
                'brand_id' => $request->brand,
                'nota_type_pay' => $request->type,
                'nota_status' => 0,
                'nota_tempo' => $request->tempo,
                'nota_total' => $request->grandTotal,
                'nota_accumulation' => $request->grandTotal,
                'nota_commission' => 0,
                'nota_discount' => 0,
                'nota_discount2' => $request->discount,
                'nota_netto' => $request->netto
            ]);

            $lastInsertedId = Nota::select('notas.*', 'b.customer_name', 'b.customer_expedisi', 'b.customer_store', 'b.customer_addres', 'b.customer_code', 'c.sales_name', 'c.sales_code',
                                            \DB::raw('COUNT(d.nota_detail_id) as jml_detail'), 'e.city_name')
                                    ->join('customers as b', 'b.customer_id', 'notas.customer_id')
                                    ->join('sales as c', 'c.sales_id', 'notas.sales_id')
                                    ->join('cities as e', 'e.city_id', 'b.city_id')
                                    ->join('nota_details as d', 'd.nota_id', 'notas.nota_id')
                                    ->where('notas.nota_id', $create->nota_id)
                                    ->get();

            $list = NotaDetailTmp::select('nota_detail_tmp.*', 'd.item_name', 'd.item_het', 'd.item_stock', 'c.memo_detail_qty', 'c.memo_detail_discount', 'c.memo_accumulation', 'c.memo_detail_cancel', 'e.memo_code', 'e.memo_date', 'c.memo_detail_id', 'c.item_id')
                                ->join('memo_details as c', 'c.memo_detail_id', 'nota_detail_tmp.memo_detail_id')
                                ->join('items as d', 'd.item_id', 'c.item_id')
                                ->join('memos as e', 'e.memo_id', 'c.memo_id')
                                ->where('nota_detail_tmp.user_id', Auth::user()->user_id)
                                ->get();

            foreach ($list as $row):

                $data2['nota_id']                   = $create->nota_id;
                
                $data2['nota_fill_qty']             = $row['nota_fill_qty'];
                $data2['nota_detail_total']         = $row['nota_detail_total'];
                $data2['nota_detail_discount']      = $row['nota_detail_discount'];
                $data2['nota_detail_price']         = $row['nota_detail_price'];

                $stock_akhir        = $row['item_stock'] - $data2['nota_fill_qty'];
                $akumulasi_akhir    = $row['memo_accumulation'] + $data2['nota_fill_qty'];

                Item::where('item_id', $row['item_id'])->update([
                    'item_stock' => $stock_akhir
                ]);

                if ($row['memo_detail_id']) {
                    # code...
                    MemoDetail::where('memo_detail_id', $row['memo_detail_id'])->update([
                        'memo_accumulation' => $akumulasi_akhir
                    ]);

                    $data2['memo_detail_id']            = $row['memo_detail_id'];
                    $data2['item_id']                   = $row['item_id_memo'];
                } else {
                    $data2['item_id']                   = $row['item_id'];
                }

                // if ($data2['nota_fill_qty'] != 0) {
                    NotaDetail::create([
                        'nota_id' => $create->nota_id,
                        'memo_detail_id' => $data2['memo_detail_id'],
                        'nota_detail_price' => $data2['nota_detail_price'],
                        'nota_fill_qty' => $data2['nota_fill_qty'],
                        'nota_detail_total' => $data2['nota_detail_total'],
                        'nota_detail_discount' => $data2['nota_detail_discount'],
                        'item_id' => $data2['item_id']
                    ]);
                // }

            endforeach;

            NotaDetailTmp::where('user_id', Auth::user()->user_id)->delete();

            if ($create) {
                # code...
                return response(['status' => 'success', 'message' => 'Nota Berhasil Dibuat'], 200);
            } else {
                return response(['status' => 'error', 'message' => 'Gagal Membuat Nota'], 200);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strpos($this->permit, 'u') !== null) {
            $brand = Brand::select('brand_id', 'brand_name')->get();
            $sales = Sales::select('sales_id', 'sales_name')->get();
            $cust = Customer::select('customer_id', 'customer_name', 'customer_code')->get();
            $nota = Nota::where('nota_id', base64_decode($id))->first();
            $list = NotaDetail::select('nota_details.*', 'd.item_name', 'd.item_het', 'd.item_stock', 'c.memo_detail_qty', 'c.memo_detail_discount', 'c.memo_accumulation', 'c.memo_detail_cancel', 'c.memo_detail_id', 'c.item_id')
                                ->join('memo_details as c', 'c.memo_detail_id', 'nota_details.memo_detail_id')
                                ->join('items as d', 'd.item_id', 'c.item_id')
                                ->where('nota_details.nota_id', base64_decode($id))
                                ->get();
            return view('admin.faktur.edit', compact('nota', 'brand', 'sales', 'cust', 'list'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            $del = Nota::findOrFail(base64_decode($id));
            if ($del->delete()) {
                Alert::success('Faktur Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Faktur Gagal Dihapus', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403);
        }
    }

    protected function format_code($brand)
    {
        $code = '';
        $cek = Brand::where('brand_id', $brand)->first();
        if (empty($cek)) {
            return $code;
        } else {
            $year =date('y');

            switch (date('m')) {
                case '01': $month = "A"; break;
                case '02': $month = "B"; break;
                case '03': $month = "C"; break;
                case '04': $month = "D"; break;
                case '05': $month = "E"; break;
                case '06': $month = "F"; break;
                case '07': $month = "G"; break;
                case '08': $month = "H"; break;
                case '09': $month = "I"; break;
                case '10': $month = "J"; break;
                case '11': $month = "K"; break;
                case '12': $month = "L"; break;
            }

            $likes = $cek->brand_code.'-'.$year.$month;
            $findMemo = Nota::select('nota_code')->where('nota_code', 'LIKE', '%'.$likes.'%')->orderBy('nota_id', 'desc')->first();
            $count = Nota::select('nota_code')->where('nota_code', 'LIKE', '%'.$likes.'%')->orderBy('nota_id', 'desc')->count();

            $number = 0;

//            dd(sprintf("%04d", $number));
            if ($count <= 0) {
                $numbers = sprintf("%0" . 4 . "d", $number + 1);
                $code = $cek->brand_code.'-'.$year.$month.$numbers;
                return $code;
            }

            $numbers = intval(substr($findMemo->nota_code, -4));
            $number = sprintf("%0" . 4 . "d", $numbers + 1);
            $code = $cek->brand_code.'-'.$year.$month.$number;
            return $code;
        }
    }
}
