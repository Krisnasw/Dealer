<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Access;
use Alert;
use Validator;
use App\Retur;
use App\ReturDetail;
use App\Receipt;
use App\ReceiptCardboard;
use App\ReceiptAgency;

class ReturSupController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,63);

            $this->permit = $akses->permit_acces;
                
            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            $list = Retur::select('returs.*', 'b.receipt_code')->join('receipts as b', 'b.receipt_id', 'returs.retur_data_id')->where('returs.retur_type', 2)->get();
            return view('admin.retur-supplier.index', compact('list'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            # code...
            $do = Receipt::orderBy('receipt_id', 'desc')->get();
            return view('admin.retur-supplier.create', compact('do'));
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'kode_do' => 'required',
            'jenis' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Retur::create([
                'retur_type' => 2,
                'retur_data_id' => $request->kode_do,
                'retur_type_saldo' => $request->jenis,
                'retur_code' => $this->format_code(),
                'retur_date' => date('Y-m-d'),
                'retur_total' => 0,
                'retur_status' => 0
            ]);

            if ($create) {
                # code...
                Alert::success('Data Berhasil Diproses', 'Success');
                return redirect('home/pembelian/retur-supplier/'.base64_encode($create->retur_id));
            } else {
                Alert::error('Gagal Memproses Data', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            $do = Receipt::orderBy('receipt_id', 'desc')->get();
            $retur = Retur::where('retur_id', base64_decode($id))->first();
            $list = ReturDetail::select('retur_details.*', 'c.receipt_cardboard_barcode', 'd.purchase_detail_price', 'd.purchase_detail_qty', 'e.item_code')
                                ->join('receipt_agencies as b', 'b.receipt_agency_id', 'retur_details.retur_detail_data_id')
                                ->join('receipt_cardboards as c', 'c.receipt_cardboard_id', 'b.receipt_cardboard_id')
                                ->join('purchase_details as d', 'd.purchase_detail_id', 'b.purchase_detail_id')
                                ->join('items as e', 'e.item_id', 'd.item_id')->where('retur_details.retur_id', base64_decode($id))->where('retur_details.retur_detail_type', 2)
                                ->get();
            $kodeps = ReceiptCardboard::where('receipt_id', $retur['retur_data_id'])->get();
            return view('admin.retur-supplier.show', compact('do', 'retur', 'list', 'kodeps'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            # code...
            $del = Retur::findOrFail(base64_decode($id));
            if ($del->delete()) {
                # code...
                Alert::success('Retur Supplier Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menghapus Retur Supplier', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403);
        }
    }

    public function saveDetail(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'id' => 'required',
            'value' => 'required',
            'data_id' =>'required',
            'status' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            return response(['status' => 'error', 'message' => 'Form Tidak Lengkap'], 200);
        } else {
            $create = ReturDetail::create([
                'retur_id' => $request->id,
                'retur_detail_data_id' => $request->data_id,
                'retur_detail_qty' => $request->value,
                'retur_detail_status' => $request->status,
                'user_id' => Auth::user()->user_id,
                'retur_detail_type' => 2
            ]);

            $list = ReturDetail::select('retur_details.*', 'c.receipt_cardboard_barcode', 'd.purchase_detail_price', 'd.purchase_detail_qty', 'e.item_code')
                                ->join('receipt_agencies as b', 'b.receipt_agency_id', 'retur_details.retur_detail_data_id')
                                ->join('receipt_cardboards as c', 'c.receipt_cardboard_id', 'b.receipt_cardboard_id')
                                ->join('purchase_details as d', 'd.purchase_detail_id', 'b.purchase_detail_id')
                                ->join('items as e', 'e.item_id', 'd.item_id')
                                ->where('retur_details.retur_id', $request->id)
                                ->where('retur_details.retur_detail_type', 2)->get();
            $grand_total = 0;
            foreach ($list as $row) {
                $total = $row['retur_detail_qty'] * $row['purchase_detail_price'];
                $grand_total += $total;
            }

            Retur::where('retur_id', $request->id)->update([
                'retur_total' => $grand_total
            ]);

            if ($create) {
                # code...
                return response(['status' => 'success', 'message' => 'Barang Berhasil Ditambahkan'], 200);
            } else {
                return response(['status' => 'error', 'message' => 'Gagal Menambahkan Barang'], 200);
            }
        }
    }

    public function readPart(Request $request)
    {
        $part = ReceiptAgency::select('receipt_agencies.*', 'c.item_code')
                            ->join('purchase_details as b', 'b.purchase_detail_id', 'receipt_agencies.purchase_detail_id')
                            ->join('items as c', 'c.item_id', 'b.item_id')
                            ->where('receipt_agencies.receipt_cardboard_id', $request->id)
                            ->get();
        $data = "<option value='0'>-- Pilih Number Part --</option>";
        foreach ($part as $row) {
            $data .= "<option value=".$row['receipt_agency_id']." >".$row['item_code']."</option>";
        }

        $response['type']       = 'spbe';
        $response['content']    = $data;
        $response['param']      = '';
        return response($response);
    }

    public function readQty(Request $request)
    {
        $query = ReceiptAgency::select('receipt_agency_qty')->where('receipt_agency_id', $request->id)->first();
        $response['type']       = 'spbe';
        $response['content']    = $query['receipt_agency_qty'];
        $response['param']      = '';
        return response($response);
    }

    protected function format_code()
    {
        $code = '';

        $likes = "RT-SUP";
        $findMemo = Retur::select('retur_code')->where('retur_code', 'LIKE', '%'.$likes.'%')->orderBy('retur_id', 'desc')->first();
        $count = Retur::select('retur_code')->where('retur_code', 'LIKE', '%'.$likes.'%')->orderBy('retur_id', 'desc')->count();

        $number = 0;

//            dd(sprintf("%04d", $number));
        if ($count <= 0) {
            $numbers = sprintf("%0" . 4 . "d", $number + 1);
            $code = "RT-SUP".$numbers;
            return $code;
        }

        $numbers = intval(substr($findMemo->retur_code, -4));
        $number = sprintf("%0" . 4 . "d", $numbers + 1);
        $code = "RT-SUP".$number;
        return $code;
    }
}
