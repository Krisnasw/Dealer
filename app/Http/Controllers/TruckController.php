<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Access;
use Alert;
use Validator;
use App\Truck;
use App\TruckScan;

class TruckController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,57);

            $this->permit = $akses->permit_acces;
                
            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            $truck = Truck::orderBy('truck_id', 'asc')->get();
            return view('admin.truk.index', compact('truck'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            # code...
            return view('admin.truk.create');
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'nopol' => 'required',
            'kir' => 'required',
            'pajak' => 'required',
            'merk' => 'required',
            'tahun' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Truck::create([
                'truck_nopol' => $request->input('nopol'),
                'truck_tanggal_kir' => $request->input('kir'),
                'truck_tanggal_kir2' => $request->input('kir'),
                'truck_tanggal_pajak' => $request->input('pajak'),
                'truck_merk' => $request->input('merk'),
                'truck_tahun_buat' => $request->input('tahun')
            ]);

            if ($create) {
                # code...
                Alert::success('Truk Berhasil Ditambahkan', 'Success');
                return redirect('home/setup/truck');
            } else {
                Alert::error('Gagal Menambahkan Truk', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strpos($this->permit, 'u') !== null) {
            # code...
            $truck = Truck::where('truck_id', base64_decode($id))->first();
            $gal = TruckScan::where('truck_id', base64_decode($id))->get();
            return view('admin.truk.edit', compact('truck', 'gal'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'nopol' => 'required',
            'kir' => 'required',
            'pajak' => 'required',
            'merk' => 'required',
            'tahun' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Truck::where('truck_id', base64_decode($id))->update([
                'truck_nopol' => $request->input('nopol'),
                'truck_tanggal_kir' => $request->input('kir'),
                'truck_tanggal_kir2' => $request->input('kir'),
                'truck_tanggal_pajak' => $request->input('pajak'),
                'truck_merk' => $request->input('merk'),
                'truck_tahun_buat' => $request->input('tahun')
            ]);

            if ($create) {
                # code...
                Alert::success('Truk Berhasil Diupdate', 'Success');
                return redirect('home/setup/truck');
            } else {
                Alert::error('Gagal Update Truk', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            # code...
            $del = Truck::findOrFail(base64_decode($id));
            if ($del->delete()) {
                # code...
                Alert::success('Truck Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menghapus Truck', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403);
        }
    }
}
