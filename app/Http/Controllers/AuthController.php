<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Alert;
use Auth;
use App\User;

class AuthController extends Controller
{
    //
    public function __construct()
    {
    	$this->middleware('guest');
    }

    public function index()
    {
    	return view('welcome');
    }

    public function doLogin(Request $request)
    {
    	$valid = Validator::make($request->all(), [
    		'username' => 'min:3|required',
    		'password' => 'min:3|required'
    	]);

    	if ($valid->passes()) {
    		# code...
    		if ( Auth::attempt(['user_username' => $request->input('username'), 'password' => $request->input('password') ]) ) {
    			# code...
    			Alert::success('Login Berhasil', 'Success');
    			return redirect('home/main');
    		} else {
    			Alert::error('Username atau Password Salah', 'Error');
    			return redirect()->back();
    		}
    	} else {
    		Alert::error('Username atau Password Masih Kosong', 'Error');
    		return redirect()->back();
    	}
    }
}