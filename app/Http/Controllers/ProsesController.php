<?php

namespace App\Http\Controllers;

use App\MemoDetail;
use Illuminate\Http\Request;
use Auth;
use Access;
use Alert;
use Validator;
use App\Memo;
use App\Brand;
use App\Sales;
use App\Customer;

class ProsesController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,47);

            $this->permit = $akses->permit_acces;

            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            $memo = Memo::orderBy('memo_id', 'desc')->with('customer')->with('sales')->with('brand')->get();
//            dd($memo);
            return view('admin.proses.index', compact('memo'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            $brand = Brand::select('brand_id', 'brand_name')->get();
            $sales = Sales::select('sales_id', 'sales_name')->get();
            $memoDet = MemoDetail::join('items as c', 'c.item_id', 'memo_details.item_id')->where('memo_details.memo_id', 0)->where('memo_details.user_id', Auth::user()->user_id)->get();
            $cust = Customer::select('customer_id', 'customer_name')->get();
            return view('admin.proses.create', compact('brand', 'sales', 'cust', 'memoDet'));
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'merk' => 'required',
            'sales' => 'required',
            'customer' => 'required'
        ]);

        if ($valid->fails()) {
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $num = $this->format_code($request->merk);
            $create = Memo::create([
                'memo_code' => $num,
                'memo_date' => date('Y-m-d'),
                'sales_id' => $request->input('sales'),
                'customer_id' => $request->input('customer'),
                'memo_type_pay' => 0,
                'brand_id' => $request->input('merk')
            ]);

            $cek = MemoDetail::where('memo_id', 0)->get();

            for ($i=0; $i < count($cek); $i++) { 
                # code...
                MemoDetail::where('memo_id', 0)->update([
                    'memo_id' => $create->memo_id
                ]);
            }

            if ($create) {
                Alert::success('Memo Berhasil Dibuat', 'Success');
                return redirect('home/penjualan/proses');
            } else {
                Alert::error('Gagal Membuat Memo', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (strpos($this->permit, 'u') !== null) {
            $memo = Memo::where('memo_id', base64_decode($id))->first();
            $memoDet = MemoDetail::where('memo_id', base64_decode($id))->get();
            $brand = Brand::select('brand_id', 'brand_name')->get();
            $sales = Sales::select('sales_id', 'sales_name')->get();
            $cust = Customer::select('customer_id', 'customer_name')->get();
            return view('admin.proses.edit', compact('brand', 'sales', 'cust', 'memo', 'memoDet'));
        } else {
            abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $valid = Validator::make($request->all(), [
            'merk' => 'required',
            'sales' => 'required',
            'customer' => 'required'
        ]);

        if ($valid->fails()) {
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Memo::where('memo_id', base64_decode($id))->update([
                'memo_date' => date('Y-m-d'),
                'sales_id' => $request->input('sales'),
                'customer_id' => $request->input('customer'),
                'memo_type_pay' => 0,
                'brand_id' => $request->input('merk')
            ]);

            if ($create) {
                Alert::success('Memo Berhasil Diupdate', 'Success');
                return redirect('home/penjualan/proses');
            } else {
                Alert::error('Gagal Update Memo', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            $del = Memo::findOrFail(base64_decode($id));
            if ($del->delete()) {
                Alert::success('Memo Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menghapus Memo', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403);
        }
    }

    protected function format_code($brand)
    {
        $code = '';
        $cek = Brand::where('brand_id', $brand)->first();
        if (empty($cek)) {
            return $code;
        } else {
            $year =date('y');

            switch (date('m')) {
                case '01': $month = "A"; break;
                case '02': $month = "B"; break;
                case '03': $month = "C"; break;
                case '04': $month = "D"; break;
                case '05': $month = "E"; break;
                case '06': $month = "F"; break;
                case '07': $month = "G"; break;
                case '08': $month = "H"; break;
                case '09': $month = "I"; break;
                case '10': $month = "J"; break;
                case '11': $month = "K"; break;
                case '12': $month = "L"; break;
            }

            $likes = $cek->brand_code.$year.$month;
            $findMemo = Memo::select('memo_code')->where('memo_code', 'LIKE', '%'.$likes.'%')->orderBy('memo_id', 'desc')->first();

            $number = 0;

//            dd(sprintf("%04d", $number));
            if (empty($findMemo)) {
                $number = sprintf("%0" . 4 . "d", $number + 1);
                $code = $cek->brand_code.$year.$month.$number;
                return $code;
            }

            $number = intval(substr($findMemo->memo_code, -4));
            $number = sprintf("%0" . 4 . "d", $number + 1);
            $code = $cek->brand_code.$year.$month.$number;
            return $code;
        }
    }
}
