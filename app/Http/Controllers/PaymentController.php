<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Access;
use Alert;
use Validator;
use DB;
use View;
use App\Payment;
use App\PaymentDebit;
use App\PaymentEntrusted;
use App\PaymentNota;
use App\PaymentRetur;
use App\PaymentDetail;
use App\Customer;
use App\Nota;
use App\Retur;
use App\Entrusted;
use App\Debit;

class PaymentController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,58);

            $this->permit = $akses->permit_acces;

            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            $list = Payment::select('payments.*', 'c.total as total_retur', 'd.total as total_debit', 'e.total as total_entrusted', 'f.total as total_kredit', 'b.total as total_tagihan', 'g.total as total_terbayar')
                            ->leftJoin(DB::raw("(select h.payment_id,sum(i.nota_total - (i.nota_total * nota_discount / 100)) as total from payment_details h join notas i on i.nota_id = h.nota_id where h.payment_detail_type = 0 group by h.payment_id) as b"), 'b.payment_id', 'payments.payment_id')
                            ->leftJoin(DB::raw("(select h.payment_id,sum(i.retur_total) as total from payment_returs h join returs i on i.retur_id = h.retur_id group by h.payment_id) as c"), 'c.payment_id', 'payments.payment_id')
                            ->leftJoin(DB::raw("(select k.payment_id,sum(l.debit_nominal) as total from payment_debits k join debits l on l.debit_id = k.debit_id where l.debit_type = 1 group by k.payment_id) as d"), 'd.payment_id', 'payments.payment_id')
                            ->leftJoin(DB::raw("(select h.payment_id,sum(i.entrusted_total) as total from payment_entrusteds h join entrusteds i on i.entrusted_id = h.entrusted_id group by h.payment_id) as e"), 'e.payment_id', 'payments.payment_id')
                            ->leftJoin(DB::raw("(select k.payment_id,sum(l.debit_nominal) as total from payment_debits k join debits l on l.debit_id = k.debit_id where l.debit_type = 2 group by k.payment_id) as f"), 'd.payment_id', 'payments.payment_id')
                            ->leftJoin(DB::raw("(select h.payment_id,sum(h.payment_detail_nominal) as total from payment_details h where h.payment_detail_type <> 0 group by h.payment_id) as g"), 'g.payment_id', 'payments.payment_id')
                            ->get();
            return view('admin.payment.index', compact('list'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            # code...
            $cust = Customer::select('customer_id', 'customer_name')->get();
            return view('admin.payment.create', compact('cust'));
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $valid = Validator::make($request->all(), [
            'customer_id' => 'required',
            'date' => 'required'
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $create = Payment::create([
                'payment_code' => $this->format_code(),
                'payment_date' => $request->date,
                'customer_id' => $request->customer_id,
                'entrusted_id' => 0
            ]);

            $list_detail = Nota::where('nota_total', '<>', 'nota_accumulation')->where('customer_id', $request->customer_id)->get();
            $list_retur = Retur::select('returs.*')->join('notas as b', 'b.nota_id', 'returs.retur_data_id')->where('returs.retur_type', 1)->where('returs.retur_status', 0)->where('b.customer_id', $request->customer_id)->get();
            $list_entrusted = Entrusted::where('entrusted_status', 0)->where('customer_id', $request->customer_id)->get();
            $list_debit = Debit::where('debit_status', 0)->where('customer_id', $request->customer_id)->get();

            if ($list_detail) {
                # code...
                foreach ($list_detail as $row) {
                    $cek = $request->nota.$row['nota_id'];
                    if ($cek) {
                        # code...
                        PaymentNota::create([
                            'payment_id' => $create->payment_id,
                            'nota_id' => $row['nota_id']
                        ]);
                    }
                }
            }

            if ($list_retur) {
                # code...
                foreach ($list_retur as $row) {
                    $cek = $request->retur.$row['retur_id'];
                    if ($cek) {
                        # code...
                        PaymentRetur::create([
                            'payment_id' => $create->payment_id,
                            'retur_id' => $row['retur_id']
                        ]);

                        Retur::where('retur_id', $row['retur_id'])->update([
                            'retur_status' => 1
                        ]);
                    }
                }
            }

            if ($list_entrusted) {
                # code...
                foreach ($list_entrusted as $row) {
                    $cek = $request->entrusted_id.$row['entrusted_id'];
                    if ($cek) {
                        # code...
                        PaymentEntrusted::create([
                            'payment_id' => $create->payment_id,
                            'entrusted_id' => $row['entrusted_id']
                        ]);

                        Entrusted::where('entrusted_id', $row['entrusted_id'])->update([
                            'entrusted_status' => 1
                        ]);
                    }
                }
            }

            if ($list_debit) {
                # code...
                foreach ($list_debit as $row) {
                    $cek = $request->debit.$row['debit_id'];
                    if ($cek) {
                        # code...
                        PaymentDebit::create([
                            'payment_id' => $create->payment_id,
                            'debit_id' => $row['debit_id']
                        ]);

                        Debit::where('debit_id', $row['debit_id'])->update([
                            'debit_status' => 1
                        ]);
                    }
                }
            }

            $list_nota = PaymentNota::select('payment_notas.*', 'b.nota_code', \DB::raw('(b.nota_total - b.nota_accumulation) as tagihan'), 'b.nota_date')->join('notas as b', 'b.nota_id', 'payment_notas.nota_id')->where('payment_notas.payment_id', $create->payment_id)->get();

            foreach ($list_nota as $row) {
                $pd = new PaymentDetail();
                $pd->payment_id = $create->payment_id;
                $pd->nota_id = $row['nota_id'];
                $pd->payment_detail_date = $row['nota_date'];
                $pd->payment_detail_nominal = $row['tagihan'];
                $pd->save();

                Nota::where('nota_id', $row['nota_id'])->update([
                    'nota_accumulation' => $row['nota_total']
                ]);
            }

            if ($create) {
                # code...
                Alert::success('Data Berhasil Diproses', 'Success');
                return redirect('home/pembayaran/payment/'.base64_encode($create->payment_id));
            } else {
                Alert::error('Gagal Memproses Data', 'Error');
                return redirect()->back();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            $payment = Payment::where('payment_id', base64_decode($id))->first();
            $cust = Customer::select('customer_id', 'customer_name')->get();
            $nota = Nota::select('nota_code', 'nota_id')->where('customer_id', $payment['customer_id'])->get();
            $debit = Debit::select('debit_id', 'debit_code')->where('customer_id', $payment['customer_id'])->get();
            $retur = Retur::select('retur_id', 'retur_code')->get();
            $balance = Entrusted::select('entrusted_id', 'entrusted_code')->where('customer_id', $payment['customer_id'])->get();
            $list = PaymentDetail::select('payment_details.*', 'b.nota_code')->join('notas as b', 'b.nota_id', 'payment_details.nota_id')->where('payment_details.payment_id', base64_decode($id))->get();
            $list_retur = PaymentRetur::select('payment_returs.*', 'b.retur_code', 'b.retur_total', 'b.retur_date')->join('returs as b', 'b.retur_id', 'payment_returs.retur_id')->where('payment_returs.payment_id', base64_decode($id))->get();
            $list_debit = PaymentDebit::select('payment_debits.*', 'b.debit_code', 'b.debit_date', 'b.debit_nominal', 'b.debit_type')->join('debits as b', 'b.debit_id', 'payment_debits.debit_id')->where('payment_debits.payment_id', base64_decode($id))->get();
            $list_entrusted = PaymentEntrusted::select('payment_entrusteds.*', 'b.entrusted_code', 'b.entrusted_date', 'b.entrusted_total')->join('entrusteds as b', 'b.entrusted_id', 'payment_entrusteds.entrusted_id')->where('payment_entrusteds.payment_id', base64_decode($id))->get();
            $list_nota = PaymentNota::select('payment_notas.*', 'b.nota_code', \DB::raw('(b.nota_code - b.nota_accumulation) as tagihan'), 'b.nota_date')->join('notas as b', 'b.nota_id', 'payment_notas.nota_id')->where('payment_notas.payment_id', base64_decode($id))->get();
            return view('admin.payment.show', compact('payment', 'cust', 'nota', 'debit', 'retur', 'balance', 'list', 'list_retur', 'list_debit', 'list_entrusted', 'list_nota'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            # code...
            $del = Payment::findOrFail(base64_decode($id));
            if ($del->delete()) {
                # code...
                Alert::success('Data Berhasil Dihapus', 'Success');
                return redirect()->back();
            } else {
                Alert::error('Gagal Menghapus Data', 'Error');
                return redirect()->back();
            }
        } else {
            abort(403);
        }
    }

    public function preview($id)
    {
        if (strpos($this->permit, 'c') !== null) {
            # code...
            $expiresAt = now()->addMinutes(30);
            \Cache::put('customer_id', $id, $expiresAt);
            $cust = Customer::select('customer_id', 'customer_name')->get();
            $list_detail = Nota::where('nota_total', '<>', 'nota_accumulation')->where('customer_id', $id)->get();
            $list_retur = Retur::select('returs.*')->join('notas as b', 'b.nota_id', 'returs.retur_data_id')->where('returs.retur_type', 1)->where('returs.retur_status', 0)->where('b.customer_id', $id)->get();
            $list_entrusted = Entrusted::where('entrusted_status', 0)->where('customer_id', $id)->get();
            $list_debit = Debit::where('debit_status', 0)->where('customer_id', $id)->get();
            return view('admin.payment.preview', compact('cust', 'list_detail', 'list_retur', 'list_entrusted', 'list_debit'));
        } else {
            abort(403);
        }
    }

    public function addDetailNew(Request $request)
    {
        $id                 = $request->id;
        $i_detail_type      = (empty($request->i_detail_type)) ? 0 : $request->i_detail_type;
        $i_detail_rek       = (empty($request->i_detail_rek)) ? 0 : $request->i_detail_rek;
        $i_detail_bank      = (empty($request->i_detail_bank)) ? 0 : $request->i_detail_bank;
        $i_detail_tempo     = (empty($request->i_detail_tempo)) ? 0 : $request->i_detail_tempo;
        $i_detail_nominal   = (empty($request->i_detail_nominal)) ? 0 : $request->i_detail_nominal;

        if ($i_detail_tempo) {
            # code...
            $date = explode("/", $i_detail_tempo);
            $date = $date[2]."-".$date['0']."-".$date[1];
        } else {
            $date = date('Y-m-d');
        }

        $create = new PaymentDetail;

        for ($i = 0; $i < count($request->i_nota); $i++) {
            $create['payment_id'] = $id;
            $create['nota_id'] = $request->i_nota[$i];
            $create['payment_detail_type'] = $i_detail_type;
            $create['payment_detail_nominal'] = $i_detail_nominal;
            $create['payment_detail_rek'] = $i_detail_rek;
            $create['payment_detail_bank'] = $i_detail_bank;
            $create['payment_detail_date'] = date('Y-m-d');
            $create['payment_detail_tempo'] = $date;
            $create['user_id'] = Auth::user()->user_id;
        }

        if ($create->save()) {
            # code...
            return response(['status' => 'success', 'message' => 'Detail Berhasil Ditambahkan'], 200);
        } else {
            return response(['status' => 'error', 'message' => 'Gagal Menambahkan Detail'], 200);
        }
    }

    public function doBayar(Request $request)
    {
        $valid = Validator::make($request->all(), [
            'id' => 'required',
        ]);

        if ($valid->fails()) {
            # code...
            Alert::info('Form Tidak Lengkap', 'Info');
            return redirect()->back();
        } else {
            $update = PaymentDetail::where('payment_id', $request->id)->update([
                'payment_detail_status' => 1,
            ]);

            if ($update) {
                # code...
                Alert::success('Pembayaran Berhasil Dilakukan', 'Success');
                return redirect('pembayaran/payment');
            } else {
                Alert::error('Gagal Melakukan Pembayaran', 'Error');
                return redirect()->back();
            }
        }
    }

    protected function format_code()
    {
        $code = '';
        $year = date('y');

        switch (date('m')) {
            case '01': $month = "A"; break;
            case '02': $month = "B"; break;
            case '03': $month = "C"; break;
            case '04': $month = "D"; break;
            case '05': $month = "E"; break;
            case '06': $month = "F"; break;
            case '07': $month = "G"; break;
            case '08': $month = "H"; break;
            case '09': $month = "I"; break;
            case '10': $month = "J"; break;
            case '11': $month = "K"; break;
            case '12': $month = "L"; break;
        }

        $likes = 'PAY-'.$year.$month;
        $findMemo = Payment::select('payment_code')->where('payment_code', 'LIKE', '%'.$likes.'%')->orderBy('payment_code', 'desc')->first();

        $number = 0;

        if (empty($findMemo)) {
            $number = sprintf("%0" . 4 . "d", $number + 1);
            $code = 'PAY-'.$year.$month.$number;
            return $code;
        }

        $number = intval(substr($findMemo->payment_code, -4));
        $number = sprintf("%0" . 4 . "d", $number + 1);
        $code = 'PAY-'.$year.$month.$number;
        return $code;
    }
}
