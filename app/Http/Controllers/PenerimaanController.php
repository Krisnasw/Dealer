<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Access;
use Alert;
use Validator;
use App\ReceiptStorehouse;
use App\ReceiptCardStorehouse;

class PenerimaanController extends Controller
{
    var $permit;
    protected $user;

    function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user()->user_id;

            $akses = Access::getUserAccess($this->user,74);

            $this->permit = $akses->permit_acces;
                
            if($akses->permit_acces == '') {
                abort(403, 'Unauthorized action.');
            }

            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (strpos($this->permit, 'r') !== null) {
            # code...
            $gudang = ReceiptStorehouse::orderBy('receipt_storehouse_id', 'desc')->get();
            return view('admin.penerimaan.index', compact('gudang'));
        } else {
            abort(403);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (strpos($this->permit, 'c') !== null) {
            # code...
            return view('admin.penerimaan.create');
        } else {
            abort(403);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $cek = ReceiptStorehouse::where('receipt_storehouse_date', $request->date)->get();
        if (count($cek) <= 0) {
            # code...
            $create = ReceiptStorehouse::create([
                'receipt_storehouse_date' => $request->date
            ]);

            if ($create) {
                # code...
                Alert::success('Data Berhasil Dimuat', 'Success');
                return redirect('home/gudang/penerimaan/'.base64_encode($create->receipt_storehouse_id));
            } else {
                Alert::error('Gagal Memuat Data', 'Error');
                return redirect()->back();
            }
        } else {
            Alert::info('Tanggal Sudah Ada', 'Info');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $receipt = ReceiptStorehouse::where('receipt_storehouse_id', base64_decode($id))->first();
        $list = ReceiptCardStorehouse::select('receipt_card_storehouses.*', 'b.*')->join('receipt_cardboards as b', 'b.receipt_cardboard_id', 'receipt_card_storehouses.receipt_cardboard_id')->where('receipt_card_storehouses.receipt_storehouse_id', base64_decode($id))->get();
        return view('admin.penerimaan.show', compact('receipt', 'list'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $create = ReceiptStorehouse::where('receipt_storehouse_id', base64_decode($id))->update([
            'receipt_storehouse_date' => $request->date
        ]);

        if ($create) {
            # code...
            Alert::success('Data Berhasil Diupdate', 'Success');
            return redirect('home/gudang/penerimaan/');
        } else {
            Alert::error('Gagal Update Data', 'Error');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if (strpos($this->permit, 'd') !== null) {
            # code...
        } else {
            abort(403);
        }
    }
}
