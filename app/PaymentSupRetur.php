<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentSupRetur extends Model
{
    //
    protected $table = 'payment_sup_returs';
    protected $primaryKey = 'payment_sup_retur_id';
    protected $fillable = ['payment_sup_id', 'retur_id'];

    public $timestamps = false;
}
