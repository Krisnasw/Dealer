<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cost extends Model
{
    //
    protected $table = 'costs';
    protected $primaryKey = 'cost_id';
    protected $fillable = ['cost_name'];

    public $timestamps = false;
}
