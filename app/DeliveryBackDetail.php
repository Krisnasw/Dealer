<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryBackDetail extends Model
{
    //
    protected $table = 'delivery_back_details';
    protected $primaryKey = 'delivery_back_detail_id';
    protected $fillable = ['delivery_back_id', 'cardboard_id'];

    public $timestamps = false;
}
