<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentEntrusted extends Model
{
    //
    protected $table = 'payment_entrusteds';
    protected $primaryKey = 'payment_entrusted_id';
    protected $fillable = ['payment_id', 'entrusted_id'];

    public $timestamps = false;
}
