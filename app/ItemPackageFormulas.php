<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemPackageFormulas extends Model
{
    //
    protected $table = 'item_package_formulas';
    protected $primaryKey = 'item_package_formula_id';
    protected $fillable = ['item_package_id', 'item_id', 'item_package_formula_qty'];

    public $timestamps = false;

    public function packages()
    {
    	return $this->belongsTo('App\ItemPackage', 'item_package_id', 'item_package_id');
    }

    public function items()
    {
    	return $this->belongsTo('App\Item', 'item_id', 'item_id');
    }
}
