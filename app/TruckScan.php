<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TruckScan extends Model
{
    //
    protected $table = 'truck_scans';
    protected $primaryKey = 'truck_scan_id';
    protected $fillable = ['truck_scan_img', 'truck_id'];

    public $timestamps = false;

    public function truk()
    {
    	return $this->belongsTo('App\Truck', 'truck_id', 'truck_id');
    }
}