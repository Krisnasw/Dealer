<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemPackage extends Model
{
    //
    protected $table = 'item_packages';
    protected $primaryKey = 'item_package_id';
    protected $fillable = ['item_package_code', 'brand_detail_id', 'item_package_name', 'unit_id', 'character_id', 'item_package_het', 'item_package_price_primary', 'item_package_price_buy', 'item_package_stock', 'item_package_stock_min', 'item_package_stock_max', 'item_package_barcode', 'item_package_weight'];

    public $timestamps = false;

    public function brandDetails()
    {
    	return $this->belongsTo('App\BrandDetail', 'brand_detail_id', 'brand_detail_id');
    }

    public function units()
    {
    	return $this->belongsTo('App\Unit', 'unit_id', 'unit_id');
    }
}
