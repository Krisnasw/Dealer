<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackagingNota extends Model
{
    //
    protected $table = 'packaging_notas';
    protected $primaryKey = 'packaging_nota_id';
    protected $fillable = ['packaging_id', 'nota_id'];

    public $timestamps = false;
}
