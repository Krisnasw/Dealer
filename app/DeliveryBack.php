<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryBack extends Model
{
    //
    protected $table = 'delivery_backs';
    protected $primaryKey = 'delivery_back_id';
    protected $fillable = ['delivery_id', 'packaging_id', 'delivery_back_note'];

    public $timestamps = false;
}
