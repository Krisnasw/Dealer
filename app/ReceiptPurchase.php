<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReceiptPurchase extends Model
{
    //
    protected $table = 'receipt_purchases';
    protected $primaryKey = 'receipt_purchase_id';
    protected $fillable = ['receipt_id', 'purchase_id'];

    public $timestamps = false;
}
