<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Debit extends Model
{
    //
    protected $table = 'debits';
    protected $primaryKey = 'debit_id';
    protected $fillable = ['debit_code', 'customer_id', 'debit_date', 'debit_nominal', 'debit_desc', 'debit_status', 'debit_type'];

    public $timestamps = false;

    public function customer()
    {
    	return $this->belongsTo('App\Customer', 'customer_id', 'customer_id');
    }
}
