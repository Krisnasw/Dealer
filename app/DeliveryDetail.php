<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryDetail extends Model
{
    //
    protected $table = 'delivery_details';
    protected $primaryKey = 'delivery_detail_id';
    protected $fillable = ['delivery_id', 'cardboard_id'];

    public $timestamps = false;
}
