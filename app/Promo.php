<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{
    //
    protected $table = 'bonuss';
    protected $primaryKey = 'bonus_id';
    protected $fillable = ['bonus_code', 'bonus_name', 'bonus_period_first', 'bonus_period_last', 'bonus_status'];

    public $timestamps = false;
}
