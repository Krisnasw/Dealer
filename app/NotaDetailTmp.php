<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotaDetailTmp extends Model
{
    //
    protected $table = 'nota_detail_tmp';
    protected $primaryKey = 'nota_detail_id';
    protected $fillable = ['nota_id', 'memo_detail_id', 'nota_detail_price', 'nota_fill_qty', 'nota_detail_total', 'nota_detail_discount', 'user_id', 'nota_detail_est', 'item_id'];

    public $timestamps = false;
}
