<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    //
    protected $table = 'grade_customers';
    protected $primaryKey = 'grade_id';
    protected $fillable = ['grade_kode', 'grade_nama'];

    public $timestamps = false;
}
