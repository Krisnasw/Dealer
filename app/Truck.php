<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Truck extends Model
{
    //
    protected $table = 'trucks';
    protected $primaryKey = 'truck_id';
    protected $fillable = ['truck_nopol', 'truck_tanggal_kir', 'truck_tanggal_kir2', 'truck_tanggal_pajak', 'truck_merk', 'truck_tahun_buat'];

    public $timestamps = false;
}
