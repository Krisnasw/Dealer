<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReceiptStorehouse extends Model
{
    //
    protected $table = 'receipt_storehouses';
    protected $primaryKey = 'receipt_storehouse_id';
    protected $fillable = ['receipt_storehouse_date'];

    public $timestamps = false;
}
