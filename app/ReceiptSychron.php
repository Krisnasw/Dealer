<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReceiptSychron extends Model
{
    //
    protected $table = 'receipt_sychrons';
    protected $primaryKey = 'receipt_sychron_id';
    protected $fillable = ['receipt_sychron_date'];

    public $timestamps = false;
}
