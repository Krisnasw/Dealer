<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    //
    protected $table = 'deliveries';
    protected $primaryKey = 'delivery_id';
    protected $fillable = ['delivery_code', 'delivery_date', 'delivery_sopir', 'delivery_kernet', 'packaging_id'];

    public $timestamps = false;
}
