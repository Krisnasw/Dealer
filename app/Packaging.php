<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Packaging extends Model
{
    //
    protected $table = 'packagings';
    protected $primaryKey = 'packaging_id';
    protected $fillable = ['packaging_date', 'nota_id', 'packaging_expedisi', 'packaging_status', 'packaging_sj'];

    public $timestamps = false;
}
