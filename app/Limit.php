<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Limit extends Model
{
    //
    protected $table = 'limits';
    protected $primaryKey = 'limit_id';
    protected $fillable = ['limit_start', 'limit_length'];

    public $timestamps = false;
}
