<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CardboardStatus extends Model
{
    //
    protected $table = 'cardboard_status';
    protected $primaryKey = 'cardboard_status_id';
    protected $fillable = ['cardboard_id', 'user_id', 'cardboard_status_type'];
}
